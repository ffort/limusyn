all: limusync

limusync:
	dune build src/bin/$@.exe

doc:
	dune build @doc

install: all
	dune build @install
	dune install

clean:
	dune clean
	rm ./limusync

tests: all
	dune build @install
	dune runtest -f

.PHONY: all limusync doc install clean tests

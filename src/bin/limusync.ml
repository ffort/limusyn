open Limusyn

exception Done

let node = ref ""

type process =
  | All
  | PrintFlat
  | PrintAnf
  | PrintTyped
  | PrintSClocked
  | PrintRClocked

let process : process ref  = ref All

let set_process s =
  match s with
  | "print_flat" -> process := PrintFlat
  | "print_anf" -> process := PrintAnf
  | "print_typed" -> process := PrintTyped
  | "print_sclocked" -> process := PrintSClocked
  | "print_rclocked" -> process := PrintRClocked
  | _ -> () (* unreachable *)

let set_debug s =
  match s with
  | "sclockedlang" -> Print.add_debug Print.DebugSClocked
  | "rclockedlang" -> Print.add_debug Print.DebugRClocked
  | "z3" -> Print.add_debug Print.DebugZ3
  | "typedlang" -> Print.add_debug Print.DebugTyped
  | _ -> () (* unreachable *)

let usage = "Usage: limusync [options] -node <node> <source-file>"

let options : (string * Arg.spec * string) list =
  [
    "-node", Arg.Set_string node, "Set main node";
    "-process", Arg.Symbol (["print_flat";"print_anf";"print_typed";"print_sclocked";"print_rclocked"], set_process), "Set processing mode";
    "-debug", Arg.Symbol (["sclockedlang"; "rclockedlang"; "typedlang"; "z3"], set_debug), "Print verbose debug messages"
  ]

let compile file =
  let main_node = !node in
  let prog = Parse.parse file in
  let prog = Flatlang.Make.of_parsed prog in
  if !process = PrintFlat then
    begin
      Format.printf "%a" Flatlang.PP.prog prog;
      raise Done
    end;
  let prog = Anflang.Make.of_flat prog in
  if !process = PrintAnf then
    begin
      Format.printf "%a" Anflang.PP.prog prog;
      raise Done;
    end;
  let prog = Typedlang.Make.of_anf main_node prog in
  if !process = PrintTyped then
    begin
      Format.printf "%a" Typedlang.PP.prog prog;
      raise Done
    end;
  let prog = Sclockedlang.Make.of_typed main_node prog in
  if !process = PrintSClocked then
    begin
      Format.printf "%a" Sclockedlang.PP.prog prog;
      raise Done;
    end;
  let prog = Rclockedlang.Make.of_sclocked main_node prog in
  if !process = PrintRClocked then
    begin
      Format.printf "%a" Rclockedlang.PP.prog prog;
      raise Done;
    end

let try_compile file =
  match compile file with
  | () -> ()
  | exception Done -> Format.fprintf Format.std_formatter "@."

let anonymous file =
  if Filename.check_suffix file ".plu" then
    try_compile file
  else
    raise (Arg.Bad ("Can only compile *.plu files"))

let () =
  Arg.parse options anonymous usage

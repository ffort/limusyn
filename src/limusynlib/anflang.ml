open Common

type prog = decl list

and decl =
  {
    decl_desc: decl_desc;
    decl_loc: Location.t;
  }

and decl_desc =
  | Node of node_desc
  | EnumDecl of enum_desc

and node_desc =
  {
    node_name: String.t;
    node_inputs: vdecl list;
    node_outputs: vdecl list;
    node_locals: vdecl list;
    node_eqs: eq list;
  }

and enum_desc = String.t * StringSet.t

and eq =
  {
    eq_lhs: String.t list;
    eq_rhs: expr;
    eq_loc: Location.t;
  }

and 'desc expr_base =
  {
    expr_desc: 'desc;
    expr_loc: Location.t
  }

and expr = expr_desc expr_base

(* and atom_expr = atom_expr_desc expr_base *)

and const_expr = const_expr_desc expr_base

and ident_expr = ident_expr_desc expr_base

and expr_desc =
  [
    | `ExprConst of const
    | `ExprIdent of String.t
    | `ExprApply of ident_expr * ident_expr List.t
    | `ExprPckUp of rate_op_desc
    | `ExprFby of const_expr * rate_op_desc
    | `ExprWhen of when_desc
    | `ExprMerge of merge_desc
    | `ExprITE of ite_desc
  ]

(* and atom_expr_desc = *)
(*   [ *)
(*     | `ExprConst of const *)
(*     | `ExprIdent of String.t *)
(*   ] *)

and const_expr_desc =
  [
    | `ExprConst of const
  ]

and ident_expr_desc =
  [
    `ExprIdent of String.t
  ]

and vdecl =
  {
    vdecl_name: String.t;
    vdecl_ty_decl: Types.decl;
    vdecl_ck_decl: Commonclocks.decl;
    vdecl_dl: Int.t Option.t;
  }

and const =
  [
    | `ConstInt of Int.t
    | `ConstChar of Char.t
    | `ConstFloat of Float.t
    | `ConstEnum of enum_value
  ]

and enum_value = {variant: String.t; enum: String.t}

and rate_op_desc = {rate_expr: ident_expr; rate_factor: Int.t}

and when_desc = {when_expr: ident_expr; case: String.t; cond_expr: ident_expr;}

and merge_desc = {merge_var: ident_expr; merge_exprs: (String.t*ident_expr) list}

and ite_desc = {if_expr: ident_expr; then_expr: ident_expr; else_expr: ident_expr}

module Predef =
struct
  let prog =
    Env.String.empty
end

module Make :
sig
  val of_flat : Flatlang.prog -> prog
end

=
struct
  type enve =
    [
      | `Node of node_desc
      | `NIn of vdecl
      | `NOut of vdecl
      | `NLoc of vdecl
      | `EnumVariant of enum_desc
      | `Enum of enum_desc
    ]

  type env = enve Env.String.t

  type state =
    {
      next_tmp_var: int;
    }

  type node_state =
    {
      env : env;
      locals : vdecl list;
      eqs : eq list;
      state : state;
      added_decls : decl list;
    }

  let rec of_flat : Flatlang.prog -> prog =
    fun prog ->
    let state = init_state in
    Env.String.st_map decl_of_flat Predef.prog state prog
    |> List.concat

  and decl_of_flat : env -> state -> Flatlang.decl -> (string*enve) list * state * decl list =
    fun env state decl ->
    let decl_loc = decl.decl_loc in
    match decl.decl_desc with
    | Node ndesc ->
      let decls,state = node_of_flat env state decl_loc ndesc in
      let kvs = List.concat_map kvs_of_decl decls in
      kvs,state,decls
    | EnumDecl enum_desc ->
      let decls = [{decl_desc=EnumDecl enum_desc;decl_loc}] in
      let kvs = List.concat_map kvs_of_decl decls in
      kvs,state,decls

  and kvs_of_decl : decl -> (string * enve) list =
    function
    | {decl_desc=Node ({node_name;_} as ndesc);_} ->
      [(node_name, `Node ndesc)]
    | {decl_desc=EnumDecl ((enum_name, enum_variants) as enum_desc);_} ->
      let kvs =
        StringSet.to_seq enum_variants
        |> Seq.map
          (fun variant -> (variant, `EnumVariant enum_desc))
        |> List.of_seq
      in
      (enum_name, `Enum enum_desc)::kvs

  and node_of_flat :
    env -> state -> Location.t -> Flatlang.node_desc -> decl list * state =
    fun env state decl_loc ndesc ->
    let node_name = ndesc.node_name in
    let node_inputs = vdecls_of_flat ndesc.node_inputs in
    let node_outputs = vdecls_of_flat ndesc.node_outputs in
    let node_locals = vdecls_of_flat ndesc.node_locals in
    let env = List.fold_left
        (fun env (v:vdecl) ->
           Env.String.add v.vdecl_name (`NIn v) env)
        env node_inputs
    in
    let env = List.fold_left
        (fun env (v:vdecl) ->
           Env.String.add v.vdecl_name (`NLoc v) env)
        env node_locals
    in
    let env = List.fold_left
        (fun env (v:vdecl) ->
           Env.String.add v.vdecl_name (`NOut v) env)
        env node_outputs
    in
    let node_state =
      {
        env;
        locals = node_locals;
        eqs = [];
        state;
        added_decls = [];
      }
    in
    let node_state = eqs_of_flat node_state ndesc.node_eqs in
    let node_eqs = node_state.eqs in
    let node_locals = node_state.locals in
    let decl_desc = Node {node_name;node_inputs;node_outputs;node_locals;node_eqs} in
    let node_decl = {decl_desc; decl_loc} in
    let decls = node_state.added_decls@[node_decl] in
    decls,state

  and vdecls_of_flat : Flatlang.vdecl list -> vdecl list =
    fun vdecls ->
    List.map vdecl_of_flat vdecls

  and vdecl_of_flat : Flatlang.vdecl -> vdecl =
    fun v ->
    let vdecl_name = v.vdecl_name in
    let vdecl_ty_decl = v.vdecl_ty_decl in
    let vdecl_ck_decl = v.vdecl_ck_decl in
    let vdecl_dl = v.vdecl_dl in
    {vdecl_name; vdecl_ty_decl; vdecl_ck_decl; vdecl_dl}

  and eqs_of_flat : node_state -> Flatlang.eq list -> node_state =
    fun node_state eqs ->
    List.fold_left eq_of_flat node_state eqs

  and eq_of_flat: node_state -> Flatlang.eq -> node_state =
    fun node_state {eq_lhs;eq_rhs;eq_loc} ->
    let node_state,eq_rhs = expr_of_flat node_state eq_rhs in
    node_state_add_eq node_state eq_lhs eq_rhs eq_loc

  (* and auto_eq_of_flat: node_state -> Location.t -> Flatlang.auto_desc -> node_state = *)
  (*   fun node_state auto_loc {states} -> *)
  (*   let node_state,state_enum_name,state_enum_variants = *)
  (*     auto_state_enum_decl node_state states *)
  (*   in *)
  (*   let node_state = *)
  (*     decl_enum node_state *)
  (*       state_enum_name *)
  (*       (List.to_seq state_enum_variants) *)
  (*       auto_loc *)
  (*   in *)
  (*   let auto_vars = *)
  (*     List.fold_left *)
  (*       (fun acc {Flatlang.state_defs;_} -> *)
  (*          List.fold_left *)
  (*            (fun acc {Flatlang.eq_lhs;_} -> *)
  (*               List.fold_left (Fun.flip StringSet.add) acc eq_lhs) *)
  (*            acc state_defs) *)
  (*       StringSet.empty states *)
  (*     |> StringSet.to_seq *)
  (*     |> List.of_seq *)
  (*   in *)
  (*   let node_state,auto_prefix = safe_auto_prefix node_state in *)
  (*   let node_state,(main_state_names, main_state_exprs, ns_vars, s_vars,local_vars,local_exprs_merges) = *)
  (*     auto_main_vars node_state auto_loc auto_prefix states state_enum_variants auto_vars *)
  (*   in *)
  (*   let node_state,(ns_exprs, s_exprs) = auto_transitions node_state ns_vars s_vars state_enum_variants states in *)
  (*   let node_state, _ = ignore (node_state); failwith "TODO expressions" in *)
  (*   let vars = main_state_names@ns_vars@s_vars in *)
  (*   let exprs = main_state_exprs@ns_exprs@s_exprs in *)
  (*   let node_state,intro_vars = *)
  (*     introduce_exprs_as_eqs node_state vars exprs *)
  (*   in *)
  (*   if intro_vars <> vars then *)
  (*     failwith "Internal error"; *)
  (*   node_state *)

  (* and auto_main_vars : node_state -> Location.t -> String.t -> Flatlang.state list -> String.t list -> String.t List.t -> node_state * (String.t List.t * Flatlang.expr List.t * String.t List.t * String.t List.t * String.t List.t List.t * Flatlang.expr List.t) *)
  (*   = *)
  (*   fun node_state auto_loc auto_prefix _states state_enums auto_vars -> *)
  (*   let pns_var = safe_new_var node_state (auto_prefix^"_pns") in *)
  (*   let ns_var = safe_new_var node_state (auto_prefix ^ "_ns") in *)
  (*   let s_var = safe_new_var node_state (auto_prefix ^ "_s") in *)
  (*   let ns_vars = List.map *)
  (*       (fun variant -> *)
  (*          safe_new_var node_state (ns_var^"_"^variant)) *)
  (*       state_enums *)
  (*   in *)
  (*   let s_vars = List.map *)
  (*       (fun variant -> *)
  (*          safe_new_var node_state (s_var^"_"^variant)) *)
  (*       state_enums *)
  (*   in *)
  (*   let auto_local_vars = *)
  (*     (\* For each variable, the local variable for a given state *\) *)
  (*     List.map *)
  (*       (fun var -> *)
  (*          List.map *)
  (*            (fun variant -> *)
  (*               safe_new_var node_state (var^"_"^variant)) *)
  (*            state_enums) *)
  (*       auto_vars *)
  (*   in *)
  (*   let init_state = List.hd state_enums in *)
  (*   let pns_expr : Flatlang.expr = *)
  (*     {Flatlang.expr_desc = *)
  (*        `ExprFby *)
  (*          ({Flatlang.expr_desc = `ExprAtom (`AtomIdent init_state); *)
  (*            expr_loc = auto_loc;}, *)
  (*           {Flatlang.rate_expr = *)
  (*              {Flatlang.expr_desc = `ExprAtom (`AtomIdent ns_var); *)
  (*               expr_loc = auto_loc}; *)
  (*            rate_factor = 1}); *)
  (*      expr_loc = auto_loc} *)
  (*   in *)
  (*   let ns_expr : Flatlang.expr = *)
  (*     {Flatlang.expr_desc = `ExprMerge *)
  (*          {Flatlang.merge_var = *)
  (*             {Flatlang.expr_desc = `ExprAtom (`AtomIdent s_var); *)
  (*              expr_loc = auto_loc}; *)
  (*           merge_exprs = *)
  (*             List.combine state_enums ns_vars *)
  (*             |> List.map *)
  (*               (fun (variant,var) -> *)
  (*                  (variant, *)
  (*                   {Flatlang.expr_desc = `ExprAtom (`AtomIdent var); *)
  (*                    expr_loc = auto_loc}))}; *)
  (*      expr_loc = auto_loc} *)
  (*   in *)
  (*   let s_expr : Flatlang.expr = *)
  (*     {Flatlang.expr_desc = `ExprMerge *)
  (*          {Flatlang.merge_var = *)
  (*             {Flatlang.expr_desc = `ExprAtom (`AtomIdent pns_var); *)
  (*              expr_loc = auto_loc}; *)
  (*           merge_exprs = *)
  (*             List.combine state_enums s_vars *)
  (*             |> List.map *)
  (*               (fun (variant,var) -> *)
  (*                  (variant, *)
  (*                   {Flatlang.expr_desc = `ExprAtom (`AtomIdent var); *)
  (*                    expr_loc = auto_loc}))}; *)
  (*      expr_loc = auto_loc} *)
  (*   in *)
  (*   let auto_local_exprs : Flatlang.expr List.t = *)
  (*     List.map *)
  (*       (fun local_vars -> *)
  (*          {Flatlang.expr_desc = `ExprMerge *)
  (*               {Flatlang.merge_var = *)
  (*                  {Flatlang.expr_desc = `ExprAtom (`AtomIdent s_var); *)
  (*                   expr_loc = auto_loc}; *)
  (*                merge_exprs = *)
  (*                  List.map2 *)
  (*                    (fun variant var -> *)
  (*                       (variant, *)
  (*                        {Flatlang.expr_desc = `ExprAtom (`AtomIdent var); *)
  (*                        expr_loc = auto_loc})) *)
  (*                    state_enums local_vars}; *)
  (*           expr_loc = auto_loc;}) *)
  (*       auto_local_vars *)
  (*   in *)
  (*   let var_names = [pns_var; ns_var; s_var] in *)
  (*   let exprs = [pns_expr; ns_expr; s_expr] in *)
  (*   node_state, (var_names,exprs,ns_vars,s_vars,auto_local_vars,auto_local_exprs) *)

  (* and auto_transitions : node_state -> String.t List.t -> String.t List.t -> String.t List.t -> Flatlang.state List.t -> node_state * (Flatlang.expr List.t * Flatlang.expr List.t) *)
  (*   = *)
  (*   fun node_state _ns_vars _s_vars enum_variants states -> *)
  (*   let ns_exprs : Flatlang.expr List.t = *)
  (*     List.map2 *)
  (*       (fun enum_variant state -> *)
  (*          { *)
  (*            Flatlang.expr_desc = `ExprAtom (`AtomIdent enum_variant); *)
  (*            expr_loc = state.Flatlang.state_loc; *)
  (*          } *)
  (*       ) *)
  (*       enum_variants states *)
  (*   in *)
  (*   let s_exprs : Flatlang.expr List.t = *)
  (*     List.map2 *)
  (*       (fun enum_variant state -> *)
  (*          { *)
  (*            Flatlang.expr_desc = `ExprAtom (`AtomIdent enum_variant); *)
  (*            expr_loc = state.Flatlang.state_loc; *)
  (*          }) *)
  (*          enum_variants states *)
  (*   in *)
  (*   node_state, (ns_exprs,s_exprs) *)

  and expr_of_flat: node_state -> Flatlang.expr -> node_state * expr =
    fun node_state expr ->
    match expr with
    | {expr_desc=`ExprAtom ((`AtomInt _|`AtomChar _|`AtomFloat _) as a);expr_loc;_} ->
      let c = const_of_flat a in
      node_state,{expr_desc=`ExprConst c; expr_loc}
    | {expr_desc=`ExprAtom `AtomIdent id;expr_loc;_} ->
      begin
        match Env.String.curr node_state.env |> Env.String.Map.find_opt id with
        | Some (`Node _|`NIn _|`NOut _|`NLoc _) ->
          let node_state,expr = ident_expr_of_flat node_state expr in
          node_state,(expr :> expr)
        | Some `EnumVariant (_enum,_) ->
          let node_state,new_var = expr_as_eq_of_flat node_state expr in
          node_state,{expr_desc=`ExprIdent new_var;expr_loc}
        (* node_state, *)
        (* {expr_desc=`ExprConst (`ConstEnum {variant=id;enum});expr_loc} *)
        | Some `Enum _ | None ->
          failwith
            (Format.asprintf
               "%sIdentifier %s is not a value in scope"
               (MenhirLib.LexerUtil.range expr_loc) id)
      end
    (* | {expr_desc=`ExprAtom _;_} -> *)
    (*   let node_state,expr = ident_expr_of_flat node_state expr in *)
    (*   node_state,(expr :> expr) *)
    | {expr_desc=`ExprApply (f,args);expr_loc} ->
      let f = ident_expr_of_flat_ident f in
      let node_state,args =
        List.fold_left_map ident_expr_of_flat node_state args
      in
      node_state,{expr_desc=`ExprApply (f,args);expr_loc}
    | {expr_desc=`ExprPckUp {rate_expr;rate_factor};expr_loc} ->
      let node_state,rate_expr = ident_expr_of_flat node_state rate_expr in
      node_state,
      {expr_desc=`ExprPckUp {rate_expr;rate_factor};expr_loc}
    | {expr_desc=`ExprFby (cst,{rate_expr;rate_factor});expr_loc;_} ->
      let node_state,rate_expr = ident_expr_of_flat node_state rate_expr in
      let node_state,cst = constant_expr_of_flat node_state cst in
      node_state,
      {expr_desc=`ExprFby (cst,{rate_expr;rate_factor});expr_loc}
    | {expr_desc=`ExprWhen {when_expr;case;cond_expr};expr_loc;_} ->
      let node_state,when_expr = ident_expr_of_flat node_state when_expr in
      let cond_expr = ident_expr_of_flat_ident cond_expr in
      node_state,
      {expr_desc=`ExprWhen {when_expr;case;cond_expr};expr_loc}
    | {expr_desc=`ExprMerge {merge_var;merge_exprs};expr_loc} ->
      let merge_var = ident_expr_of_flat_ident merge_var in
      let node_state,merge_exprs =
        List.fold_left_map
          (fun node_state (case,e) ->
             let node_state,e = ident_expr_of_flat node_state e in
             node_state,(case,e))
          node_state
          merge_exprs
      in
      node_state,
      {expr_desc=`ExprMerge {merge_var;merge_exprs};expr_loc}
    | {expr_desc=`ExprITE {if_expr;then_expr;else_expr};expr_loc} ->
      let node_state,if_expr = ident_expr_of_flat node_state if_expr in
      let node_state,then_expr = ident_expr_of_flat node_state then_expr in
      let node_state,else_expr = ident_expr_of_flat node_state else_expr in
      node_state,
      {expr_desc=`ExprITE {if_expr;then_expr;else_expr};expr_loc}

  and ident_expr_of_flat: node_state -> Flatlang.expr -> node_state * ident_expr =
    fun node_state expr ->
    let expr_loc = expr.expr_loc in
    match expr.expr_desc with
    (* | `ExprAtom ((`AtomInt _|`AtomChar _|`AtomFloat _) as a) -> *)
    (*   let c = const_of_flat a in *)
    (*   node_state,{expr_desc=`ExprConst c; expr_loc} *)
    | `ExprAtom `AtomIdent id ->
      begin
        match Env.String.curr node_state.env |> Env.String.Map.find_opt id with
        | Some (`Node _|`NIn _|`NOut _|`NLoc _) ->
          node_state,{expr_desc=`ExprIdent id;expr_loc}
        | Some `EnumVariant (_enum,_) ->
          let node_state,new_var = expr_as_eq_of_flat node_state expr in
          node_state,{expr_desc=`ExprIdent new_var;expr_loc}
        (* node_state, *)
        (* {expr_desc=`ExprConst (`ConstEnum {variant=id;enum});expr_loc} *)
        | Some `Enum _ | None ->
          failwith
            (Format.asprintf
               "%sIdentifier %s is not a value in scope"
               (MenhirLib.LexerUtil.range expr_loc) id)
      end
    | `ExprAtom _ | `ExprApply _ | `ExprITE _
    | `ExprPckUp _ | `ExprFby _
    | `ExprWhen _ | `ExprMerge _  ->
      let node_state,new_var = expr_as_eq_of_flat node_state expr in
      node_state,{expr_desc=`ExprIdent new_var;expr_loc}

  and constant_expr_of_flat: node_state -> Flatlang.atom_expr -> node_state * const_expr =
    fun node_state expr ->
    let expr_loc = expr.expr_loc in
    match expr.expr_desc with
    | `ExprAtom ((`AtomInt _|`AtomChar _|`AtomFloat _) as a)  ->
      let c = const_of_flat a in
      node_state,{expr_desc=`ExprConst c; expr_loc}
    | `ExprAtom `AtomIdent id ->
      begin
        match Env.String.curr node_state.env |> Env.String.Map.find_opt id with
        | Some `EnumVariant (enum,_) ->
          node_state,
          {expr_desc=`ExprConst (`ConstEnum {variant=id;enum});expr_loc}
        | Some (`Enum _|`Node _|`NIn _|`NOut _|`NLoc _) | None ->
          failwith @@ Format.sprintf
            "%sIdentifier %s is not a constant"
            (MenhirLib.LexerUtil.range expr_loc) id
      end

  and ident_expr_of_flat_ident: Flatlang.ident_expr -> ident_expr =
    fun {expr_desc=`ExprAtom (`AtomIdent id);expr_loc} ->
    {expr_desc=`ExprIdent id;expr_loc}

  and expr_as_eq_of_flat: node_state -> Flatlang.expr -> node_state * String.t =
    fun node_state expr ->
    let node_state,var_name = new_tmp_var node_state in
    introduce_expr_as_eq node_state var_name expr

  and introduce_exprs_as_eqs: node_state -> String.t List.t -> Flatlang.expr List.t -> node_state * String.t List.t
  (* Introduce potentially mutually dependent exprs as eqs ie
     introduce_exprs_as_eqs node_state ["v1"; "v2"] [ expr "v2"; expr "v1" ]
     will add the equations
     v1 = v2;
     v2 = v1;
     (Even if this example is not really sound)
  *)
    =
    fun node_state var_names exprs ->
    let var_names =
      List.map (safe_new_var node_state) var_names
    in
    let new_locals =
      List.map
        (fun var_name ->
           {
             vdecl_name = var_name;
             vdecl_ty_decl = Types.DeclAny;
             vdecl_ck_decl = Commonclocks.DeclAny;
             vdecl_dl = None;
           })
        var_names
    in
    let locals = List.rev_append new_locals node_state.locals in
    let env =
      List.fold_left
        (fun env v ->
           Env.String.add v.vdecl_name (`NLoc v) env)
        node_state.env new_locals
    in
    let node_state = {node_state with locals; env} in
    let node_state,exprs = List.fold_left_map expr_of_flat node_state exprs in
    let new_eqs =
      List.map2
        (fun v eq_rhs -> {eq_lhs=[v];eq_rhs;eq_loc=eq_rhs.expr_loc})
        var_names exprs
    in
    let eqs = List.rev_append new_eqs node_state.eqs in
    let node_state = {node_state with eqs} in
    node_state,var_names

  and introduce_expr_as_eq: node_state -> String.t -> Flatlang.expr -> node_state * String.t =
    fun node_state var_name expr ->
    match introduce_exprs_as_eqs node_state [var_name] [expr] with
    | node_state,var_name::_ -> node_state,var_name
    | _ -> failwith "Unreachable"

  and const_of_flat =
    fun atom ->
    match atom with
    | `AtomInt i -> `ConstInt i
    | `AtomChar c -> `ConstChar c
    | `AtomFloat f -> `ConstFloat f

  (* and decl_enum : node_state -> String.t -> String.t Seq.t -> Location.t -> node_state *)
  (*   = *)
  (*   fun node_state enum_name variants decl_loc -> *)
  (*   let variants = StringSet.of_seq variants in *)
  (*   let enum_desc = (enum_name, variants) in *)
  (*   let decl_desc = EnumDecl  enum_desc in *)
  (*   let decl = {decl_desc;decl_loc} in *)
  (*   let added_decls = decl::node_state.added_decls in *)
  (*   let env = Env.String.add enum_name (`Enum enum_desc) node_state.env in *)
  (*   let env = *)
  (*     StringSet.fold *)
  (*       (fun variant env -> *)
  (*          Env.String.add variant (`EnumVariant enum_desc) env) *)
  (*       variants env *)
  (*   in *)
  (*   {node_state with added_decls; env} *)

  (* and auto_state_enum_decl : node_state -> Flatlang.state list -> _ = *)
  (*   fun node_state states -> *)
  (*   let node_state,(enum_name,enum_prefix) = *)
  (*     safe_enum node_state states *)
  (*   in *)
  (*   let enums = *)
  (*     List.map *)
  (*       (fun state -> enum_prefix ^ state.Flatlang.state_name) *)
  (*       states *)
  (*   in *)
  (*   node_state,enum_name,enums *)

  and init_state =
    {
      next_tmp_var = 0;
    }

  and new_tmp_var node_state =
    let var = tmp_var_id node_state.state in
    let state = incr_next_tmp_var node_state.state in
    let node_state = {node_state with state} in
    if Env.String.curr node_state.env |> Env.String.Map.mem var then
      new_tmp_var node_state
    else
      node_state, var

  and safe_new_var node_state var =
    if Env.String.curr node_state.env |> Env.String.Map.mem var then
      safe_new_var_cnt node_state var 0
    else
      var

  and safe_new_var_cnt node_state prefix cnt =
    let var = Format.sprintf "%s%i" prefix cnt in
    if Env.String.curr node_state.env |> Env.String.Map.mem var then
      safe_new_var_cnt node_state prefix (cnt+1)
    else
      var

  and incr_next_tmp_var state =
    {next_tmp_var = state.next_tmp_var+1}

  (* and incr_next_enum state = *)
  (*   {state with next_enum = state.next_enum+1} *)

  (* and safe_enum node_state states = *)
  (*   let enum_num = node_state.state.next_enum in *)
  (*   let enum_name = Format.sprintf "state%i" enum_num in *)
  (*   let enum_prefix = Format.sprintf "State%i" enum_num in *)
  (*   let state = incr_next_enum node_state.state in *)
  (*   let node_state = {node_state with state} in *)
  (*   let is_safe = *)
  (*     Env.String.curr node_state.env *)
  (*     |> Env.String.Map.for_all *)
  (*       (fun k _ -> *)
  (*          not (string_sub k enum_prefix || string_sub k enum_prefix)) *)
  (*   in *)
  (*   if is_safe then *)
  (*     node_state,(enum_name,enum_prefix) *)
  (*   else *)
  (*     safe_enum node_state states *)

  (* and safe_auto_prefix node_state = *)
  (*   let auto_num = node_state.state.next_auto_id in *)
  (*   let auto_id = Format.sprintf "auto%i" auto_num in *)
  (*   let state = incr_auto_id node_state.state in *)
  (*   let has_collision = *)
  (*     Env.String.curr node_state.env *)
  (*     |> Env.String.Map.exists *)
  (*       (fun k _ -> string_sub k auto_id) *)
  (*   in *)
  (*   let node_state = {node_state with state} in *)
  (*   if has_collision then *)
  (*     safe_auto_prefix node_state *)
  (*   else *)
  (*     node_state,auto_id *)

  (* and incr_auto_id state = *)
  (*   {state with next_auto_id = state.next_auto_id + 1} *)

  and tmp_var_id state =
    let next_tmp_var = state.next_tmp_var in
    Format.asprintf "v%i" next_tmp_var

  and node_state_add_eq node_state eq_lhs eq_rhs eq_loc =
    let eqs = {eq_lhs;eq_rhs;eq_loc}::node_state.eqs in
    {node_state with eqs}

end

module PP =
struct
  let rec prog =
    fun ff p ->
      Print.list "@[<v>" "@ @ " "@ @]" decl ff p

  and decl =
    fun ff (d:decl) ->
      match d.decl_desc with
      | Node ndesc -> node ff ndesc
      | EnumDecl (enum_name,enum_variants) -> enum ff enum_name enum_variants

  and node =
    fun ff {node_name;node_inputs;node_outputs;node_locals;node_eqs} ->
      Format.fprintf ff "@[<v>";
      Format.fprintf ff "@[<hov 2>node@ %s@ " node_name;
      Print.list "@[<hov 2>(" ";@ " ")@]" vdecl ff node_inputs;
      Format.fprintf ff "@ returns@ ";
      Print.list "@[<hov 2>(" ";@ " ")@]" vdecl ff node_outputs;
      Format.fprintf ff "@]@ ";
      Print.list "var @[<hov>" "@ " "@]@ " (Print.post "%a;" vdecl) ff node_locals;
      Print.list "@[<v 2>let@ " "@ " "@]@ tel" eq ff node_eqs;
      Format.fprintf ff "@]"

  and enum =
    fun ff enum_name enum_variants ->
      Format.fprintf ff "@[<v 2>@[<h>type@ %s@ =@]%a@]"
        enum_name
        (Print.seq "@ | " "@ | " "" Format.pp_print_string)
        (StringSet.to_seq enum_variants)

  and eq =
    fun ff {eq_lhs;eq_rhs;_} ->
      Format.fprintf ff "@[<hov 2>%a@ =@ %a;@]"
        (Print.list "@[<hov>" ",@," "@]" Format.pp_print_string) eq_lhs
        expr eq_rhs

  and expr =
    fun ff e ->
      match e.expr_desc with
      | `ExprConst `ConstInt i -> Format.fprintf ff "%i" i
      | `ExprConst `ConstChar c -> Format.fprintf ff "%c" c
      | `ExprConst `ConstFloat f -> Format.fprintf ff "%f" f
      | `ExprConst `ConstEnum {variant;_} -> Format.fprintf ff "%s" variant
      | `ExprIdent id -> Format.fprintf ff "%s" id
      | `ExprApply (f,args) ->
        Format.fprintf ff "%a(%a)"
          expr (f :> expr)
          (Print.list
             "@[<hov 2>" ",@ " "@]"
             expr)
          (args :> expr List.t)
      | `ExprPckUp {rate_expr;rate_factor} ->
        Format.fprintf ff "%a *^ %i"
          expr (rate_expr :> expr) rate_factor
      | `ExprFby (cst,{rate_expr;rate_factor=_}) ->
        Format.fprintf ff "%a fby %a"
          expr (cst :> expr)
          expr (rate_expr :> expr)
      | `ExprWhen {when_expr;case;cond_expr} ->
        Format.fprintf ff "%a when %s->%a"
          expr (when_expr :> expr)
          case
          expr (cond_expr :> expr)
      | `ExprMerge {merge_var; merge_exprs} ->
        Format.fprintf ff "merge(%a, %a)"
          expr (merge_var :> expr)
          (Print.list
             "@[<hov 2>" "@ " "@]"
             (fun ff (case,e) ->
                Format.fprintf ff "%s->%a" case expr (e :> expr)))
          merge_exprs
      | `ExprITE {if_expr;then_expr;else_expr} ->
        Format.fprintf ff "if %a then %a else %a"
          expr (if_expr :> expr)
          expr (then_expr :> expr)
          expr (else_expr :> expr)

  and vdecl =
    fun ff v ->
      Format.fprintf ff "@[<hov>%s: %a %a %a@]"
        v.vdecl_name
        Types.PP.decl v.vdecl_ty_decl
        Structclocks.PP.decl v.vdecl_ck_decl
        dl v.vdecl_dl

  and dl =
    fun ff d ->
      Print.option
        (fun ff i -> Format.fprintf ff "due %i" i)
        ""
        ff d
end

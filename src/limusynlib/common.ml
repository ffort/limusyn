module StringMap = Map.Make(String)
module IntMap = Map.Make(Int)

module StringSet = Set.Make(String)

let string_sub =
  fun base sub ->
  if String.length sub > String.length base then
    false
  else
    let base_sub = String.sub base 0 (String.length base) in
    String.equal base_sub sub

let list_transpose =
  fun m ->
  let inner_len = List.length (List.hd m) in
  let start = List.init inner_len (fun _ -> []) in
  let step =
    List.fold_left
      (fun acc l ->
         List.map2
           (fun acc x -> x::acc)
           acc l)
      start m
  in
  List.map List.rev step

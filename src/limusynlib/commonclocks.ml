type decl =
  | DeclAny
  | Decl of decl_desc

and decl_desc =
  | Pck of pck
  | On of on_decl

and pck = {period: Int.t; offset: Int.t}

and on_decl = {decl: decl; decl_case: String.t; decl_cond: String.t}

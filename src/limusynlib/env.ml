(** Generic inference environments *)

module type S =
sig
  type key

  type 'a t

  module Map : Map.S

  val empty: 'a t

  val add: key -> 'a -> 'a t -> 'a t

  val curr: 'a t -> 'a Map.t

  val rollback_until: key -> 'a t -> 'a t

  val iter_from_list: ('a t -> 'b -> ((key * 'a) list)) -> 'b list -> 'a t -> unit

  val at_point: ('a t -> 'b -> (key * 'a) list) -> ('a -> 'a t -> 'c) -> key -> 'b list -> 'a t -> 'c

  val st_map: ('a t -> 'b -> 'c -> ((key * 'a) list * 'b * 'd)) -> 'a t -> 'b -> 'c list -> 'd list
end

module Make (Key: Map.OrderedType) = struct
  type key = Key.t

  module Map = Map.Make(Key)

  type 'a t = {
    current: 'a Map.t;
    histories: (key * ('a Map.t)) list;
  }

  exception Shadow of key

  let empty = {current = Map.empty; histories = []}

  let add k v env =
    if List.mem k (List.map fst env.histories) then
      raise (Shadow k)
    else
      {current = Map.add k v env.current; histories = (k,env.current)::env.histories}

  let curr env = env.current

  let rollback_until k env =
    let rec aux k histories =
      match histories with
      | (x,e)::envs when x=k -> {current = e; histories = envs}
      | _::envs -> aux k envs
      | [] -> raise Not_found
    in
    aux k env.histories

  let iter_from_list f l env =
    let rec aux f l env =
      match l with
      | [] -> ()
      | x::xs ->
        let kvs = f env x in
        let env = List.fold_left
            (fun acc (k,v) -> add k v acc) env kvs
        in
        aux f xs env
    in
    aux f l env

  let at_point fenv fpoint k l env =
    let rec aux fenv fpoint k l env =
      match l with
      | [] -> raise Not_found
      | x::xs ->
        let kvs = fenv env x in
        match List.assoc_opt k kvs with
        | Some v -> fpoint v env
        | None ->
          let map = List.fold_left
              (fun acc (k,v) -> add k v acc) env kvs
          in
          aux fenv fpoint k xs map
    in
    aux fenv fpoint k l env

  let st_map =
    let rec aux f env st l acc =
      match l with
      | [] -> List.rev acc
      | x::xs ->
        let kvs,st,mapped = f env st x in
        let env = List.fold_left (fun acc (k,v) -> add k v acc) env kvs in
        aux f env st xs (mapped::acc)
    in
    fun f env st l ->
      aux f env st l []

end

module String = Make(String)

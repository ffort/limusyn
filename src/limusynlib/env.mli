module type S =
sig
  type key

  type 'a t

  module Map : Map.S

  val empty: 'a t

  val add: key -> 'a -> 'a t -> 'a t

  val curr: 'a t -> 'a Map.t

  val rollback_until: key -> 'a t -> 'a t

  val iter_from_list: ('a t -> 'b -> ((key * 'a) list)) -> 'b list -> 'a t -> unit

  val at_point: ('a t -> 'b -> (key * 'a) list) -> ('a -> 'a t -> 'c) -> key -> 'b list -> 'a t -> 'c

  val st_map: ('a t -> 'b -> 'c -> ((key * 'a) list * 'b * 'd)) -> 'a t -> 'b -> 'c list -> 'd list
end

module Make (Key: Map.OrderedType) : S with
  type key = Key.t and type Map.key = Key.t

module String : S with
  type key = String.t and type Map.key = String.t

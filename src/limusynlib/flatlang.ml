open Common

type prog = decl list

and decl =
  {
    decl_desc: decl_desc;
    decl_loc: Location.t;
  }

and decl_desc =
  | Node of node_desc
  | EnumDecl of enum_desc

and node_desc =
  {
    node_name: String.t;
    node_inputs: vdecl list;
    node_outputs: vdecl list;
    node_locals: vdecl list;
    node_eqs: eq list;
  }

and enum_desc = String.t * StringSet.t

and eq =
  {
    eq_lhs: String.t list;
    eq_rhs: expr;
    eq_loc: Location.t;
  }

and 'desc expr_base =
  {
    expr_desc: 'desc;
    expr_loc: Location.t;
  }

and expr = expr_desc expr_base

and atom_expr = atom_expr_desc expr_base

and ident_expr = ident_expr_desc expr_base

and expr_desc =
  [
    | `ExprAtom of atom
    | `ExprApply of ident_expr * expr list
    | `ExprPckUp of rate_op_desc
    | `ExprFby of atom_expr * rate_op_desc
    | `ExprWhen of when_desc
    | `ExprMerge of merge_desc
    | `ExprITE of ite_desc
  ]

and atom_expr_desc =
  [
    | `ExprAtom of atom
  ]

and ident_expr_desc =
  [
    | `ExprAtom of ident_desc
  ]

and vdecl =
  {
    vdecl_name: String.t;
    vdecl_ty_decl: Types.decl;
    vdecl_ck_decl: Commonclocks.decl;
    vdecl_dl: Int.t Option.t;
  }

and atom =
  [
    | `AtomInt of Int.t
    | `AtomChar of Char.t
    | `AtomFloat of Float.t
    | `AtomIdent of String.t
  ]

and ident_desc =
  [
    | `AtomIdent of String.t
  ]

and rate_op_desc = {rate_expr: expr; rate_factor: Int.t}

and when_desc = {when_expr: expr; case: String.t; cond_expr: ident_expr;}

and merge_desc = {merge_var: ident_expr; merge_exprs: (String.t*expr) list}

and ite_desc = {if_expr: expr; then_expr: expr; else_expr: expr}

module PP =
struct

  let rec prog =
    fun ff p ->
      Print.list "@[<v>" "@ @ " "@ @]" decl ff p

  and decl =
    fun ff (d:decl) ->
      match d.decl_desc with
      | Node ndesc -> node ff ndesc
      | EnumDecl (enum_name,enum_variants) -> enum ff enum_name enum_variants

  and node =
    fun ff {node_name;node_inputs;node_outputs;node_locals;node_eqs} ->
      Format.fprintf ff "@[<v>";
      Format.fprintf ff "@[<hov 2>node@ %s@ " node_name;
      Print.list "@[<hov 2>(" ";@ " ")@]" vdecl ff node_inputs;
      Format.fprintf ff "@ returns@ ";
      Print.list "@[<hov 2>(" ";@ " ")@]" vdecl ff node_outputs;
      Format.fprintf ff "@]@ ";
      Print.list "var @[<hov>" "@ " "@]@ " (Print.post "%a;" vdecl) ff node_locals;
      Print.list "@[<v 2>let@ " "@ " "@]@ tel" eq ff node_eqs;
      Format.fprintf ff "@]"

  and enum =
    fun ff enum_name enum_variants ->
      Format.fprintf ff "@[<v 2>@[<h>type@ %s@ =@]%a@]"
        enum_name
        (Print.seq "@ | " "@ | " "" Format.pp_print_string)
        (StringSet.to_seq enum_variants)

  and eq =
    fun ff {eq_lhs;eq_rhs;_} ->
      Format.fprintf ff "@[<hov 2>%a@ =@ %a;@]"
        (Print.list "@[<hov>" ",@," "@]" Format.pp_print_string) eq_lhs
        expr eq_rhs

  and expr =
    fun ff e ->
      match e.expr_desc with
      | `ExprAtom `AtomInt i -> Format.fprintf ff "%i" i
      | `ExprAtom `AtomChar c -> Format.fprintf ff "%c" c
      | `ExprAtom `AtomFloat f -> Format.fprintf ff "%f" f
      | `ExprAtom `AtomIdent id -> Format.fprintf ff "%s" id
      | `ExprApply (f,args) ->
        Format.fprintf ff "%a(%a)"
          expr (f :> expr)
          (Print.list
             "@[<hov 2>" ",@ " "@]"
             expr)
          args
      | `ExprPckUp {rate_expr;rate_factor} ->
        Format.fprintf ff "%a *^ %i"
          expr (rate_expr :> expr) rate_factor
      | `ExprFby (cst,{rate_expr;rate_factor=_}) ->
        Format.fprintf ff "%a fby %a"
          expr (cst :> expr)
          expr (rate_expr :> expr)
      | `ExprWhen {when_expr;case;cond_expr} ->
        Format.fprintf ff "%a when %s->%a"
          expr (when_expr :> expr)
          case
          expr (cond_expr :> expr)
      | `ExprMerge {merge_var; merge_exprs} ->
        Format.fprintf ff "merge(%a, %a)"
          expr (merge_var :> expr)
          (Print.list
             "@[<hov 2>" "@ " "@]"
             (fun ff (case,e) ->
                Format.fprintf ff "%s->%a" case expr (e :> expr)))
          merge_exprs
      | `ExprITE {if_expr; then_expr; else_expr} ->
        Format.fprintf ff "if %a then %a else %a"
          expr if_expr
          expr then_expr
          expr else_expr

  and vdecl =
    fun ff v ->
      Format.fprintf ff "@[<hov>%s: %a %a %a@]"
        v.vdecl_name
        Types.PP.decl v.vdecl_ty_decl
        Structclocks.PP.decl v.vdecl_ck_decl
        dl v.vdecl_dl

  and dl =
    fun ff d ->
      Print.option
        (fun ff i -> Format.fprintf ff "due %i" i)
        ""
        ff d
end

module Predef =
struct
  let prog =
    Env.String.empty
end

module Make :
sig
  val of_parsed : Parsedlang.prog -> prog
end
=
struct
  type enve =
    | NName of node_desc
    | NIn of vdecl
    | NOut of vdecl
    | NLoc of vdecl
    | EnumVariant of enum_desc
    | Enum of enum_desc

  type env = enve Env.String.t

  type state =
    {
      next_enum: int;
      next_auto: int;
    }

  type node_state =
    {
      env : env;
      locals : vdecl list;
      eqs : eq list;
      state : state;
      added_decls : decl list;
    }

  let rec of_parsed : Parsedlang.prog -> prog =
    fun prog ->
    let state = init_state in
    Env.String.st_map decl_of_parsed Predef.prog state prog
    |> List.concat

  and decl_of_parsed : env -> state -> Parsedlang.top_decl -> (String.t *enve) list * state * decl list =
    fun env state decl ->
    let decl_loc = decl.top_decl_loc in
    let state,decls =
      match decl.top_decl_desc with
      | Node ndesc ->
        node_of_parsed env state decl_loc ndesc
      | EnumDecl (enum,variants) ->
        state,[{decl_desc=EnumDecl (enum,variants); decl_loc}]
    in
    let kvs = List.concat_map kvs_of_decl decls in
    kvs,state,decls

  and kvs_of_decl : decl -> (string * enve) list =
    function
    | {decl_desc=Node ({node_name;_} as ndesc);_} ->
      [(node_name, NName ndesc)]
    | {decl_desc=EnumDecl ((enum_name, enum_variants) as enum_desc);_} ->
      let kvs =
        StringSet.to_seq enum_variants
        |> Seq.map
          (fun variant -> (variant, EnumVariant enum_desc))
        |> List.of_seq
      in
      (enum_name, Enum enum_desc)::kvs

  and node_of_parsed :
    env -> state -> Location.t  -> Parsedlang.node_desc -> state * decl list =
    fun env state decl_loc ndesc ->
    let node_name = ndesc.node_name in
    let node_inputs = vdecls_of_parsed ndesc.node_inputs in
    let node_outputs = vdecls_of_parsed ndesc.node_outputs in
    let node_locals = vdecls_of_parsed ndesc.node_locals in
    let env = List.fold_left
        (fun env (v:vdecl) ->
           Env.String.add v.vdecl_name (NIn v) env)
        env node_inputs
    in
    let env = List.fold_left
        (fun env (v:vdecl) ->
           Env.String.add v.vdecl_name (NLoc v) env)
        env node_locals
    in
    let env = List.fold_left
        (fun env (v:vdecl) ->
           Env.String.add v.vdecl_name (NOut v) env)
        env node_outputs
    in
    let node_state =
      {
        env;
        locals = node_locals;
        eqs = [];
        state;
        added_decls = [];
      }
    in
    let node_state = eqs_of_parsed node_state ndesc.node_defs in
    let node_eqs = node_state.eqs in
    let node_locals = node_state.locals in
    let decl_desc = Node {node_name;node_inputs;node_outputs;node_locals;node_eqs} in
    let node_decl = {decl_desc; decl_loc} in
    let decls = node_state.added_decls@[node_decl] in
    state,decls

  and vdecls_of_parsed : Parsedlang.vdecl list -> vdecl list =
    fun vdecls ->
    List.map vdecl_of_parsed vdecls

  and vdecl_of_parsed : Parsedlang.vdecl -> vdecl =
    fun v ->
    let vdecl_name = v.vdecl_name in
    let vdecl_ty_decl = v.vdecl_ty_decl in
    let vdecl_ck_decl = v.vdecl_ck_decl in
    let vdecl_dl = v.vdecl_dl in
    {vdecl_name; vdecl_ty_decl; vdecl_ck_decl; vdecl_dl}

  and eqs_of_parsed : node_state -> Parsedlang.def list -> node_state =
    fun node_state defs ->
    List.fold_left
      eqs_of_def_parsed
      node_state defs

  and eqs_of_def_parsed : node_state -> Parsedlang.def -> node_state =
    fun node_state ->
    function
    | {def_desc=Eq eq_desc;def_loc;_} ->
      eq_of_parsed node_state def_loc eq_desc
    | {def_desc=Automaton auto_desc;def_loc;_} ->
      auto_eq_of_parsed node_state def_loc auto_desc

  and eq_of_parsed : node_state -> Location.t -> Parsedlang.eq_desc -> node_state =
    fun node_state eq_loc {eq_lhs;eq_rhs} ->
    let eq_rhs = expr_of_parsed eq_rhs in
    node_state_add_eq node_state eq_lhs eq_rhs eq_loc

  and auto_eq_of_parsed : node_state -> Location.t -> Parsedlang.auto_desc -> node_state =
    fun node_state auto_loc {states} ->
    let node_state,state_enum_name,state_enum_variants =
      auto_state_enum_decl node_state states
    in
    let state2enum_map =
      List.fold_left2
        (fun acc {Parsedlang.state_name;_} variant ->
           StringMap.add state_name variant acc)
        StringMap.empty
        states state_enum_variants
    in
    let node_state =
      decl_enum node_state
        state_enum_name
        state_enum_variants
        auto_loc
    in
    let init_state = List.hd state_enum_variants in
    let def_vars =
      (List.hd states).state_defs
      |> List.concat_map (fun {Parsedlang.eq_lhs;_} -> eq_lhs)
    in
    let node_state,auto_prefix,state_prefixes =
      safe_auto_prefixes node_state states
    in
    let node_state,(main_vars,ns_vars,s_vars,local_vars) =
      auto_vars node_state auto_loc auto_prefix state_prefixes states
    in
    let _,ns_var,s_var = main_vars in
    let main_vars_exprs =
      auto_main_var_exprs auto_loc init_state state_enum_variants ns_vars s_vars main_vars
    in
    let merge_vars_exprs =
      auto_output_exprs auto_loc s_var state_enum_variants local_vars
    in
    let state_exprs =
      auto_project_exprs s_var states state_enum_variants
    in
    let ns_exprs =
      auto_weak_transitions s_var states state2enum_map
    in
    let s_exprs =
      auto_strong_transitions ns_var states state2enum_map
    in
    auto_add_vars_exprs
      node_state auto_loc
      main_vars main_vars_exprs
      def_vars merge_vars_exprs
      local_vars state_exprs
      s_vars s_exprs
      ns_vars ns_exprs

  and expr_of_parsed : Parsedlang.expr -> expr =
    function
    | ({expr_desc=`ExprAtom _;_} as expr) ->
      ((atom_expr_of_parsed expr) :> expr)
    | {expr_desc=`ExprApply (f,args);expr_loc} ->
      let f = ident_expr_of_parsed f in
      let args = List.map expr_of_parsed args in
      {expr_desc=`ExprApply (f,args); expr_loc}
    | {expr_desc=`ExprPckUp {rate_expr;rate_factor}; expr_loc} ->
      let rate_expr = expr_of_parsed rate_expr in
      {expr_desc=`ExprPckUp {rate_expr;rate_factor}; expr_loc}
    | {expr_desc=`ExprFby (cst,{rate_expr;rate_factor}); expr_loc} ->
      let rate_expr = expr_of_parsed rate_expr in
      let cst = atom_expr_of_parsed cst in
      {expr_desc=`ExprFby (cst, {rate_expr;rate_factor}); expr_loc}
    | {expr_desc=`ExprWhen {when_expr;case;cond_expr};expr_loc} ->
      let when_expr = expr_of_parsed when_expr in
      let cond_expr = ident_expr_of_parsed cond_expr in
      {expr_desc=`ExprWhen {when_expr;case;cond_expr}; expr_loc}
    | {expr_desc=`ExprMerge {merge_var; merge_exprs}; expr_loc} ->
      let merge_var = ident_expr_of_parsed merge_var in
      let merge_exprs =
        List.map
          (fun (v,e) -> (v,expr_of_parsed e))
          merge_exprs
      in
      {expr_desc=`ExprMerge {merge_var; merge_exprs}; expr_loc}

  and atom_expr_of_parsed : Parsedlang.atom_expr -> atom_expr =
    function
    | ({expr_desc=`ExprAtom `AtomIdent _;_} as expr) ->
      ((ident_expr_of_parsed expr) :> atom_expr)
    | {expr_desc=`ExprAtom a; expr_loc} ->
      {expr_desc=`ExprAtom a; expr_loc}

  and ident_expr_of_parsed : Parsedlang.ident_expr -> ident_expr =
    function
      {expr_desc=`ExprAtom `AtomIdent ident;expr_loc} ->
      {expr_desc=`ExprAtom (`AtomIdent ident);expr_loc}

  and auto_vars : node_state -> Location.t -> String.t -> String.t List.t -> Parsedlang.state List.t -> node_state * _ =
    fun node_state _auto_loc auto_prefix state_prefixes states ->
    let pns_var = auto_prefix^"_pns" in
    let ns_var = auto_prefix ^ "_ns" in
    let s_var = auto_prefix ^ "_s" in
    let ns_vars =
      List.map (fun prefix -> prefix^"_ns") state_prefixes
    in
    let s_vars =
      List.map (fun prefix -> prefix^"_s") state_prefixes
    in
    let auto_local_vars =
      List.map2
        (fun prefix {Parsedlang.state_defs;_} ->
           List.concat_map
             (fun {Parsedlang.eq_lhs;_} ->
             List.map (fun v -> prefix^"_"^v) eq_lhs)
             state_defs)
        state_prefixes states
    in
    node_state,((pns_var,ns_var,s_var),ns_vars,s_vars,auto_local_vars)

  and auto_main_var_exprs : Location.t -> String.t -> String.t List.t -> String.t List.t -> String.t List.t -> (String.t * String.t * String.t) -> (expr * expr * expr) =
    fun auto_loc init_state state_enums ns_vars s_vars (pns_var,ns_var,s_var) ->
    let expr_loc = auto_loc in
    let pns_expr : expr =
      {expr_desc =`ExprFby (
           {expr_desc = `ExprAtom (`AtomIdent init_state);
            expr_loc},
           {rate_expr = {expr_desc=`ExprAtom (`AtomIdent ns_var); expr_loc};
            rate_factor=1});
       expr_loc;}
    in
    let ns_expr : expr =
      {expr_desc=`ExprMerge
           {merge_var={expr_desc=`ExprAtom (`AtomIdent s_var);
                       expr_loc};
            merge_exprs =
              List.map2
                (fun variant var ->
                   (variant,
                    {expr_desc=`ExprAtom (`AtomIdent var);
                     expr_loc}))
                state_enums ns_vars};
      expr_loc;}
    in
    let s_expr : expr =
      {expr_desc=`ExprMerge
           {merge_var={expr_desc=`ExprAtom (`AtomIdent pns_var);
                       expr_loc};
            merge_exprs =
              List.map2
                (fun variant var ->
                   (variant,
                    {expr_desc=`ExprAtom (`AtomIdent var);
                     expr_loc}))
                state_enums s_vars};
       expr_loc;}
    in
    (pns_expr,ns_expr,s_expr)

  and auto_output_exprs : Location.t -> String.t -> String.t List.t -> String.t List.t List.t -> expr List.t =
    fun auto_loc s_var state_enums local_vars ->
    let expr_loc = auto_loc in
    let merged_vars = list_transpose local_vars in
    List.map
      (fun local_vars ->
         {expr_desc=`ExprMerge
              {merge_var={expr_desc=`ExprAtom (`AtomIdent s_var);
                          expr_loc};
               merge_exprs=
                 List.map2
                   (fun variant var ->
                      (variant,
                       {expr_desc=`ExprAtom (`AtomIdent var);
                        expr_loc}))
                   state_enums local_vars};
          expr_loc})
      merged_vars

  and auto_project_exprs : String.t -> Parsedlang.state List.t -> String.t List.t -> expr List.t List.t =
    fun s_var states state_enums ->
    List.map2
      (fun {Parsedlang.state_defs;_} enum ->
         List.map
           (fun {Parsedlang.eq_rhs;_} ->
              auto_project_expr s_var enum eq_rhs)
           state_defs)
      states state_enums

  and auto_project_expr : String.t -> String.t -> Parsedlang.expr -> expr =
    fun s_var enum ->
    function
    | {expr_desc=`ExprAtom atom;expr_loc} ->
      {expr_desc=`ExprWhen
           {when_expr={expr_desc=`ExprAtom atom; expr_loc};
            case=enum;
            cond_expr={expr_desc=`ExprAtom (`AtomIdent s_var);
                       expr_loc}};
       expr_loc}
    | {expr_desc=`ExprApply (f,args);expr_loc} ->
      let f = ident_expr_of_parsed f in
      let args =
        List.map
          (auto_project_expr s_var enum)
          args
      in
      {expr_desc=`ExprApply (f,args);expr_loc}
    | {expr_desc=((`ExprPckUp _|`ExprFby _|`ExprWhen _|`ExprMerge _) as expr_desc);expr_loc} ->
      let expr_desc =
        match expr_desc with
        | (`ExprPckUp {rate_expr;rate_factor} as expr_desc)
        | (`ExprFby (_,{rate_expr;rate_factor}) as expr_desc) ->
           let rate_expr = auto_project_expr s_var enum rate_expr in
           let expr_desc =
             match expr_desc with
             | `ExprPckUp _ -> `ExprPckUp {rate_expr;rate_factor}
             | `ExprFby (cst,_) ->
               let cst = atom_expr_of_parsed cst in
               `ExprFby (cst,{rate_expr;rate_factor})
           in
           expr_desc
        | `ExprWhen {when_expr;case;cond_expr} ->
          let when_expr = auto_project_expr s_var enum when_expr in
          let cond_expr = ident_expr_of_parsed cond_expr in
          `ExprWhen {when_expr;case;cond_expr}
        | `ExprMerge {merge_var;merge_exprs} ->
          let merge_var = ident_expr_of_parsed merge_var in
          let merge_exprs = List.map (fun (case,e) -> (case,auto_project_expr s_var enum e)) merge_exprs in
          `ExprMerge {merge_var;merge_exprs}
      in
      {expr_desc;expr_loc}

  and auto_weak_transitions : String.t -> Parsedlang.state List.t -> String.t StringMap.t -> expr List.t =
      fun s_var states state2enum_map ->
      List.map
        (fun {Parsedlang.state_name;state_weak_trans;state_loc;_} ->
           let variant = StringMap.find state_name state2enum_map in
           let base_transition = {expr_desc=`ExprAtom (`AtomIdent variant);expr_loc=state_loc} in
           List.fold_right
             (fun {Parsedlang.trans_cond;trans_state} expr ->
                let if_expr = auto_project_expr s_var variant trans_cond in
                let destination_variant = StringMap.find trans_state state2enum_map in
                let then_expr = {expr_desc=`ExprAtom (`AtomIdent destination_variant); expr_loc=state_loc} in
                let else_expr = expr in
                {expr_desc=`ExprITE {if_expr;then_expr;else_expr};expr_loc=state_loc})
             state_weak_trans base_transition)
        states

    and auto_strong_transitions : String.t -> Parsedlang.state List.t -> String.t StringMap.t -> expr List.t =
      fun ns_var states state2enum_map ->
      List.map
        (fun {Parsedlang.state_name;state_strong_trans;state_loc;_} ->
           let variant = StringMap.find state_name state2enum_map in
           let base_transition = {expr_desc=`ExprAtom (`AtomIdent variant);expr_loc=state_loc} in
           List.fold_right
             (fun {Parsedlang.trans_cond;trans_state} expr ->
                let if_expr = auto_project_expr ns_var variant trans_cond in
                let destination_variant = StringMap.find trans_state state2enum_map in
                let then_expr = {expr_desc=`ExprAtom (`AtomIdent destination_variant); expr_loc=state_loc} in
                let else_expr = expr in
                {expr_desc=`ExprITE {if_expr;then_expr;else_expr};expr_loc=state_loc})
             state_strong_trans base_transition)
        states

  and auto_add_vars_exprs : node_state -> _ =
    fun node_state auto_loc
      (pns_var,ns_var,s_var) (pns_expr,ns_expr,s_expr)
      output_vars merge_vars_exprs
      local_vars state_exprs
      s_vars s_exprs
      ns_vars ns_exprs
      ->
    let node_state =
      node_state_add_eqs_vars node_state
        [pns_var; ns_var; s_var]
        [pns_expr; ns_expr; s_expr]
        [auto_loc; auto_loc; auto_loc]
    in
    let node_state =
      List.combine output_vars merge_vars_exprs
      |> List.fold_left
        (fun node_state (var,expr) ->
           node_state_add_eq node_state [var] expr auto_loc)
        node_state
    in
    let node_state =
      List.combine local_vars state_exprs
      |> List.fold_left
        (fun node_state (vars,exprs) ->
           node_state_add_eqs_vars node_state
             vars exprs
             (List.map (fun {expr_loc;_} -> expr_loc) exprs)
        )
        node_state
    in
    let node_state =
      node_state_add_eqs_vars node_state
        s_vars s_exprs
        (List.map (fun {expr_loc;_} -> expr_loc) s_exprs)
    in
    let node_state =
      node_state_add_eqs_vars node_state
        ns_vars ns_exprs
        (List.map (fun {expr_loc;_} -> expr_loc) ns_exprs)
    in
    node_state

  and node_state_add_eq node_state eq_lhs eq_rhs eq_loc =
    let eqs = {eq_lhs;eq_rhs;eq_loc}::node_state.eqs in
    {node_state with eqs}

  and node_state_add_eqs_vars : node_state -> String.t List.t -> expr List.t -> Location.t List.t -> node_state =
    fun node_state vars exprs locs ->
    let new_locals =
      List.map
        (fun vdecl_name ->
           {
             vdecl_name;
             vdecl_ty_decl = Types.DeclAny;
             vdecl_ck_decl = Commonclocks.DeclAny;
             vdecl_dl = None;
           })
        vars
    in
    let env =
      List.fold_left
        (fun env v ->
           Env.String.add v.vdecl_name (NLoc v) env)
        node_state.env new_locals
    in
    let locals = List.rev_append new_locals node_state.locals in
    let new_eqs =
      List.combine (List.combine vars exprs) locs
      |> List.map
        (fun ((var,eq_rhs),eq_loc) ->
        {eq_lhs=[var];eq_rhs;eq_loc})
    in
    let eqs = List.append new_eqs node_state.eqs in
    {node_state with eqs; locals; env}

  and auto_state_enum_decl : node_state -> Parsedlang.state list -> node_state * String.t * String.t List.t =
    fun node_state states ->
    let node_state,(enum_name,enum_prefix) =
      safe_state_enum node_state states
    in
    let enums =
      List.map
        (fun state -> enum_prefix ^ state.Parsedlang.state_name)
        states
    in
    node_state,enum_name,enums

  and safe_state_enum node_state states =
    let enum_num = node_state.state.next_enum in
    let enum_name = Format.sprintf "state%i" enum_num in
    let enum_prefix = Format.sprintf "State%i" enum_num in
    let state = incr_next_enum node_state.state in
    let node_state = {node_state with state} in
    let is_safe =
      Env.String.curr node_state.env
      |> Env.String.Map.for_all
        (fun k _ ->
           not (string_sub k enum_name || string_sub k enum_prefix))
    in
    if is_safe then
      node_state,(enum_name,enum_prefix)
    else
      safe_state_enum node_state states

  and decl_enum : node_state -> String.t -> String.t List.t -> Location.t -> node_state
    =
    fun node_state enum_name variants decl_loc ->
    let variants = List.to_seq variants |> StringSet.of_seq in
    let enum_desc = (enum_name, variants) in
    let decl_desc = EnumDecl  enum_desc in
    let decl = {decl_desc;decl_loc} in
    let added_decls = decl::node_state.added_decls in
    let env = Env.String.add enum_name (Enum enum_desc) node_state.env in
    let env =
      StringSet.fold
        (fun variant env ->
           Env.String.add variant (EnumVariant enum_desc) env)
        variants env
    in
    {node_state with added_decls; env}

  and safe_auto_prefixes node_state states =
    let auto_num = node_state.state.next_auto in
    let auto_prefix = Format.sprintf "auto%i" auto_num in
    let state_prefixes =
      List.map
        (fun {Parsedlang.state_name;_} ->
           Format.sprintf "auto%i%s" auto_num state_name)
        states
    in
    let state = incr_next_auto node_state.state in
    let node_state = {node_state with state} in
    let is_safe =
      Env.String.curr node_state.env
      |> Env.String.Map.for_all
        (fun k _ ->
           not (string_sub k auto_prefix))
    in
    if is_safe then
      node_state,auto_prefix,state_prefixes
    else
      safe_auto_prefixes node_state states

  and incr_next_enum state =
    {state with next_enum = state.next_enum+1}

  and incr_next_auto state =
    {state with next_auto = state.next_auto+1}

  and init_state =
    {
      next_enum = 0;
      next_auto = 0;
    }
end

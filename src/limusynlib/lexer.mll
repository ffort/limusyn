{
open Tokens
open Common


let incr_line = MenhirLib.LexerUtil.newline

let keywords =
  [
      "int", TINT;
      "char", TCHAR;
      "float", TFLOAT;
      "on", ON;
      "var", VAR;
      "end", END;
      "node", NODE;
      "returns", RETURNS;
      "let", LET;
      "tel", TEL;
      "due", DUE;
      "fby", FBY;
      "when", WHEN;
      "merge", MERGE;
      "automaton", AUTOMATON;
      "then", THEN;
      "unless", UNLESS;
      "until", UNTIL;
      "rate", RATE;
  ]
  |> List.to_seq
  |> StringMap.of_seq

let try_keyword s =
  match StringMap.find_opt s keywords with
  | Some k -> k
  | None -> IDENT s


let ops =
  [
    "*^", PCKUP;
    ":", COL;
    ",", COMMA;
    ";", SCOL;
    "=", EQ;
    "|", PIPE;
    "->", ARROW;
  ]
  |> List.to_seq
  |> StringMap.of_seq

  let try_op s =
    match StringMap.find_opt s ops with
    | Some op -> op
    | None -> failwith (Format.asprintf "unknown operator ``%s''" s)

}

let newline = ('\010' | '\013' | "\013\010")
let blank = [' ' '\009' '\012']
let ident = ['_' 'A'-'Z' 'a'-'z']['_' 'A'-'Z' 'a'-'z' '0'-'9']*
let dec = ['0'-'9']+
let fp = ['0'-'9'] '.' ['0'-'9']*
let op = ['*' '^' ':' ',' ';' '=' '|' '-' '>']+

rule token = parse
| "--" [^ '\n']* ['\n']
  { incr_line lexbuf;
    token lexbuf }
| "--" [^ '\n']* eof
  { token lexbuf }
| newline
  { incr_line lexbuf;
    token lexbuf }
| blank +
  {token lexbuf}
| "(" { LPAR }
| ")" { RPAR }
| "[" { LBRACK }
| "]" { RBRACK }
| ident as s { try_keyword s }
| dec as s { INT (int_of_string s) }
| op as s { try_op s }
| eof { EOF }
| _ as c { failwith (Format.asprintf "Unknown token: ``%c''" c) }

module LexerUtil = MenhirLib.LexerUtil

module E = MenhirLib.ErrorReports
module I = Parser.MenhirInterpreter

let parse =
  let succeed prog =
    prog
  in
  let fail lexbuf _checkpoint =
    failwith
      (Format.asprintf "Parse error at %s"
         (LexerUtil.range
            (Lexing.lexeme_start_p lexbuf,
            (Lexing.lexeme_end_p lexbuf))))
  in
  let loop lexbuf result =
    let supplier = I.lexer_lexbuf_to_supplier Lexer.token lexbuf in
    I.loop_handle succeed (fail lexbuf) supplier result
  in
  fun file ->
  let lexbuf =
    LexerUtil.init
      file
      (Lexing.from_channel (open_in file))
  in
  loop lexbuf (Parser.Incremental.prog lexbuf.lex_curr_p)

open Common

type prog = top_decl list

and top_decl =
  {
    top_decl_desc: top_decl_desc;
    top_decl_loc: Location.t;
  }

and top_decl_desc =
  | Node of node_desc
  | EnumDecl of enum_desc

and node_desc =
  {
    node_name: String.t;
    node_inputs: vdecl list;
    node_outputs: vdecl list;
    node_locals: vdecl list;
    node_defs: def list;
  }

and enum_desc = String.t * StringSet.t

and def =
  {
    def_desc: def_desc;
    def_loc: Location.t;
  }

and def_desc =
  | Eq of eq_desc
  | Automaton of auto_desc

and eq_desc = {eq_lhs: String.t list; eq_rhs: expr}

and auto_desc = {states: state list}

and 'desc expr_base =
  {
    expr_desc: 'desc;
    expr_loc: Location.t
  }

and expr = expr_desc expr_base

and atom_expr = atom_expr_desc expr_base

and ident_expr = ident_expr_desc expr_base

and state =
  {
    state_name: String.t;
    state_strong_trans: trans list;
    state_defs: eq_desc list;
    state_weak_trans: trans list;
    state_loc: Location.t;
  }

and trans =
  {
    trans_cond: expr;
    trans_state: String.t;
  }

and expr_desc =
  [
    | `ExprAtom of atom
    | `ExprApply of ident_expr * expr list
    | `ExprPckUp of rate_op_desc
    | `ExprFby of atom_expr * rate_op_desc
    | `ExprWhen of when_desc
    | `ExprMerge of merge_desc
  ]

and atom_expr_desc =
  [
    | `ExprAtom of atom
  ]

and ident_expr_desc =
  [
    | `ExprAtom of ident_desc
  ]

and vdecl =
  {
    vdecl_name: String.t;
    vdecl_ty_decl: Types.decl;
    vdecl_ck_decl: Commonclocks.decl;
    vdecl_dl: Int.t Option.t;
  }

and atom =
  [
    | `AtomInt of Int.t
    | `AtomChar of Char.t
    | `AtomFloat of Float.t
    | `AtomIdent of String.t
  ]

and ident_desc =
  [
    | `AtomIdent of String.t
  ]

and rate_op_desc = {rate_expr: expr; rate_factor: Int.t}

and when_desc = {when_expr: expr; case: String.t; cond_expr: ident_expr;}

and merge_desc = {merge_var: ident_expr; merge_exprs: (String.t*expr) list}

let init_prog =
  [
    {
      top_decl_desc=EnumDecl ("bool",StringSet.of_list ["false"; "true"]);
      top_decl_loc=Location.dummy
    }
  ]

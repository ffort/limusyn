%{
  open Parsedlang

  let mk_node_decl node_name node_inputs node_outputs node_locals node_defs top_decl_loc =
    {
      top_decl_desc =
        Node
          {
            node_name;
            node_inputs;
            node_outputs;
            node_locals;
            node_defs;
          };
      top_decl_loc;
    }

  let mk_def def_desc def_loc = { def_desc; def_loc }

  let mk_eq eq_lhs eq_rhs = {eq_lhs; eq_rhs}

  let mk_expr: 'a -> _ -> 'a expr_base =
        fun expr_desc expr_loc -> {expr_desc; expr_loc}

  let mk_state state_name state_strong_trans state_defs state_weak_trans state_loc =
    {state_name; state_strong_trans; state_defs; state_weak_trans; state_loc}
%}

%start prog
%type <top_decl list> prog

%%

let prog := decls=list(top_decl); EOF; { init_prog @ decls}

let top_decl :=
  | NODE; id=IDENT;
    LPAR; inputs=node_vdecls; RPAR;
    RETURNS; LPAR; outputs=node_vdecls; RPAR;
     ~=locals;
    LET; ~=defs; TEL;
    {mk_node_decl id inputs outputs locals defs $loc}

let node_vdecls :=
  | ~=flatten(separated_nonempty_list(SCOL, node_vdecl)); <>

let locals :=
  | locs=option(preceded(VAR, local_vdecls)); {Option.value locs ~default:[]}

let local_vdecls :=
  | ~=flatten(nonempty_list(terminated(node_vdecl, SCOL))); <>

let defs := ~=nonempty_list(def); <>

let eqs := ~=nonempty_list(eq); <>

let def :=
  | ~=eq; {mk_def (Eq eq) $loc}
  | ~=automaton; {mk_def (Automaton automaton) $loc}

let eq :=
  | ids=ident_list; EQ; e=expr; SCOL; {mk_eq ids e}

let automaton :=
  | AUTOMATON; states=nonempty_list(auto_state); END; {{states}}

let auto_state :=
  | PIPE; name=IDENT; ARROW;
    strong_trans=list(strong_trans);
    ~=eqs;
    weak_trans=list(weak_trans);
    {mk_state name strong_trans eqs weak_trans $loc}

let strong_trans :=
  UNLESS; trans_cond=expr; THEN; trans_state=IDENT; SCOL; {{trans_cond;trans_state}}

let weak_trans :=
  UNTIL; trans_cond=expr; THEN; trans_state=IDENT; SCOL; {{trans_cond;trans_state}}

let expr := ~=l2_expr; <>

let l2_expr :=
  | rate_expr=l2_expr; PCKUP; rate_factor=INT;
    {mk_expr (`ExprPckUp {rate_expr;rate_factor}) $loc}
  | when_expr=l2_expr; WHEN; case=IDENT; LPAR; cond_expr=ident_expr; RPAR;
    {mk_expr (`ExprWhen {when_expr;case;cond_expr}) $loc}
  | ~=l1_expr; <>

let l1_expr :=
/* fby, concat, tail */
  | ~=atom_expr; FBY; rate_expr=l1_expr;
    {mk_expr (`ExprFby (atom_expr,{rate_expr;rate_factor=1})) $loc}
  | ~=l0_expr; <>

let l0_expr :=
  | ~=atom_expr; {(atom_expr :> Parsedlang.expr)}
  | f=ident_expr; LPAR; args=separated_nonempty_list(COMMA, expr); RPAR;
    {mk_expr (`ExprApply (f,args)) $loc}
  | MERGE; LPAR; merge_var=ident_expr; COMMA; merge_exprs=separated_nonempty_list(COMMA, merge_fragment); RPAR;
    {mk_expr (`ExprMerge {merge_var;merge_exprs}) $loc}
  | LPAR; ~=expr; RPAR; <>

let atom :=
  | a=INT; {`AtomInt a}
  | a=IDENT; {`AtomIdent a}
  | a=CHAR; {`AtomChar a}

let ident_expr :=
  | a=IDENT; {mk_expr (`ExprAtom (`AtomIdent a)) $loc}

let atom_expr :=
  | ~=atom; {mk_expr (`ExprAtom atom) $loc}

let merge_fragment :=
  | ~=IDENT; ARROW; ~=expr; <>

let node_vdecl :=
  | ids=ident_list; spec=opt_vdecl_spec;
    {
      let vdecl_ty_decl,vdecl_ck_decl,vdecl_dl = spec in
      List.map
        (fun v ->
          {vdecl_name=v;
           vdecl_ty_decl;
           vdecl_ck_decl;
           vdecl_dl})
        ids
    }

let opt_vdecl_spec :=
  | x=option(preceded(COL, vdecl_spec));
    {Option.value x ~default:(Types.DeclAny,Commonclocks.DeclAny,None)}

let vdecl_spec :=
  | ty=typ; ckdl=opt_ckdl; {let ck,dl = ckdl in ty,ck,dl}
  | ckdl=obl_ckdl; {let ck,dl = ckdl in Types.DeclAny,ck,dl}

let opt_ckdl :=
  | ~=ck; dl=opt_dl; {ck,dl}
  | dl=opt_dl; {Commonclocks.DeclAny,dl}

let obl_ckdl :=
  | ~=ck; dl=opt_dl; {ck,dl}
  | dl=obl_dl; {Commonclocks.DeclAny,dl}

let opt_dl :=
  | ~=dl; {Some dl}
  | {None}

let obl_dl :=
  | ~=dl; {Some dl}

let dl := DUE; i=INT; {i}

let typ :=
  | TINT; {Types.Decl Types.Int}
  | TCHAR; {Types.Decl Types.Char}
  | id=IDENT; {Types.Decl (Types.Usertype id)}
  | TFLOAT; {Types.Decl Types.Float}
  | t=typ; LBRACK; i=INT; RBRACK; {Types.Decl (Types.Array (t,i))}

let ck :=
  | RATE; ~=pck; {Commonclocks.Decl (Pck pck)}
  | decl=ck; ON; decl_case=IDENT; decl_cond=IDENT; {Commonclocks.Decl (On {decl;decl_case;decl_cond})}

let pck := LPAR; period=INT; COMMA; offset=INT; RPAR; {{Commonclocks.period;offset}}

let ident_list == ~=separated_nonempty_list(COMMA,IDENT); <>

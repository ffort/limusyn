let list =
  let rec aux mid_fmt end_fmt pp ff l =
    match l with
    | [] ->
      Format.fprintf ff end_fmt
    | x::xs ->
      Format.fprintf ff mid_fmt;
      Format.fprintf ff "%a" pp x;
      aux mid_fmt end_fmt pp ff xs
  in
  fun beg_fmt mid_fmt end_fmt pp ff l ->
  match l with
  | [] -> ()
  | [x] ->
    Format.fprintf ff beg_fmt;
    Format.fprintf ff "%a" pp x;
    Format.fprintf ff end_fmt
  | x::xs ->
    Format.fprintf ff beg_fmt;
    Format.fprintf ff "%a" pp x;
    aux mid_fmt end_fmt pp ff xs

let seq =
  let rec aux mid_fmt end_fmt pp ff (n: _ Seq.node) =
    match n with
    | Nil -> Format.fprintf ff end_fmt
    | Cons (x,s) ->
      Format.fprintf ff mid_fmt;
      Format.fprintf ff "%a" pp x;
      aux mid_fmt end_fmt pp ff (s ())
  in
  fun beg_fmt mid_fmt end_fmt pp ff (s: _ Seq.t) ->
  match s () with
  | Nil -> ()
  | Cons (x,s) ->
    Format.fprintf ff beg_fmt;
    Format.fprintf ff "%a" pp x;
    aux mid_fmt end_fmt pp ff (s ())

let int_map =
  fun beg_fmt mid_fmt end_fmt pp ff (m: _ Common.IntMap.t) ->
  seq beg_fmt mid_fmt end_fmt pp ff (Common.IntMap.to_seq m)

let option =
  fun pp_some none_fmt ff o ->
  match o with
  | Some x -> Format.fprintf ff "%a" pp_some x
  | None -> Format.fprintf ff none_fmt

let post =
  fun fmt pp ff x ->
  Format.fprintf ff fmt pp x

type debug =
  | DebugSClocked
  | DebugRClocked
  | DebugTyped
  | DebugZ3

let print_debug = ref []

let add_debug d =
  print_debug := d::(!print_debug)

let vdebug: debug -> ('a, Format.formatter, unit) format -> 'a =
  fun d x ->
  if List.mem d !print_debug then
    Format.eprintf x
  else
    Format.ifprintf Format.err_formatter x

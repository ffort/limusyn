open Common
open Commonclocks

type 'desc ck_expr_base =
  {
    ck_desc: 'desc;
    ck_id: Int.t;
    ck_scoped: Bool.t;
  }

and ck_e = ck_e_desc ck_expr_base

and ck_r = ck_r_desc ck_expr_base

and ck_c = ck_c_desc ck_expr_base

and ck_b = ck_b_desc ck_expr_base

and ck_v = ck_v_desc ck_expr_base

and ck_uni_expr = ck_uni_desc ck_expr_base
and ck_var_expr = ck_var_desc ck_expr_base
and ck_fn = ck_fn_desc ck_expr_base
and ck_out = ck_out_desc ck_expr_base

and ck_e_desc =
  [
    | `CkArrow of fun_desc
    | `CkTuple of  ck_r list
    | `CkRefine of ck_ref
  ]

and ck_r_desc =
  [
    | `CkRefine of ck_ref
  ]

and ck_c_desc =
  [
    | `CkVar
    | `CkUnivar
    | `Pck
    | `On of on_desc
    | `Zeta of ck_zeta
  ]

and ck_b_desc =
  [
    | `CkVar
    | `CkUnivar
    | `Pck
    | `On of on_desc
  ]

and ck_v_desc =
  [
    | `CkVar
    | `CkUnivar
  ]

and ck_uni_desc = [ `CkUnivar ]
and ck_var_desc = [ `CkVar ]
and ck_fn_desc = [ `CkArrow of fun_desc ]
and ck_out_desc = [ `CkRefine of ck_ref | `CkTuple of ck_r List.t ]

(* and ck_on = {ck_on: ty_expr; cond: ident; cons: ident; view: ty_expr} *)

and ref_fragments =
  | FragTru
  | FragPEV of arg_var
  | FragPEC of int
  | FragOEV of arg_var
  | FragOEC of int
  | FragPMd of int
  | FragPDv of int

and _ refinement =
  | RefCst: int -> int refinement
  | RefPer: arg_var -> int refinement
  | RefOff: arg_var -> int refinement
  | RefBin: (int -> int -> int) refinement * int refinement * int refinement ->
      int refinement
  | RefMul: (int -> int -> int) refinement
  | RefDiv: (int -> int -> int) refinement
  | RefAdd: (int -> int -> int) refinement
  | RefSub: (int -> int -> int) refinement
  | RefPEq: (int -> bool) refinement
  | RefOEq: (int -> bool) refinement
  | RefPDv: (int -> bool) refinement
  | RefOGe: (int -> bool) refinement
  | RefRel: (int -> bool) refinement * int refinement -> bool refinement
  | RefCon: bool refinement * bool refinement -> bool refinement
  | RefPck: int refinement * int refinement -> bool refinement
  | RefTru: bool refinement

and arg_var =
  | AVVar of String.t
  | AVSelf
  | AVZeta

and fun_desc = (string * ck_r) list * ck_out

and on_desc =
  {
    ck_on: ck_b;
    case: String.t;
    cond: String.t;
    view: ck_r;
  }

and ck_ref = ck_c * bool refinement

and ck_zeta = ck_v * bool refinement

type fck_e = fck_e_desc ck_expr_base

and fck_r = fck_r_desc ck_expr_base

and fck_c = fck_c_desc ck_expr_base

and fck_b = fck_b_desc ck_expr_base

and fck_e_desc =
  [
    | `CkArrow of fck_fun_desc
    | `CkTuple of fck_r list
    | `CkRefine of fck_ref
  ]

and fck_r_desc =
  [
    | `CkRefine of fck_ref
  ]

and fck_c_desc =
  [
    | `CkVar
    | `CkUnivar
    | `Pck
    | `Zeta of fck_b
  ]

and fck_b_desc =
  [
    | `CkVar
    | `CkUnivar
    | `Pck
  ]

and fck_fun_desc = (String.t * fck_r) list * fck_e

and fck_ref = fck_c * ref_fragments list

type ack_e = ack_e_desc ck_expr_base

and ack_r = ack_r_desc ck_expr_base

and ack_c = ack_c_desc ck_expr_base

and ack_b = ack_b_desc ck_expr_base

and ack_e_desc =
  [
    | `CkArrow of ack_fun_desc
    | `CkTuple of ack_r list
    | `CkRefine of ack_ref
  ]

and ack_r_desc =
  [
    | `CkRefine of ack_ref
  ]

and ack_c_desc =
  [
    | `CkVar
    | `CkUnivar
    | `Pck
    | `Zeta of ack_b
  ]

and ack_b_desc =
  [
    | `CkVar
    | `CkUnivar
    | `Pck
  ]

and ack_fun_desc = (String.t * ack_r) list * ack_e

and ack_ref = ack_c * bool refinement list

type ck = ck_desc ck_expr_base

and ck_desc = [ ck_e_desc | ck_r_desc | ck_c_desc | ck_b_desc ]

type state =
  {
    next_ck_id: Int.t;
  }

module PP =
struct
  let rec decl ff d =
    match d with
    | DeclAny -> Format.fprintf ff "_"
    | Decl Pck {period;offset} -> Format.fprintf ff "(%i,%i)" period offset
    | Decl On {decl=d';decl_case;decl_cond} ->
      Format.fprintf ff "@[<hov 2>%a@ on@ %s:%s@]"
        decl d' decl_case decl_cond

  let ck_v : Format.formatter -> ck_v -> unit =
    fun ff {ck_desc;ck_scoped;ck_id} ->
    let underscore = if ck_desc = `CkVar then "_" else "" in
    let apostrophe = if ck_scoped then "\"" else "'" in
    Format.fprintf ff "%s%sck%i"
      apostrophe underscore ck_id

  let arg_var ff (av: arg_var) =
    match av with
    | AVSelf -> Format.fprintf ff "self"
    | AVVar av -> Format.pp_print_string ff av
    | AVZeta -> Format.fprintf ff "zetax"

  let rec refinement: type a. Format.formatter -> a refinement -> unit =
    fun ff ->
    function
    | RefCst i -> Format.fprintf ff "%i" i
    | RefPer av ->
      Format.fprintf ff "@[<hov 2>p(@,%a)@]" arg_var av
    | RefOff av ->
      Format.fprintf ff "@[<hov 2>o(@,%a)@]" arg_var av
    | RefBin(o,l,r) ->
      Format.fprintf ff "@[<hov 2>(@,%a@ %a@ %a@,)@]"
        refinement l refinement o refinement r
    | RefRel(r,x) ->
      Format.fprintf ff "@[<hov 2>%a@ %a@]"
        refinement r refinement x
    | RefAdd -> Format.fprintf ff "+"
    | RefSub -> Format.fprintf ff "-"
    | RefMul -> Format.fprintf ff "*"
    | RefDiv -> Format.fprintf ff "/"
    | RefPEq -> Format.fprintf ff "p ="
    | RefOEq -> Format.fprintf ff "o ="
    | RefPDv -> Format.fprintf ff "p div"
    | RefOGe -> Format.fprintf ff "o >="
    | RefCon(r1,r2) -> Format.fprintf ff "@[<hov 2>%a@ &&@ %a@]" refinement r1 refinement r2
    | RefPck(p,o) ->
      Format.fprintf ff "@[<hov 2>p =@ %a@ &&@ o =@ %a@]"
        refinement p refinement o
    | RefTru -> Format.fprintf ff "true"

  let rec ck_b : Format.formatter -> ck_b -> unit =
    fun ff ->
    function
    | {ck_desc=`Pck;_} -> Format.fprintf ff "pck"
    | ({ck_desc=`CkUnivar | `CkVar;_} as ck) ->
      ck_v ff ck
    | {ck_desc=`On {ck_on;case;cond;view};_} ->
      Format.fprintf ff "@[<hv 2>%a@ on@ %s(@,%s,@ %a)@]"
        ck_b ck_on
        case cond
        ck_r view

  and ck_c ff (c: ck_c) =
    match c with
    | ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as c) -> ck_b ff c
    | ({ck_desc=`Zeta (c',r);_}) ->
      Format.fprintf ff "z(%a, \\x.%a)"
        ck_v c'
        refinement r

  and ck_r ff (c: ck_r) =
    match c.ck_desc with
    | `CkRefine (ck_ref,r) ->
      Format.fprintf ff "@[{@ %a@ |@ %a@ }@]"
        ck_c ck_ref
        refinement r

  let rec ck_fn ff (c: ck_fn) =
    match c.ck_desc with
    | `CkArrow (ck_args,ck_out) ->
      Format.fprintf ff "@[%a@ ->@ %a@]"
        (Print.list "" "@ ->@ " ""
           (fun ff (id,t) -> Format.fprintf ff "%s:%a" id ck_r t))
        ck_args ck (ck_out :> ck)

  and ck_e ff (c: ck_e) =
    match c with
    | ({ck_desc=`CkArrow _;_} as c) ->
      ck_fn ff c
    | ({ck_desc=`CkRefine _;_} as c) ->
      ck_r ff c
    | {ck_desc=`CkTuple ckl;_} -> Print.list "@[(" "@ *@ " ")@]" ck_r ff ckl


  and ck ff (c: ck) =
    match c with
    | ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as c) -> ck_b ff c
    | ({ck_desc=`Zeta _;_} as c) -> ck_c ff c
    | ({ck_desc=`CkRefine _;_} as c) ->
      ck_r ff c
    | ({ck_desc=`CkTuple _;_} as c) -> ck_e ff c
    | ({ck_desc=`CkArrow _;_} as c) ->
      ck_fn ff c

end

let new_ck_id {next_ck_id} =
  next_ck_id,{next_ck_id=next_ck_id+1}

let new_ck : state -> [> ] -> bool -> state * [> ] ck_expr_base =
  fun state ck_desc ck_scoped ->
  let ck_id,state = new_ck_id state in
  state,
  {
    ck_desc;
    ck_scoped;
    ck_id;
  }


module Predef =
struct
  let pck,init_state =
    let state = {next_ck_id=0} in
    let state,pck = new_ck state `Pck false in
    pck,state
end


(* let fragments_of_decl decl = *)
(*   match decl with *)
(*   | DeclAny -> [FragTru] *)
(*   | Decl Pck {period;offset} -> *)
(*     [FragPEC period;FragOEC offset] *)
(*   | Decl On _ -> failwith "" *)

module Make =
struct
  type made_vars =
    {
      mck_v: ck_v IntMap.t;
      mck_b: ck_b IntMap.t;
      mck_c: ck_c IntMap.t;
      mck_r: ck_r IntMap.t;
      mck_e: ck_e IntMap.t;
    }

  let no_made_vars =
    {
      mck_v = IntMap.empty;
      mck_b = IntMap.empty;
      mck_c = IntMap.empty;
      mck_r = IntMap.empty;
      mck_e = IntMap.empty;
    }

  type make_state = state * made_vars

  let init_make_state state = (state,no_made_vars)

  let rec pck_of_decl =
    function
    | Commonclocks.DeclAny -> None
    | Decl Pck pck -> Some pck
    | Decl On {decl;_} -> pck_of_decl decl

  let of_struct_ck_v : make_state -> Structclocks.ck_v -> make_state * ck_v =
    fun ((state,made_vars) as make_state) ck_v ->
    let result = IntMap.find_opt ck_v.ck_id made_vars.mck_v in
    match result,ck_v with
    | Some ck_v,_ -> make_state,ck_v
    | None,{ck_desc;ck_scoped;ck_id} ->
      let state,ck_v = new_ck state ck_desc ck_scoped in
      let made_vars = {made_vars with mck_v=IntMap.add ck_id ck_v made_vars.mck_v} in
      (state,made_vars),ck_v

  let of_struct_ck_b : make_state -> Structclocks.ck_b -> make_state * ck_b =
    fun ((state,made_vars) as make_state) ck_b ->
    let result = IntMap.find_opt ck_b.ck_id made_vars.mck_b in
    match result,ck_b with
    | Some ck_b,_ -> make_state,ck_b
    | None,{ck_desc=`Pck;ck_id;_} ->
      let made_vars = {made_vars with mck_b=IntMap.add ck_id Predef.pck made_vars.mck_b} in
      (state,made_vars),Predef.pck
    | None,({ck_desc=`CkVar|`CkUnivar;ck_id;_} as ck_v) ->
      let (state,made_vars),ck_v = of_struct_ck_v make_state ck_v in
      let ck_b = (ck_v :> ck_b) in
      let made_vars = {made_vars with mck_b=IntMap.add ck_id ck_b made_vars.mck_b} in
      (state,made_vars),ck_b
    | None,({ck_desc=`On _;_}) -> failwith ""


  let of_struct_ck_c : make_state -> Structclocks.ck_c -> make_state * ck_c =
    fun ((_,made_vars) as make_state) ck_c ->
    let result = IntMap.find_opt ck_c.ck_id made_vars.mck_c in
    match result,ck_c with
    | Some ck_c, _ -> make_state,ck_c
    | None,({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;ck_id;_} as ck_c) ->
      let (state,made_vars), ck_b = of_struct_ck_b make_state ck_c in
      let ck_c = (ck_b :> ck_c) in
      let made_vars = {made_vars with mck_c=IntMap.add ck_id ck_c made_vars.mck_c} in
      (state,made_vars),ck_c
    | None,({ck_desc=`Zeta ck_zvar;ck_id;ck_scoped}) ->
      let (state,made_vars), ck_zvar = of_struct_ck_v make_state ck_zvar in
      let state,ck_c = new_ck state (`Zeta (ck_zvar,RefTru)) ck_scoped in
      let made_vars = {made_vars with mck_c=IntMap.add ck_id ck_c made_vars.mck_c} in
      (state,made_vars),ck_c


  let of_struct_ck_r : make_state -> Structclocks.ck_r -> Commonclocks.decl -> make_state * ck_r =
    fun ((_,made_vars) as make_state) ck ck_decl ->
    let {ck_desc=`CkRefine (ck_ref,Structclocks.RefHole);ck_scoped;ck_id} = ck in
    let result = IntMap.find_opt ck_id made_vars.mck_r in
    match result,pck_of_decl ck_decl with
    | Some ck_r,_ -> make_state,ck_r
    | None, None ->
      let (state,made_vars),ck_ref = of_struct_ck_c make_state ck_ref in
      let state,ck_r = new_ck state (`CkRefine (ck_ref,RefTru)) ck_scoped in
      let made_vars = {made_vars with mck_r=IntMap.add ck_id ck_r made_vars.mck_r} in
      (state,made_vars),ck_r
    | None, Some {Commonclocks.period;offset} ->
      let (state,made_vars),ck_ref = of_struct_ck_c make_state ck_ref in
      let state,ck_r =
        new_ck state (`CkRefine (ck_ref,
                                 RefCon(
                                   RefRel(RefPEq,RefCst period),
                                   RefRel(RefOEq,RefCst offset))))
          ck_scoped
      in
      let made_vars = {made_vars with mck_r=IntMap.add ck_id ck_r made_vars.mck_r} in
      (state,made_vars),ck_r
end


module SMT =
struct
  type made_exprs =
    {
      mck_v: Smtlib.expr IntMap.t;
      mck_b: Smtlib.expr IntMap.t;
      mck_c: Smtlib.expr IntMap.t;
      mck_r: Smtlib.expr IntMap.t;
      mck_e: Smtlib.expr IntMap.t;
    }

  type vars_of_cks = (String.t * String.t) IntMap.t

  type vars_of_smt_vars = (String.t * String.t) StringMap.t

  type smt_state =
    {
      next_var_id: Int.t;
      vars_of_smt_vars: vars_of_smt_vars;
      vars_of_cks: vars_of_cks;
      made_exprs: made_exprs;
    }

  let init_smt_state =
    {
      next_var_id = 0;
      vars_of_cks = IntMap.empty;
      vars_of_smt_vars = StringMap.empty;
      made_exprs =
        {
          mck_v = IntMap.empty;
          mck_b = IntMap.empty;
          mck_c = IntMap.empty;
          mck_r = IntMap.empty;
          mck_e = IntMap.empty;
        }
    }

  let smt_vars =
    fun smt_state ->
    let id = smt_state.next_var_id in
    let period = Format.sprintf "period%i" id in
    let offset = Format.sprintf "offset%i" id in
    let smt_state = {smt_state with next_var_id=id+1} in
    smt_state,(period,offset)

  let rec state_with_ck_b =
    fun smt_state get_env ->
    function
    | {ck_desc=`Pck|`CkVar|`CkUnivar;_} -> smt_state
    | {ck_desc=`On {ck_on;view;_};_} ->
      let smt_state = state_with_ck_r smt_state get_env view in
      state_with_ck_b smt_state get_env ck_on

  and state_with_ck_c: smt_state -> _ -> ck_c -> smt_state =
    fun smt_state get_env ->
    function
    | ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as ck_c) ->
      state_with_ck_b smt_state get_env ck_c
    | {ck_desc=`Zeta _;_} -> failwith ""

  and state_with_ck_r: smt_state -> _ -> ck_r -> smt_state =
    fun smt_state get_env ck_r ->
    let {ck_desc=`CkRefine (ck_ref,refinement);ck_id;_} = ck_r in
    if IntMap.mem ck_id smt_state.vars_of_cks then
      smt_state
    else
      let smt_state,vars = smt_vars smt_state in
      let vars_of_cks = IntMap.add ck_id vars smt_state.vars_of_cks in
      let smt_state = {smt_state with vars_of_cks} in
      let smt_state = state_with_refinement smt_state get_env refinement in
      state_with_ck_c smt_state get_env ck_ref

  and state_with_refinement: type a. smt_state -> (String.t -> ck_r) -> a refinement -> smt_state =
    fun smt_state get_env ->
    function
    | RefPer AVVar v | RefOff AVVar v ->
      let ck = get_env v in
      begin
        match StringMap.find_opt v smt_state.vars_of_smt_vars,
              IntMap.find_opt ck.ck_id smt_state.vars_of_cks
        with
        | None,Some peroff ->
          let vars_of_smt_vars = StringMap.add v peroff smt_state.vars_of_smt_vars in
          {smt_state with vars_of_smt_vars}
        | None,None ->
          let smt_state = state_with_ck_r smt_state get_env ck in
          let peroff = IntMap.find ck.ck_id smt_state.vars_of_cks in
          let vars_of_smt_vars = StringMap.add v peroff smt_state.vars_of_smt_vars in
          {smt_state with vars_of_smt_vars}
        | Some _,_ -> smt_state
      end
    | RefCon (l,r) ->
      let smt_state = state_with_refinement smt_state get_env l in
      state_with_refinement smt_state get_env r
    | RefBin (op,l,r) ->
      let smt_state = state_with_refinement smt_state get_env op in
      let smt_state = state_with_refinement smt_state get_env l in
      state_with_refinement smt_state get_env r
    | RefRel (rel,e) ->
      let smt_state = state_with_refinement smt_state get_env rel in
      state_with_refinement smt_state get_env e
    | RefPck (p,o) ->
      let smt_state = state_with_refinement smt_state get_env p in
      state_with_refinement smt_state get_env o
    | RefCst _
    | RefPer (AVSelf|AVZeta)
    | RefOff (AVSelf|AVZeta)
    | RefAdd | RefSub | RefMul | RefDiv
    | RefPEq | RefOEq | RefPDv | RefOGe
    | RefTru
      -> smt_state

  let state_add_expr_v: _ -> ck_v -> _ -> _ =
    fun smt_state {ck_id;_} expr ->
    let mck_v = IntMap.add ck_id expr smt_state.made_exprs.mck_v in
    let made_exprs = {smt_state.made_exprs with mck_v} in
    {smt_state with made_exprs}

  let state_add_expr_b: _ -> ck_b -> _ -> _  =
    fun smt_state {ck_id;_} expr ->
    let mck_b = IntMap.add ck_id expr smt_state.made_exprs.mck_b in
    let made_exprs = {smt_state.made_exprs with mck_b} in
    {smt_state with made_exprs}

  let state_add_expr_c: _ -> ck_c -> _ -> _  =
    fun smt_state {ck_id;_} expr ->
    let mck_c = IntMap.add ck_id expr smt_state.made_exprs.mck_c in
    let made_exprs = {smt_state.made_exprs with mck_c} in
    {smt_state with made_exprs}

  let state_add_expr_r: _ -> ck_r -> _ -> _  =
    fun smt_state {ck_id;_} expr ->
    let mck_r = IntMap.add ck_id expr smt_state.made_exprs.mck_r in
    let made_exprs = {smt_state.made_exprs with mck_r} in
    {smt_state with made_exprs}

  let bool_true = Smtlib.Bool true

  let of_ref_bin =
    function
    | RefAdd -> "+"
    | RefSub -> "-"
    | RefMul -> "*"
    | RefDiv -> "ckdiv"

  let rec of_int_ref: smt_state -> String.t -> String.t -> int refinement -> Smtlib.expr =
    fun smt_state period offset ->
    function
    | RefCst i -> Smtlib.Int i
    | RefPer AVSelf -> Smtlib.Id period
    | RefPer AVVar var ->
      let var,_ = StringMap.find var smt_state.vars_of_smt_vars in
      Smtlib.Id var
    | RefPer AVZeta -> failwith "Internal error"
    | RefOff AVSelf -> Smtlib.Id offset
    | RefOff AVVar var ->
      let _,var = StringMap.find var smt_state.vars_of_smt_vars in
      Smtlib.Id var
    | RefOff AVZeta -> failwith "Internal error"
    | RefBin (op,l,r) ->
      Smtlib.Appl(of_ref_bin op,
                  [of_int_ref smt_state period offset l;
                   of_int_ref smt_state period offset r;])

  let of_ref_rel =
    fun period offset rel e ->
    match rel with
    | RefPEq -> Smtlib.eq [Id period; e]
    | RefOEq -> Smtlib.eq [Id offset; e]
    | RefPDv -> Smtlib.Appl ("isdiv", [Id period; e])
    | RefOGe -> Smtlib.ge (Id offset) e

  let rec of_refinement: smt_state -> (String.t * String.t) -> bool refinement -> Smtlib.expr =
    fun smt_state ((period,offset) as peroff) ->
    function
    | RefCon (l,r) ->
      let l = of_refinement smt_state peroff l in
      let r = of_refinement smt_state peroff r in
      Smtlib.pand [l;r]
    | RefTru -> bool_true
    | RefRel (rel,e) ->
      of_ref_rel period offset rel (of_int_ref smt_state period offset e)
    | RefPck (p,o) -> of_refinement smt_state peroff
                        (RefCon
                           (RefRel (RefPEq, p),
                            RefRel (RefOEq, o)))

  let of_ck_v: smt_state -> ck_v -> smt_state * Smtlib.expr =
    fun smt_state ck_v ->
    let result = IntMap.find_opt ck_v.ck_id smt_state.made_exprs.mck_v in
    match result,ck_v with
    | Some expr,_ -> smt_state,expr
    | None, _ ->
      let expr = bool_true in
      let smt_state = state_add_expr_v smt_state ck_v expr in
      smt_state,expr

  let rec of_ck_b: smt_state -> ck_b -> smt_state * Smtlib.expr =
    fun smt_state ck_b ->
    let result = IntMap.find_opt ck_b.ck_id smt_state.made_exprs.mck_b in
    match result,ck_b with
    | Some expr,_ -> smt_state,expr
    | None, ({ck_desc=`CkVar|`CkUnivar;_} as ck_c) ->
      let smt_state, expr = of_ck_v smt_state ck_c in
      let smt_state = state_add_expr_b smt_state ck_c expr in
      smt_state,expr
    | None, {ck_desc=`Pck;_} ->
      let expr = bool_true in
      let smt_state = state_add_expr_b smt_state ck_b expr in
      smt_state,expr
    | None, {ck_desc=`On {ck_on;view;_};_} ->
      let smt_state,smt_ck_on = of_ck_b smt_state ck_on in
      let smt_state,smt_view = of_ck_r smt_state view in
      let expr = Smtlib.pand [smt_ck_on;smt_view] in
      let smt_state = state_add_expr_b smt_state ck_b expr in
      smt_state,expr


  and of_ck_c: smt_state -> ck_c -> smt_state * Smtlib.expr =
    fun smt_state ck_c ->
    let result = IntMap.find_opt ck_c.ck_id smt_state.made_exprs.mck_c in
    match result,ck_c with
    | Some expr,_ -> smt_state, expr
    | None, ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as ck_c) ->
      let smt_state,expr = of_ck_b smt_state ck_c in
      let smt_state = state_add_expr_c smt_state ck_c expr in
      smt_state,expr
    | None, {ck_desc=`Zeta _;_} -> failwith "todo"

  and of_ck_r: smt_state -> ck_r -> smt_state * Smtlib.expr =
    fun smt_state ck_r ->
    let result = IntMap.find_opt ck_r.ck_id smt_state.made_exprs.mck_r in
    match result,ck_r with
    | Some expr,_ -> smt_state, expr
    | None, {ck_desc=`CkRefine (ck_ref,refinement);ck_id;_} ->
      let smt_state,ck_ref_smt = of_ck_c smt_state ck_ref in
      let ck_r_smt = of_refinement smt_state (IntMap.find ck_id smt_state.vars_of_cks) refinement in
      let expr = Smtlib.pand [ck_ref_smt; ck_r_smt] in
      let smt_state = state_add_expr_r smt_state ck_r expr in
      smt_state,expr

  let rec link_tys_ck_b: smt_state -> ck_b -> ck_b -> _ =
    fun smt_state ck1 ck2 ->
    match ck1,ck2 with
    | {ck_desc=`Pck|`CkVar|`CkUnivar;_},{ck_desc=`Pck|`CkVar|`CkUnivar;_}
      ->
      bool_true
    | {ck_desc=`On {ck_on=ck_on1;view=view1;_};_},
      {ck_desc=`On {ck_on=ck_on2;view=view2;_};_}
      ->
      let smt_ck_on = link_tys_ck_b smt_state ck_on1 ck_on2 in
      let smt_view = link_tys_ck_r smt_state view1 view2 in
      Smtlib.pand [smt_ck_on;smt_view]
    |  _ -> failwith "Internal error"

  and link_tys_ck_c : smt_state -> ck_c -> ck_c -> _ =
    fun smt_state ck1 ck2 ->
    match ck1,ck2 with
    | ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as ck1),
      ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as ck2)
      ->
      link_tys_ck_b smt_state ck1 ck2
    | {ck_desc=`Zeta _;_},_
    | _,{ck_desc=`Zeta _;_}
      ->
      failwith ""

  and link_tys_ck_r : smt_state -> ck_r -> ck_r -> _ =
    fun smt_state ck1 ck2 ->
    let {ck_desc=`CkRefine (ck_ref1,_);ck_id=id1;_} = ck1 in
    let {ck_desc=`CkRefine (ck_ref2,_);ck_id=id2;_} = ck2 in
    let link_ck_ref = link_tys_ck_c smt_state ck_ref1 ck_ref2 in
    let (p1,o1) = IntMap.find id1 smt_state.vars_of_cks in
    let (p2,o2) = IntMap.find id2 smt_state.vars_of_cks in
    Smtlib.pand
      [
        link_ck_ref;
        Smtlib.eq [Smtlib.Id p1; Smtlib.Id p2];
        Smtlib.eq [Smtlib.Id o1; Smtlib.Id o2];
      ]

end

module RSubstitute =
struct

  let rec of_ck_b: state -> String.t -> String.t -> ck_b -> state * ck_b =
    fun state from_var to_var ->
      function
      | ({ck_desc=`Pck|`CkVar|`CkUnivar;_} as ck_b) ->
        state,ck_b
      | {ck_desc=`On {ck_on;case;cond;view};ck_scoped;_} ->
        let state,ck_on = of_ck_b state from_var to_var ck_on in
        let state,view = of_ck_r state from_var to_var view in
        new_ck state (`On {ck_on;case;cond;view}) ck_scoped

  and of_ck_c: state -> String.t -> String.t -> ck_c -> state * ck_c =
    fun state from_var to_var ->
      function
      | ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as ck_b) ->
        (of_ck_b state from_var to_var ck_b :> state * ck_c)
      | {ck_desc=`Zeta _;_} -> failwith "Unimplemented"

  and of_refinement: type a. String.t -> String.t -> a refinement -> a refinement =
    fun from_var to_var ->
      function
      | RefCst x -> RefCst x
      | RefPer AVVar v when v=from_var ->
        RefPer (AVVar to_var)
      | RefPer v ->
        RefPer v
      | RefOff AVVar v when v=from_var ->
        RefOff (AVVar to_var)
      | RefOff v ->
        RefOff v
      | RefBin (op,l,r) ->
        let op = of_refinement from_var to_var op in
        let l = of_refinement from_var to_var l in
        let r = of_refinement from_var to_var r in
        RefBin (op,l,r)
      | RefRel (rel,e) ->
        let rel = of_refinement from_var to_var rel in
        let e = of_refinement from_var to_var e in
        RefRel (rel,e)
      | RefCon (l,r) ->
        let l = of_refinement from_var to_var l in
        let r = of_refinement from_var to_var r in
        RefCon (l,r)
      | RefPck (p,o) ->
        let p = of_refinement from_var to_var p in
        let o = of_refinement from_var to_var o in
        RefPck (p,o)
      | RefAdd -> RefAdd
      | RefSub -> RefSub
      | RefMul -> RefMul
      | RefDiv -> RefDiv
      | RefPEq -> RefPEq
      | RefOEq -> RefOEq
      | RefPDv -> RefPDv
      | RefOGe -> RefOGe
      | RefTru -> RefTru

  and of_ck_r: state -> String.t -> String.t -> ck_r -> state * ck_r =
    fun state from_var to_var ->
      function
      | {ck_desc=`CkRefine (ck_c,refinement);ck_scoped;_} ->
        let state,ck_c = of_ck_c state from_var to_var ck_c in
        let refinement = of_refinement from_var to_var refinement in
        new_ck state (`CkRefine (ck_c,refinement)) ck_scoped

  and of_ck_out: state -> String.t -> String.t -> ck_out -> state * ck_out =
    fun state from_var to_var ->
      function
      | ({ck_desc=`CkRefine _;_} as ck_r) ->
        (of_ck_r state from_var to_var ck_r :> state * ck_out)
      | {ck_desc=`CkTuple ckl;ck_scoped;_} ->
        let state,ckl =
          List.fold_left_map
            (fun state ck_r ->
               of_ck_r state from_var to_var ck_r)
            state ckl
        in
        new_ck state (`CkTuple ckl) ck_scoped

end

open Common

type prog = decl list

and decl =
  {
    decl_desc: decl_desc;
    decl_loc: Location.t;
  }

and decl_desc =
  | Node of node_desc
  | Enum of String.t * StringSet.t * Types.ty_enum

and node_desc =
  {
    node_name: String.t;
    node_inputs: vdecl list;
    node_outputs: vdecl list;
    node_locals: vdecl list;
    node_eqs: eq list;
    node_ty: Types.ty_expr;
    node_ck: Structclocks.ck_arrow_expr;
  }

and eq =
  {
    eq_lhs: String.t list;
    eq_rhs: expr;
    eq_loc: Location.t;
  }

and ('desc,'ck) expr_base =
  {
    expr_desc: 'desc;
    expr_loc: Location.t;
    expr_ty: Types.ty_expr;
    expr_ck: 'ck;
  }

and expr = (expr_desc,Structclocks.ck_r) expr_base

and atom_expr = (atom_expr_desc,Structclocks.ck_r) expr_base

and ident_expr = (ident_expr_desc,Structclocks.ck_r) expr_base

and const_expr = (const_expr_desc,Structclocks.ck_r) expr_base

and ident_fn_expr = (ident_expr_desc,Structclocks.ck_arrow_expr) expr_base

and expr_desc =
  [
    | `ExprConst of const
    | `ExprApply of ident_fn_expr * ident_expr List.t
    | `ExprIdent of String.t
    | `ExprPckUp of rate_op_desc
    | `ExprFby of const_expr*rate_op_desc
    | `ExprWhen of when_desc
    | `ExprMerge of merge_desc
    | `ExprITE of ite_desc
  ]

and atom_expr_desc =
  [
    | `ExprConst of const
    | `ExprIdent of String.t
  ]


and ident_expr_desc =
  [
    | `ExprIdent of String.t
  ]

and const_expr_desc =
  [
    | `ExprConst of const
  ]

and vdecl =
  {
    vdecl_name: String.t;
    vdecl_ty_decl: Types.decl;
    vdecl_ck_decl: Commonclocks.decl;
    vdecl_dl: Int.t Option.t;
    vdecl_ty: Types.ty_expr;
    vdecl_ck: Structclocks.ck_r;
  }

and rate_op_desc =
  {
    rate_expr: ident_expr;
    rate_factor: Int.t;
    rate_ty: Types.ty_fn;
    rate_ck: Structclocks.ck_arrow_expr;
  }

and when_desc =
  {
    when_expr: ident_expr;
    case: String.t;
    cond_expr: ident_expr;
    when_ty: Types.ty_fn;
    when_ck: Structclocks.ck_arrow_expr;
  }

and merge_desc =
  {
    merge_var: ident_expr;
    merge_exprs: (String.t*ident_expr) list;
    merge_ty: Types.ty_fn;
    merge_ck: Structclocks.ck_arrow_expr
  }

and ite_desc =
  {
    if_expr: ident_expr;
    then_expr: ident_expr;
    else_expr: ident_expr;
    ite_ty: Types.ty_fn;
    ite_ck: Structclocks.ck_arrow_expr;
  }

and const =
  [
    | `ConstInt of Int.t
    | `ConstChar of Char.t
    | `ConstFloat of Float.t
    | `ConstEnum of enum_value
  ]

and enum_value = {variant: String.t; enum: String.t}

type state =
  {
    typer_state: Structclocks.state;
    main_node: String.t;
  }

module PP =
struct

  let rec prog =
    fun ff p ->
      Print.list "@[<v>" "@ @ " "@]" decl ff p

  and decl =
    fun ff (d:decl) ->
      match d.decl_desc with
      | Node ndesc ->
        node ff ndesc
      | Enum (ename,evariants,_) ->
        enum ff ename evariants

  and node =
    fun ff ({node_name;node_inputs;node_outputs;node_locals;node_eqs;node_ty;node_ck}: node_desc) ->
      Format.fprintf ff "@[<v>@[<hov 2>node@ %s@ :@ %a@ rate@ %a"
        node_name Types.PP.ty node_ty Structclocks.PP.ck_arrow node_ck;
      Print.list "@ @[<hov 2>(" ";@ " ")@]@]" vdecl ff node_inputs;
      Format.fprintf ff "@ returns@ ";
      Print.list "@[<hov 2>(" ";@ " ")@]" vdecl ff node_outputs;
      Format.fprintf ff "@ ";
      Print.list "var @[<hov>" "@ " "@]@ " (Print.post "%a;" vdecl) ff node_locals;
      Print.list "@[<v 2>let@ " "@ " "@]@ tel@]" eq ff node_eqs;

  and enum =
    fun ff ename evariants ->
      Format.fprintf ff "@[<v>@[<h>type@ %s@ =@]%a@]"
        ename
        (Print.seq "@ | " "@ | " "" Format.pp_print_string)
        (StringSet.to_seq evariants)

  and eq =
    fun ff {eq_lhs;eq_rhs;_} ->
      Format.fprintf ff "@[<hov 2>%a@ =@ %a;@]"
        (Print.list "@[<hov>" ",@," "@]" Format.pp_print_string) eq_lhs
        expr eq_rhs

  and expr =
    let pp_ident_fn_expr : Format.formatter -> ident_fn_expr -> unit =
      fun ff {expr_desc=`ExprIdent f;expr_ty;expr_ck;_} ->
        Format.fprintf ff "@[<hov 2>%s(%a@ rate@ %a)@]"
          f
          Types.PP.ty expr_ty
          Structclocks.PP.ck_arrow expr_ck
    in
    let aux =
      fun ff (e:expr) ->
        match e.expr_desc with
        | `ExprConst `ConstInt i -> Format.fprintf ff "%i" i
        | `ExprConst `ConstChar c -> Format.fprintf ff "%c" c
        | `ExprConst `ConstFloat f -> Format.fprintf ff "%f" f
        | `ExprConst `ConstEnum {variant;_} -> Format.fprintf ff "%s" variant
        | `ExprIdent id -> Format.fprintf ff "%s" id
        | `ExprApply (f,args) ->
          Format.fprintf ff "@[<hov 2>%a@,(%a)@]"
            pp_ident_fn_expr f
            (Print.list "@[<hov 2>" ",@ " "@]" expr)
            (args :> expr List.t)
        | `ExprPckUp {rate_expr;rate_factor;rate_ty;rate_ck} ->
          Format.fprintf ff "@[<hov 2>(%a)@ @[<hov 2>*^%i(%a@ rate@ %a)@]@]"
            expr (rate_expr :> expr)
            rate_factor
            Types.PP.ty (rate_ty :> Types.ty_expr)
            Structclocks.PP.ck_arrow rate_ck
        | `ExprFby (cst,{rate_expr;rate_ty;rate_ck;rate_factor=_}) ->
          Format.fprintf ff "@[<hov 2>%a@ @[<hov 2>fby(%a@ rate@ %a)@]@ %a@]"
            expr (cst :> expr)
            Types.PP.ty (rate_ty :> Types.ty_expr)
            Structclocks.PP.ck_arrow rate_ck
            expr (rate_expr :> expr)
        | `ExprWhen {when_expr;case;cond_expr;when_ty;when_ck} ->
          Format.fprintf ff "@[<hov 2>(%a)@ @[<hov 2>when(%a@ rate@ %a)@]@ @[<hov 2>%s->%a@]@]"
            expr (when_expr :> expr)
            Types.PP.ty (when_ty :> Types.ty_expr)
            Structclocks.PP.ck_arrow when_ck
            case
            expr (cond_expr :> expr)
        | `ExprMerge {merge_var;merge_exprs;merge_ty;merge_ck} ->
          Format.fprintf ff "@[<hov 2>merge(%a@ rate@ %a)(%a,@ %a)@]"
            Types.PP.ty (merge_ty :> Types.ty_expr)
            Structclocks.PP.ck_arrow merge_ck
            expr (merge_var :> expr)
            (Print.list
               "" ",@ " ""
               (fun ff (case,e) ->
                  Format.fprintf ff "%s->%a"
                    case expr (e :> expr)))
            merge_exprs
        | `ExprITE {if_expr;then_expr;else_expr;ite_ty;ite_ck} ->
          Format.fprintf ff "@[<hov 2>if(%a@ rate@ %a)@ %a@ then@ %a@ else@ %a@]"
            Types.PP.ty (ite_ty :> Types.ty_expr)
            Structclocks.PP.ck_arrow ite_ck
            expr (if_expr :> expr)
            expr (then_expr :> expr)
            expr (else_expr :> expr)

    in
    fun ff e ->
      Format.fprintf ff "@[<hov>%a@,:@ %a@ rate@ %a@]"
        aux e Types.PP.ty e.expr_ty Structclocks.PP.ck_r e.expr_ck


  and vdecl =
    fun ff v ->
      Format.fprintf ff "@[<hov>%s:@ %a@ rate@ %a@ %a@]"
        v.vdecl_name
        Types.PP.ty v.vdecl_ty
        Structclocks.PP.ck_r v.vdecl_ck
        dl v.vdecl_dl

  and dl =
    fun ff d ->
      Print.option
        (fun ff i -> Format.fprintf ff "due %i" i)
        ""
        ff d
end

module Utils =
struct
  let with_ty_state_2: type a b c. (Structclocks.state -> a -> b -> (Structclocks.state * c)) -> state -> a -> b -> (state*c) =
    fun fn state x y ->
      let typer_state = state.typer_state in
      let typer_state,out = fn typer_state x y in
      {state with typer_state},out


  let with_ty_state_1: type a b. (Structclocks.state -> a -> (Structclocks.state * b)) -> state -> a -> (state*b) =
    fun fn state x ->
    let typer_state = state.typer_state in
    let typer_state,out = fn typer_state x in
    {state with typer_state},out

  let with_ty_state_0: type a. (Structclocks.state -> (Structclocks.state * a)) -> state -> (state*a) =
    fun fn state ->
    let typer_state = state.typer_state in
    let typer_state,out = fn typer_state in
    {state with typer_state},out

  let new_ck state desc scoped =
    (with_ty_state_2 Structclocks.new_ck) state desc scoped

  let ck_r_expr_of_decl state ck_decl scoped =
    (with_ty_state_2 Structclocks.ck_r_expr_of_decl)
      state ck_decl scoped

  let new_ckvar state scoped =
    new_ck state `CkVar scoped

  let new_refvar state scoped =
    let state,ck_ref = new_ckvar state scoped in
    let refinement = Structclocks.RefHole in
    new_ck state (`CkRefine (ck_ref,refinement)) scoped

  let typer_state = Structclocks.Predef.init_state

  let typer_state,pck_up =
    let typer_state,univ = Structclocks.new_ck typer_state `CkUnivar false in
    let typer_state,arg =
      Structclocks.new_ck typer_state
        (`CkRefine (univ,Structclocks.RefHole)) false
    in
    let ck_args = ["@pck_up_e",arg] in
    let typer_state,ck_ref = Structclocks.new_ck typer_state (`Zeta univ) false in
    let refinement = Structclocks.RefHole in
    let typer_state,ck_out =
      Structclocks.new_ck typer_state (`CkRefine ((ck_ref,refinement))) false
    in
    let ck_out = (ck_out :> Structclocks.ck_e) in
    Structclocks.new_ck typer_state
      (`CkArrow (ck_args,ck_out)) false

  let pck_up typer_state =
    match Structclocks.inst_ck_e typer_state IntMap.empty pck_up with
    | Some (typer_state,({ck_desc=`CkArrow _;_} as pck_up),_) ->
      typer_state,pck_up
    | _ -> failwith "Internal error "

  let pck_up state = (with_ty_state_0 pck_up) state

  let fby_ck : Structclocks.state -> (Structclocks.state * Structclocks.ck_arrow_expr) =
    fun typer_state ->
    let typer_state,tyvar = Structclocks.new_ckvar typer_state true in
    let tyvar_v = (tyvar :> Structclocks.ck_v) in
    let tyvar_c = (tyvar :> Structclocks.ck_c) in
    let typer_state,ck_cst =
      Structclocks.new_ck
        typer_state
        (`CkRefine (tyvar_c,Structclocks.RefHole))
        true
    in
    let (typer_state,ck_e) : (_ * Structclocks.ck_r) =
      Structclocks.new_ck
        typer_state
        (`CkRefine (tyvar_c,Structclocks.RefHole))
        true
    in
    let ck_args = ["@fby_cst",ck_cst; "@fby_e",ck_e] in
    let typer_state,ck_zeta = Structclocks.new_ck typer_state (`Zeta tyvar_v) false in
    let refinement = Structclocks.RefHole in
    let typer_state,ck_out =
      Structclocks.new_ck typer_state (`CkRefine ((ck_zeta,refinement))) false
    in
    let ck_out = (ck_out :> Structclocks.ck_e) in
    Structclocks.new_ck typer_state
      (`CkArrow (ck_args,ck_out)) false

  let fby_ck = with_ty_state_0 fby_ck

  let when_ck : Structclocks.state -> String.t -> String.t -> (Structclocks.state * Structclocks.ck_arrow_expr) =
    fun typer_state case cond ->
    let typer_state,tyvar = Structclocks.new_ckvar typer_state true in
    let tyvar_b = (tyvar :> Structclocks.ck_b) in
    let tyvar_c = (tyvar :> Structclocks.ck_c) in
    let (typer_state,ck_e) : (_ * Structclocks.ck_r) =
      Structclocks.new_ck
        typer_state
        (`CkRefine (tyvar_c,Structclocks.RefHole))
        true
    in
    let (typer_state,ck_c) : (_ * Structclocks.ck_r) =
      Structclocks.new_ck
        typer_state
        (`CkRefine (tyvar_c,Structclocks.RefHole))
        true
    in
    let ck_args = ["@when_e",ck_e; "@when_c", ck_c] in
    let typer_state,view = Structclocks.new_refpck typer_state true in
    let on_desc = `On {Structclocks.ck_on=tyvar_b;case;cond;view} in
    let typer_state,ck_on = Structclocks.new_ck typer_state on_desc true in
    let ck_on = (ck_on :> Structclocks.ck_c) in
    let ck_ref = ck_on,Structclocks.RefHole in
    let typer_state,ck_out = Structclocks.new_ck typer_state (`CkRefine ck_ref) true in
    let ck_out = (ck_out :> Structclocks.ck_r) in
    let typer_state,ck_fn =
      Structclocks.new_ck
        typer_state (`CkArrow (ck_args,ck_out)) true
    in
    let ck_fn = (ck_fn :> Structclocks.ck_arrow_expr) in
    typer_state,ck_fn

  let when_ck = (with_ty_state_2 when_ck)

  let merge_ck =
    fun typer_state cond cases ->
    let typer_state,tyvar = Structclocks.new_ckvar typer_state true in
    let tyvar_b = (tyvar :> Structclocks.ck_b) in
    let tyvar_c = (tyvar :> Structclocks.ck_c) in
    let typer_state,ck_merge_var = Structclocks.new_refined_of typer_state tyvar_c true in
    let arg_merge_var = "@merge_c", ck_merge_var in
    let typer_state,view = Structclocks.new_refpck typer_state true in
    let typer_state,arg_exprs =
      List.fold_left_map
        (fun typer_state case ->
           let typer_state,ck =
             Structclocks.new_on_of
               typer_state
               tyvar_b case cond view
               true
           in
           let ck = (ck :> Structclocks.ck_c) in
           let typer_state,ck =
             Structclocks.new_refined_of typer_state ck true
           in
           let arg = "@merge_"^case in
           typer_state, (arg,ck))
        typer_state cases
    in
    let ck_args = arg_merge_var::arg_exprs in
    let typer_state,ck_out = Structclocks.new_refined_of typer_state tyvar_c true in
    let typer_state,ck_fn =
      Structclocks.new_ck
        typer_state (`CkArrow (ck_args,ck_out)) true
    in
    let ck_fn = (ck_fn :> Structclocks.ck_arrow_expr) in
    typer_state,ck_fn

  let merge_ck = with_ty_state_2 merge_ck

  let ite_ck =
    fun typer_state ->
    let typer_state,ref_var_if = Structclocks.new_refvar typer_state true in
    let typer_state,ref_var_then = Structclocks.new_refvar typer_state true in
    let typer_state,ref_var_else = Structclocks.new_refvar typer_state true in
    let typer_state,ck_out = Structclocks.new_refvar typer_state true in
    let ck_args = ["@ite_if", ref_var_if; "@ite_then", ref_var_then; "@ite_else", ref_var_else] in
    let typer_state,ck_fn =
      Structclocks.new_ck
        typer_state (`CkArrow (ck_args,ck_out)) true
    in
    let ck_fn = (ck_fn :> Structclocks.ck_arrow_expr) in
    typer_state,ck_fn

  let ite_ck = with_ty_state_0 ite_ck

  let generalize state ck =
    let typer_state = state.typer_state in
    match Structclocks.gen_ck_arrow typer_state ck with
    | Ok None -> Ok (ck,state)
    | Ok Some (ck,typer_state,_) -> Ok (ck,{state with typer_state})
    | Error x -> Error x

  let subs_v_in_ck_v =
    fun state subs from_ck to_ck ck_v ->
    let subs_state = (subs,state.typer_state) in
    match Structclocks.Substitute.v_in_ck_v_aux subs_state from_ck to_ck ck_v with
    | Some ((subs,typer_state),ck_v) ->
      ({state with typer_state},subs),ck_v
    | None -> (state,subs),ck_v

  let subs_c_in_ck_c =
    fun state subs from_ck to_ck ck_c ->
    let subs_state = (subs,state.typer_state) in
    match Structclocks.Substitute.c_in_ck_c_aux subs_state from_ck to_ck ck_c with
    | Some ((subs,typer_state),ck_c) ->
      {state with typer_state},subs,ck_c
    | None -> state,subs,ck_c

  let subs_c_in_ck_r =
    fun state subs from_ck to_ck ck_r ->
    let subs_state = (subs,state.typer_state) in
    match Structclocks.Substitute.c_in_ck_r_aux subs_state from_ck to_ck ck_r with
    | Some ((subs,typer_state),ck_r) ->
      {state with typer_state},subs,ck_r
    | None -> state,subs,ck_r

  let subs_r_in_ck_r =
    fun state subs from_ck to_ck ck_r ->
    let subs_state = (subs,state.typer_state) in
    match Structclocks.Substitute.r_in_ck_r_aux subs_state from_ck to_ck ck_r with
    | Some ((subs,typer_state),ck_r) ->
      {state with typer_state},subs,ck_r
    | None -> state,subs,ck_r

  let subs_c_in_ck_e =
    fun state subs from_ck to_ck ck_e ->
    let subs_state = (subs,state.typer_state) in
    match Structclocks.Substitute.c_in_ck_e_aux subs_state from_ck to_ck ck_e with
    | Some ((subs,typer_state),ck_e) ->
      {state with typer_state},subs,ck_e
    | None -> state,subs,ck_e

  let subs_r_in_ck_e =
    fun state subs from_ck to_ck ck_e ->
    let subs_state = (subs,state.typer_state) in
    match Structclocks.Substitute.r_in_ck_e_aux subs_state from_ck to_ck ck_e with
    | Some ((subs,typer_state),ck_e) ->
      {state with typer_state},subs,ck_e
    | None -> state,subs,ck_e

  let subs_r_in_ck_arrow =
    fun state subs from_ck to_ck ck_arrow ->
    let subs_state = (subs,state.typer_state) in
    match Structclocks.Substitute.r_in_ck_arrow_aux subs_state from_ck to_ck ck_arrow with
    | Some ((subs,typer_state),ck_arrow) ->
      {state with typer_state},subs,ck_arrow
    | None -> state,subs,ck_arrow

  (* let typer_state = Structclocks.Predef.init_state in
   * let typer_state,pck_up =
   *     let typer_state,univ = Structclocks.new_ck typer_state `CkUnivar false in
   *     let typer_state,arg =
   *       Structclocks.new_ck typer_state
   *         (`CkRefine {Structclocks.ck_ref=univ;refinement=RefHole}) false
   *     in
   *     let ck_args = ["@pck_up_e",arg] in
   *     let typer_state,ck_out =
   *       Structclocks.new_ck typer_state
   *         (`CkRefine {Structclocks.ck_ref=univ;refinement=RefHole}) false
   *     in
   *     Structclocks.new_ck typer_state (`CkArrow {Structclocks.ck_args;ck_out}) false
   *   in
   * let pck_up state = Structclocks.inst_ck_e state IntMap.empty pck_up in
   * let arrow_eq state ty_args ck_out =
   *     let ck_args = List.map (fun ty -> ("",ty)) ty_args in
   *     Structclocks.new_ck state (`TyArrow {Structclocks.ck_args;ck_out}) true
   *   in
   * let init_state main_node =
   *   {
   *     typer_state;
   *     main_node;
   *   }
   * in
   * with_ty_state_2 Structclocks.new_ck,
   * with_ty_state_res_1 Structclocks.generalize,
   * with_ty_state_2 Structclocks.ck_r_expr_of_decl,
   * with_ty_state_1 (fun state -> Structclocks.new_type state `TyVar),
   *   with_ty_state_0 pck_up,
   *   with_ty_state_2 arrow_eq,
   * init_state *)

end

module Make :
sig
  val of_typed : String.t -> Typedlang.prog -> prog
end =
struct
  type enve =
    | CkArrow of Structclocks.ck_arrow_expr
    | CkRef of Structclocks.ck_r

  type env = enve Env.String.t

  type ck_constraint =
    | EqE of Structclocks.ck_e * Structclocks.ck_e
    | EqR of Structclocks.ck_r * Structclocks.ck_r
    | EqC of Structclocks.ck_c * Structclocks.ck_c
    | SqE of Structclocks.ck_e * Structclocks.ck_e
    | SqR of Structclocks.ck_r * Structclocks.ck_r

  type ck_subs = Structclocks.ck_r IntMap.t

  let init_prog = Env.String.empty

  let init_state =
    let typer_state = Utils.typer_state in
    let init_state main_node =
      {
        typer_state;
        main_node
      }
    in
    init_state

  let rec of_typed: String.t -> Typedlang.prog -> prog =
    fun main_node tprog ->
    let state = init_state main_node in
    Env.String.st_map decl_of_typed init_prog state tprog

  and decl_of_typed (env: env) state (decl: Typedlang.decl) =
    let decl_loc = decl.decl_loc in
    match decl.decl_desc with
    | Node ndesc ->
      let ndesc,state = node_of_typed env state decl_loc ndesc in
      let ck = ndesc.node_ck in
      let kvs = [ndesc.node_name, CkArrow ck] in
      kvs,state,{decl_desc=Node ndesc; decl_loc}
    | Enum (ename,evariants,etype) ->
      [],state,{decl_desc=Enum (ename,evariants,etype);decl_loc}

  and node_of_typed env state _loc ndesc =
    let state,node_inputs = vdecls_of_typed state false ndesc.node_inputs in
    let state,node_outputs = vdecls_of_typed state true ndesc.node_outputs in
    let state,node_locals = vdecls_of_typed state true ndesc.node_locals in
    let vdecl_seq =
      List.to_seq node_inputs |>
      Seq.append (List.to_seq node_outputs) |>
      Seq.append (List.to_seq node_locals)
    in
    let internal_env =
      Seq.fold_left
        (fun acc {vdecl_name;vdecl_ck;_} ->
           let ck = vdecl_ck in
           Env.String.add vdecl_name (CkRef ck) acc)
        env vdecl_seq
    in
    let state,node_eqs = eqs_of_typed internal_env state ndesc.node_eqs in
    let state,constraints = eqs_constraints internal_env state node_eqs in
    let state,node_ck = node_clock_of_vars state node_inputs node_outputs in
    let ndesc' =
      {
        node_name = ndesc.node_name;
        node_inputs;
        node_locals;
        node_outputs;
        node_eqs;
        node_ty = ndesc.node_ty;
        node_ck
      }
    in
    Print.vdebug Print.DebugSClocked "%a@." PP.node ndesc';
    let init_subs = init_substitutions node_eqs node_inputs node_outputs node_locals in
    let state,substitutions = solve_constraints state constraints init_subs in
    let state,node_inputs = vdecls_ck_subs state substitutions node_inputs in
    let state,node_outputs = vdecls_ck_subs state substitutions node_outputs in
    let state,node_locals = vdecls_ck_subs state substitutions node_locals in
    let state,node_eqs = eqs_ck_subs state substitutions node_eqs in
    let state,node_ck = node_clock_of_vars state node_inputs node_outputs in
    let ndesc =
      {
        node_name = ndesc.node_name;
        node_inputs;
        node_locals;
        node_outputs;
        node_eqs;
        node_ty = ndesc.node_ty;
        node_ck
      }
    in
    (* Format.printf "%a@." PP.node ndesc; *)
    match Utils.generalize state node_ck with
    | Ok (node_ck,state) ->
      {ndesc with node_ck},state
    | Error ck ->
      failwith @@ Format.asprintf "Could not generalize node %s due to clock %a"
        ndesc.node_name Structclocks.PP.ck ck

  and node_clock_of_vars state inputs outputs =
    let ck_args =
      List.map
        (fun (v:vdecl) ->
           (v.vdecl_name,v.vdecl_ck))
        inputs
    in
    let state,ck_out = clock_of_vdecls state outputs in
    Utils.new_ck state (`CkArrow (ck_args,ck_out)) false

  and eqs_of_typed  env state eqs =
    List.fold_left_map
      (fun state eq ->
         eq_of_typed env state eq)
      state eqs

  and eq_of_typed env state ({eq_lhs;eq_rhs;eq_loc}: Typedlang.eq) =
    let state,eq_rhs = expr_of_typed env state eq_rhs in
    let eq: eq = {eq_lhs;eq_rhs;eq_loc} in
    state,eq

  and expr_of_typed env state (expr: Typedlang.expr): state*expr =
    let expr_loc = expr.expr_loc in
    match expr with
    | ({expr_desc=`ExprConst _;_} as expr) ->
      let state,expr = const_expr_of_typed state expr in
      state,(expr :> expr)
    | ({expr_desc=`ExprIdent _;_} as expr) ->
      let state,expr = ident_expr_of_typed env state expr in
      state,(expr :> expr)
    | {expr_desc=((`ExprApply _|`ExprITE _|
                   `ExprPckUp _|`ExprFby _|
                   `ExprWhen _|`ExprMerge _)
                  as expr_desc);expr_ty;_} ->
      let state,expr_desc =
        match expr_desc with
        | `ExprApply (f,args) ->
          let state,f = ident_fn_expr_of_typed env state f in
          let state,args =
            List.fold_left_map
              (ident_expr_of_typed env)
              state args
          in
          state,`ExprApply (f,args)
        | `ExprPckUp {rate_expr;rate_factor;rate_ty} ->
          let state,rate_expr = ident_expr_of_typed env state rate_expr in
          let state,rate_ck = Utils.pck_up state in
          state,`ExprPckUp {rate_expr;rate_factor;rate_ty;rate_ck}
        | `ExprFby (cst,{rate_expr;rate_factor;rate_ty}) ->
          let state,rate_expr = ident_expr_of_typed env state rate_expr in
          let state,rate_ck = Utils.fby_ck state in
          let state,cst = const_expr_of_typed state cst in
          state,`ExprFby (cst,{rate_expr;rate_factor;rate_ty;rate_ck})
        | `ExprWhen {when_expr;case;cond_expr;when_ty} ->
          let state,when_expr = ident_expr_of_typed env state when_expr in
          let state,cond_expr = ident_expr_of_typed env state cond_expr in
          let {expr_desc=`ExprIdent cond;_} = cond_expr in
          let state,when_ck = Utils.when_ck state case cond in
          state,`ExprWhen {when_expr;case;cond_expr;when_ty;when_ck}
        | `ExprMerge {merge_var;merge_exprs;merge_ty} ->
          let state,merge_var = ident_expr_of_typed env state merge_var in
          let state,merge_exprs =
            List.fold_left_map
              (fun state (case,e) ->
                 let state,e = ident_expr_of_typed env state e in
                 state,(case,e))
              state merge_exprs
          in
          let {expr_desc=`ExprIdent cond;_} = merge_var in
          let cases = List.map fst merge_exprs in
          let state,merge_ck = Utils.merge_ck state cond cases in
          state,`ExprMerge {merge_var;merge_exprs;merge_ty;merge_ck}
        | `ExprITE {if_expr;then_expr;else_expr;ite_ty} ->
          let state,if_expr = ident_expr_of_typed env state if_expr in
          let state,then_expr = ident_expr_of_typed env state then_expr in
          let state,else_expr = ident_expr_of_typed env state else_expr in
          let state,ite_ck = Utils.ite_ck state in
          state,`ExprITE {if_expr;then_expr;else_expr;ite_ty;ite_ck}

      in
      let state,expr_ck = Utils.new_refvar state true in
      let expr_ck = (expr_ck :> Structclocks.ck_r) in
      state,{expr_desc;expr_ck;expr_ty;expr_loc}

  and ident_expr_of_typed env state (expr: Typedlang.ident_expr) : state * ident_expr =
    let expr_loc = expr.expr_loc in
    let expr_ty = expr.expr_ty in
    let {Typedlang.expr_desc=((`ExprIdent v) as expr_desc);_} = expr in
    begin
      match Env.String.curr env |> Env.String.Map.find v with
      | CkRef ({ck_desc=`CkRefine _;_} as expr_ck) ->
        (* Just re-use the variable of the environment ... I think ? *)
        state,{expr_desc;expr_loc;expr_ty;expr_ck}
      | CkArrow _ -> failwith "Internal error"
    end

  and ident_fn_expr_of_typed : env -> state -> Typedlang.ident_expr -> state*ident_fn_expr =
    fun env state expr ->
    let expr_loc = expr.expr_loc in
    let expr_ty = expr.expr_ty in
    let {Typedlang.expr_desc=((`ExprIdent v) as expr_desc);_} = expr in
    begin
      match Env.String.curr env |> Env.String.Map.find v with
      | CkArrow expr_ck ->
        (* Just re-use the variable of the environment ... I think ? *)
        state,{expr_desc;expr_loc;expr_ty;expr_ck}
      | CkRef _ -> failwith "Internal error"
    end


  and const_expr_of_typed: state -> Typedlang.const_expr -> state*const_expr =
    fun state -> function
      | {expr_desc=`ExprConst cst;expr_ty;expr_loc} ->
        let state,expr_ck = Utils.new_refvar state true in
        let cst = const_of_typed cst in
        state,{expr_desc=`ExprConst cst;expr_ty;expr_ck;expr_loc}

  and const_of_typed: Typedlang.const -> const =
    function
    | (`ConstInt _|`ConstChar _|`ConstFloat _) as a -> a
    | `ConstEnum {variant;enum} -> `ConstEnum {variant;enum}

  and eqs_constraints env state eqs =
    let state,constraints =
      List.fold_left_map
      (fun state eq -> eq_constraints env state eq)
      state eqs
    in
    state,List.concat constraints

  and eq_constraints env state ({eq_lhs;eq_rhs;_}:eq) =
    let state,constraints = expr_constraints env state eq_rhs in
    let state,lhs_ck = clock_of_idents env state eq_lhs in
    let rhs_ck = (eq_rhs.expr_ck :> Structclocks.ck_e) in
    state,(EqE (lhs_ck,rhs_ck))::constraints

  and expr_constraints env state expr =
    match expr with
    | {expr_desc=`ExprConst _|`ExprIdent _;_} ->
      state,[]
    | {expr_desc=(`ExprApply _|`ExprPckUp _|`ExprFby _|`ExprWhen _|`ExprMerge _|`ExprITE _) as expr_desc;expr_ck;_}
      ->
      let args =
        match expr_desc with
        | `ExprApply (_,args) ->
          (args :> atom_expr List.t)
        | `ExprPckUp {rate_expr;_} ->
          ([rate_expr] :> atom_expr List.t)
        | `ExprFby (cst,{rate_expr;_}) ->
          [(cst :> atom_expr); (rate_expr :> atom_expr)]
        | `ExprWhen {when_expr;cond_expr;_} ->
          ([when_expr; cond_expr] :> atom_expr List.t)
        | `ExprMerge {merge_var;merge_exprs;_} ->
          (merge_var::(List.map snd merge_exprs) :> atom_expr List.t)
        | `ExprITE {if_expr;then_expr;else_expr;_} ->
          ([if_expr; then_expr; else_expr] :> atom_expr List.t)
      in
      let args = (args :> expr list) in
      let state,constraints =
        List.fold_left_map
          (fun state arg -> expr_constraints env state (arg :> expr))
          state args
      in
      let constraints = List.concat constraints in
      let expr_ck = (expr_ck :> Structclocks.ck_e) in
      let ck_args = List.map (fun arg -> ("",arg.expr_ck)) args in
      let state,eq_ck = Utils.new_ck state
          (`CkArrow (ck_args,expr_ck))
          true
      in
      let ck_fn =
        match expr_desc with
        | `ExprApply (f,_) ->
          (f.expr_ck :> Structclocks.ck_e)
        | (`ExprPckUp {rate_ck;_}|`ExprFby (_,{rate_ck;_})) ->
          (rate_ck :> Structclocks.ck_e)
        | `ExprWhen {when_ck;_} ->
          (when_ck :> Structclocks.ck_e)
        | `ExprMerge {merge_ck;_} ->
          (merge_ck :> Structclocks.ck_e)
        | `ExprITE {ite_ck;_} ->
          (ite_ck :> Structclocks.ck_e)
      in
      state,(EqE (eq_ck,ck_fn))::constraints

  and solve_constraints : state -> ck_constraint List.t -> ck_subs -> state * ck_subs =
    fun state constraints (substitutions: ck_subs) ->
    Print.vdebug DebugSClocked "%a@.%a@."
      (Print.int_map "" "@." "@."
         (fun ff (id,ckr) ->
            Format.fprintf ff "%i -> %a" id Structclocks.PP.ck_r ckr))
      substitutions
      (Print.list "" "@." "@."
         (fun ff -> function
            | EqC (ck1,ck2) ->
              Format.fprintf ff "EqC@[<hov>(%a,@ %a)@]" Structclocks.PP.ck_c ck1 Structclocks.PP.ck_c ck2
            | EqR (ck1,ck2) ->
              Format.fprintf ff "EqR@[<hov>(%a,@ %a)@]" Structclocks.PP.ck_r ck1 Structclocks.PP.ck_r ck2
            | EqE (ck1,ck2) ->
              Format.fprintf ff "EqE@[<hov>(%a,@ %a)@]" Structclocks.PP.ck_e ck1 Structclocks.PP.ck_e ck2
            (* | SqC (ck1,ck2) ->
             *   Format.fprintf ff "SqC@[<hov>(%a,@ %a)@]" Structclocks.PP.ck_c ck1 Structclocks.PP.ck_c ck2 *)
            | SqR (ck1,ck2) ->
              Format.fprintf ff "SqR@[<hov>(%a,@ %a)@]" Structclocks.PP.ck_r ck1 Structclocks.PP.ck_r ck2
            | SqE (ck1,ck2) ->
              Format.fprintf ff "SqE@[<hov>(%a,@ %a)@]" Structclocks.PP.ck_e ck1 Structclocks.PP.ck_e ck2))
      constraints;
    match constraints with
    | [] ->
      state,substitutions
    | (EqC (ck1,ck2))::constraints when ck1=ck2 ->
      (* types are identical, do nothing *)
      solve_constraints state constraints substitutions
    | (EqR (ck1,ck2))::constraints when ck1=ck2 ->
      (* types are identical, do nothing *)
      solve_constraints state constraints substitutions
    | (SqR (ck1,ck2))::constraints when ck1=ck2 ->
      (* types are identical, do nothing *)
      solve_constraints state constraints substitutions
    | (EqE (ck1,ck2))::constraints when ck1=ck2 ->
      (* types are identical, do nothing *)
      solve_constraints state constraints substitutions
    | (SqE (ck1,ck2))::constraints when ck1=ck2 ->
      (* types are identical, do nothing *)
      solve_constraints state constraints substitutions
    | (EqC (({ck_desc=`CkVar;_} as ckvar), ({ck_desc=`Pck|`On _|`Zeta _;_} as ck)))::constraints
    | (EqC (({ck_desc=`Pck|`On _|`Zeta _;_} as ck), ({ck_desc=`CkVar;_} as ckvar)))::constraints
      ->
      let subs = Structclocks.Substitute.no_subs in
      let state,subs,substitutions = substitute_c_substitutions state subs ckvar ck substitutions in
      let state,_,constraints = substitute_c_constraints state subs ckvar ck constraints in
      solve_constraints state constraints substitutions
    | (EqC (({ck_desc=`CkVar;_} as ck1),({ck_desc=`CkVar;_} as ck2)))::constraints ->
      let older,newer = if ck1.ck_id < ck2.ck_id then ck1,ck2 else ck2,ck1 in
      let subs = Structclocks.Substitute.no_subs in
      let state,subs,substitutions = substitute_c_substitutions state subs newer older substitutions in
      let state,_,constraints = substitute_c_constraints state subs newer older constraints in
      solve_constraints state constraints substitutions
    | EqC(({ck_desc=`On {view=v1;case=ca1;cond=co1;ck_on=onned1};_} as on1),
          ({ck_desc=`On {view=v2;case=ca2;cond=co2;ck_on=onned2};_} as on2))
      ::constraints
      when ca1=ca2 && co1=co2
      ->
      let older,newer = if on1.ck_id < on2.ck_id then  on1,on2 else on2,on1 in
      let constraints = EqC((onned1 :> Structclocks.ck_c),
                            (onned2 :> Structclocks.ck_c))::constraints in
      let subs = Structclocks.Substitute.no_subs in
      let state,subs,substitutions = substitute_c_substitutions state subs newer older substitutions in
      let state,_,constraints = substitute_c_constraints state subs newer older constraints in
      let constraints = (EqR(v1,v2))::constraints in
      solve_constraints state constraints substitutions
    | ((EqE(({ck_desc=`CkRefine (ckr1,RefHole);_} as ck1),
            ({ck_desc=`CkRefine (ckr2,RefHole);_} as ck2)))
      |(EqR(({ck_desc=`CkRefine (ckr1,RefHole);_} as ck1),
            ({ck_desc=`CkRefine (ckr2,RefHole);_} as ck2))))
      ::constraints ->
      let constraints =
        EqC(ckr1,ckr2)::constraints
      in
      let older,newer = if ck1.ck_id < ck2.ck_id then ck1,ck2 else ck2,ck1 in
      let subs = Structclocks.Substitute.no_subs in
      let state,subs,substitutions = substitute_r_substitutions state subs newer older substitutions in
      let state,_,constraints = substitute_r_constraints state subs newer older constraints in
      solve_constraints state constraints substitutions
    | SqR({ck_desc=`CkRefine (ckr1, RefHole);_},
          {ck_desc=`CkRefine (ckr2, RefHole);_})
      ::constraints
    | SqE({ck_desc=`CkRefine (ckr1, RefHole);_},
          {ck_desc=`CkRefine (ckr2, RefHole);_})
      ::constraints ->
      let constraints =
        EqC(ckr1,ckr2)::constraints
      in
      solve_constraints state constraints substitutions
    | EqE(({ck_desc=`CkArrow (args1, out1);_}),
          ({ck_desc=`CkArrow (args2, out2);_}))
      ::constraints
      when List.length args1 = List.length args2 ->
      let new_constraints =
        List.map2 (fun (_,ck1) (_,ck2) -> SqR (ck1,ck2)) args1 args2
      in
      let new_constraints = List.append new_constraints [SqE (out1,out2)] in
      let constraints = List.append new_constraints constraints in
      solve_constraints state constraints substitutions
    | EqC(({ck_desc=`On {view;_};_} as ck_on),
          ({ck_desc=`Zeta ck_zvar;_} as ck_zeta))
      ::constraints
    | EqC(({ck_desc=`Zeta ck_zvar;_} as ck_zeta),
          ({ck_desc=`On {view;_};_} as ck_on))
      ::constraints
      ->
      let subs = Structclocks.Substitute.no_subs in
      let ck_zvar_c = (ck_zvar :> Structclocks.ck_c) in
      let state,subs,new_clock = Utils.subs_c_in_ck_c state subs ck_zvar_c ck_on ck_zeta in
      let new_view = match new_clock.ck_desc with `On {view;_} -> view | _ -> failwith "Internal error" in
      let state,subs,substitutions = substitute_c_substitutions state subs ck_on new_clock substitutions in
      let state,subs,substitutions = substitute_c_substitutions state subs ck_zeta new_clock substitutions in
      let state,subs,substitutions = substitute_r_substitutions state subs view new_view substitutions in
      let state,subs,constraints = substitute_c_constraints state subs ck_on new_clock constraints in
      let state,subs,constraints = substitute_c_constraints state subs ck_zeta new_clock constraints in
      let state,_,constraints = substitute_r_constraints state subs view new_view constraints in
      solve_constraints state constraints substitutions
    | EqC({ck_desc=`Pck;ck_id=id1;ck_scoped=scoped1},{ck_desc=`Pck;ck_id=id2;ck_scoped=scoped2})::_ ->
      failwith @@
      Format.asprintf "EqC pck/pck %i/%i %b/%b" id1 id2 scoped1 scoped2
    | EqC(ck1,ck2)::_ ->
      failwith (Format.asprintf "EqC %a %a" Structclocks.PP.ck_c ck1 Structclocks.PP.ck_c ck2)
    | EqE(ck1,ck2)::_ ->
      failwith (Format.asprintf "EqE %a %a" Structclocks.PP.ck_e ck1 Structclocks.PP.ck_e ck2)
    | SqE(ck1,ck2)::_ ->
      failwith (Format.asprintf "SqE %a %a" Structclocks.PP.ck_e ck1 Structclocks.PP.ck_e ck2)

  and substitute_c_constraints : state -> Structclocks.Substitute.subs -> Structclocks.ck_c -> Structclocks.ck_c -> ck_constraint List.t ->  state * Structclocks.Substitute.subs * ck_constraint List.t =
      fun state subs (from_ck: Structclocks.ck_c) to_ck constraints ->
      let (state,subs),constraints =
        List.fold_left_map
          (fun (state,subs) ->
             function
             | EqC (ck1,ck2) ->
               let state,subs,ck1 = Utils.subs_c_in_ck_c state subs from_ck to_ck ck1 in
               let state,subs,ck2 = Utils.subs_c_in_ck_c state subs from_ck to_ck ck2 in
               (state,subs),EqC(ck1,ck2)
             (* | SqC (ck1,ck2) ->
              *   let ck1 = ck_c_subs_c from_ck to_ck ck1 in
              *   let ck2 = ck_c_subs_c from_ck to_ck ck2 in
              *   SqC(ck1,ck2) *)
             | EqR (ck1,ck2) ->
               let state,subs,ck1 = Utils.subs_c_in_ck_r state subs from_ck to_ck ck1 in
               let state,subs,ck2 = Utils.subs_c_in_ck_r state subs from_ck to_ck ck2 in
               (state,subs),EqR(ck1,ck2)
             | SqR (ck1,ck2) ->
               let state,subs,ck1 = Utils.subs_c_in_ck_r state subs from_ck to_ck ck1 in
               let state,subs,ck2 = Utils.subs_c_in_ck_r state subs from_ck to_ck ck2 in
               (state,subs),SqR(ck1,ck2)
             | EqE (ck1,ck2) ->
               let state,subs,ck1 = Utils.subs_c_in_ck_e state subs from_ck to_ck ck1 in
               let state,subs,ck2 = Utils.subs_c_in_ck_e state subs from_ck to_ck ck2 in
               (state,subs),EqE(ck1,ck2)
             | SqE (ck1,ck2) ->
               let state,subs,ck1 = Utils.subs_c_in_ck_e state subs from_ck to_ck ck1 in
               let state,subs,ck2 = Utils.subs_c_in_ck_e state subs from_ck to_ck ck2 in
               (state,subs),SqE(ck1,ck2))
          (state,subs) constraints
      in
      state,subs,constraints

  and substitute_r_constraints : state -> Structclocks.Substitute.subs -> Structclocks.ck_r -> Structclocks.ck_r -> ck_constraint List.t -> state * Structclocks.Substitute.subs * ck_constraint List.t =
    fun state subs from_ck to_ck constraints ->
    let (state,subs),constraints =
      List.fold_left_map
        (fun (state,subs) ->
           function
           | (EqC _) as s -> (state,subs),s
           (* | (SqC _) as s -> s *)
           | EqR (ck1,ck2) ->
             let state,subs,ck1 = Utils.subs_r_in_ck_r state subs from_ck to_ck ck1 in
             let state,subs,ck2 = Utils.subs_r_in_ck_r state subs from_ck to_ck ck2 in
             (state,subs),EqR(ck1,ck2)
           | SqR (ck1,ck2) ->
             let state,subs,ck1 = Utils.subs_r_in_ck_r state subs from_ck to_ck ck1 in
             let state,subs,ck2 = Utils.subs_r_in_ck_r state subs from_ck to_ck ck2 in
             (state,subs),SqR(ck1,ck2)
           | EqE (ck1,ck2) ->
             let state,subs,ck1 = Utils.subs_r_in_ck_e state subs from_ck to_ck ck1 in
             let state,subs,ck2 = Utils.subs_r_in_ck_e state subs from_ck to_ck ck2 in
             (state,subs),EqE(ck1,ck2)
           | SqE (ck1,ck2) ->
             let state,subs,ck1 = Utils.subs_r_in_ck_e state subs from_ck to_ck ck1 in
             let state,subs,ck2 = Utils.subs_r_in_ck_e state subs from_ck to_ck ck2 in
             (state,subs),SqE(ck1,ck2))
        (state,subs) constraints
    in
    state,subs,constraints

  and substitute_c_substitutions state subs (from_ck: Structclocks.ck_c) to_ck substitutions =
    IntMap.fold
      (fun ck_id ck (state,subs,map) ->
         let state,subs,ck = Utils.subs_c_in_ck_r state subs from_ck to_ck ck in
         let map = IntMap.add ck_id ck map in
         state,subs,map)
      substitutions
      (state,subs,IntMap.empty)

  and substitute_r_substitutions state subs from_ck to_ck substitutions =
    IntMap.fold
      (fun ck_id ck (state,subs,map) ->
         let state,subs,ck = Utils.subs_r_in_ck_r state subs from_ck to_ck ck in
         let map = IntMap.add ck_id ck map in
         state,subs,map)
      substitutions
      (state,subs,IntMap.empty)

  and vdecls_of_typed state scoped vdecls =
    List.fold_left_map
      (fun state v -> vdecl_of_typed state scoped v)
      state vdecls

  and vdecl_of_typed state scoped v : state*vdecl =
    let {vdecl_name;vdecl_ty_decl;vdecl_ck_decl;vdecl_dl;vdecl_ty} : Typedlang.vdecl = v in
    let state,vdecl_ck = Utils.ck_r_expr_of_decl state vdecl_ck_decl scoped in
    state,
    {
      vdecl_name;
      vdecl_ty_decl;
      vdecl_ck_decl;
      vdecl_dl;
      vdecl_ty;
      vdecl_ck;
    }

  and vdecls_ck_subs state substitutions vdecls =
    List.fold_left_map
      (fun state v -> vdecl_ck_subs state substitutions v)
      state vdecls

  and eqs_ck_subs state substitutions eqs =
    List.fold_left_map
      (fun state eq -> eq_ck_subs state substitutions eq)
      state eqs

  and eq_ck_subs state substitutions eq =
    let state,eq_rhs = expr_ck_subs state substitutions eq.eq_rhs in
    state,{eq with eq_rhs}

  and expr_ck_subs : state -> ck_subs -> expr -> state * expr =
    fun state substitutions expr ->
    match expr with
    | ({expr_desc=(`ExprConst _|`ExprIdent _);_} as expr) ->
      let state,expr = atom_expr_ck_subs state substitutions expr in
      state,(expr :> expr)
    | ({expr_desc=(`ExprApply _|`ExprPckUp _|`ExprFby _|`ExprWhen _|`ExprMerge _|`ExprITE _) as expr_desc;
        expr_ck;_} as expr) ->
      let ck_fn =
        match expr_desc with
        | `ExprApply (f,_) ->
          f.expr_ck
        | `ExprPckUp {rate_ck;_} | `ExprFby (_,{rate_ck;_}) ->
          rate_ck
        | `ExprWhen {when_ck;_} -> when_ck
        | `ExprMerge {merge_ck;_} -> merge_ck
        | `ExprITE {ite_ck;_} -> ite_ck
      in
      let state,ck_fn = ck_arrow_ck_subs state substitutions ck_fn in
      let state,expr_desc =
        match expr_desc with
        | `ExprApply (f,args) ->
          let state,f = ident_fn_expr_ck_subs state substitutions f in
          let state,args =
            List.fold_left_map
              (fun state arg -> ident_expr_ck_subs state substitutions arg)
              state args
          in
          state,`ExprApply (f,args)
        | `ExprPckUp ({rate_expr;_} as rate_desc) ->
          let state,rate_expr = ident_expr_ck_subs state substitutions rate_expr in
          state,`ExprPckUp {rate_desc with rate_ck=ck_fn;rate_expr}
        | `ExprFby (cst,({rate_expr;_} as rate_desc)) ->
          let state,rate_expr = ident_expr_ck_subs state substitutions rate_expr in
          state,`ExprFby (cst,{rate_desc with rate_ck=ck_fn;rate_expr})
        | `ExprWhen ({when_expr;cond_expr;_} as when_desc) ->
          let state,when_expr = ident_expr_ck_subs state substitutions when_expr in
          let state,cond_expr = ident_expr_ck_subs state substitutions cond_expr in
          state,`ExprWhen {when_desc with when_expr;cond_expr;when_ck=ck_fn}
        | `ExprMerge ({merge_var;merge_exprs;_} as merge_desc) ->
          let state,merge_var = ident_expr_ck_subs state substitutions merge_var in
          let state,merge_exprs =
            List.fold_left_map
              (fun state (case,e) ->
                 let state,e = ident_expr_ck_subs state substitutions e in
                 state,(case,e))
              state merge_exprs
          in
          state,`ExprMerge {merge_desc with merge_var;merge_exprs;merge_ck=ck_fn}
        | `ExprITE ({if_expr;then_expr;else_expr;_} as if_desc) ->
          let state,if_expr = ident_expr_ck_subs state substitutions if_expr in
          let state,then_expr = ident_expr_ck_subs state substitutions then_expr in
          let state,else_expr = ident_expr_ck_subs state substitutions else_expr in
          state,`ExprITE {if_desc with if_expr;then_expr;else_expr;ite_ck=ck_fn}
      in
      let state,expr_ck = ck_r_ck_subs state substitutions expr_ck in
      state,{expr with expr_ck;expr_desc}

  and atom_expr_ck_subs : state -> ck_subs -> atom_expr -> state * atom_expr  =
    fun state substitutions expr ->
    let state,expr_ck = ck_r_ck_subs state substitutions expr.expr_ck in
    state,{expr with expr_ck}

  and ident_expr_ck_subs : state -> ck_subs -> ident_expr -> state * ident_expr =
    fun state substitutions expr ->
    let state,expr_ck = ck_r_ck_subs state substitutions expr.expr_ck in
    state,{expr with expr_ck}

  and ident_fn_expr_ck_subs : state -> ck_subs -> ident_fn_expr -> state * ident_fn_expr =
    fun state substitutions expr ->
    let state,expr_ck = ck_arrow_ck_subs state substitutions expr.expr_ck in
    state,{expr with expr_ck}

  and vdecl_ck_subs state substitutions v =
    let state,vdecl_ck = ck_r_ck_subs state substitutions v.vdecl_ck in
    state,{v with vdecl_ck}

  and ck_r_ck_subs_aux state substitutions (ck_r: Structclocks.ck_r) =
    match IntMap.find_opt ck_r.ck_id substitutions with
    | Some ck_r ->
      Some(ck_r,state)
    | None -> None

  and ck_e_ck_subs_aux state substitutions ck_e =
    match ck_e with
    | ({Structclocks.ck_desc=`CkRefine _;_} as ck_r) ->
      ((ck_r_ck_subs_aux state substitutions ck_r) :> ((Structclocks.ck_e*state) option))
    | _ -> failwith "f"

  and ck_arrow_ck_subs_aux state substitutions ck_arrow =
    let {Structclocks.ck_desc=`CkArrow (ck_args,ck_out);_} = ck_arrow in
    let ck_out,state,changed =
      match ck_e_ck_subs_aux state substitutions ck_out with
      | Some (ck_out,state) -> ck_out,state,true
      | None -> ck_out,state,false
    in
    let (state,changed),ck_args =
      List.fold_left_map
        (fun (state,changed) (var,ck) ->
           match ck_r_ck_subs_aux state substitutions ck with
           | Some (ck,state) -> (state,true),(var,ck)
           | None -> (state,changed),(var,ck))
        (state,changed) ck_args
    in
    if changed then
      let state,ck_arrow = Utils.new_ck state (`CkArrow (ck_args,ck_out)) ck_arrow.ck_scoped in
      Some(ck_arrow,state)
    else
      None

  and ck_r_ck_subs state substitutions ck_r =
    match ck_r_ck_subs_aux state substitutions ck_r with
    | Some(ck_r,state) -> state,ck_r
    | None -> state,ck_r

  and ck_arrow_ck_subs state substitutions ck_e =
    match ck_arrow_ck_subs_aux state substitutions ck_e with
    | Some (ck_e,state) -> state,ck_e
    | None -> state,ck_e

  and clock_of_vdecls (state: state) (vdecls: vdecl list) =
    match vdecls with
    | [] -> failwith "Internal error"
    | [{vdecl_ck;_}] ->
      let vdecl_ck = (vdecl_ck :> Structclocks.ck_e) in
      state,vdecl_ck
    | vdecls ->
      let cks = List.map (fun (v:vdecl) -> v.vdecl_ck) vdecls in
      Utils.new_ck state (`CkTuple cks) false

  and clock_of_idents env state idents : state*Structclocks.ck_e =
    match idents with
    | [] -> failwith "Internal error"
    | [x] ->
      begin
        match Env.String.curr env |> Env.String.Map.find x with
        | CkRef ty ->
          let ty = (ty :> Structclocks.ck_e) in
          state,ty
        | CkArrow _ -> failwith "Internal error"
      end
    | xs ->
      let map = Env.String.curr env in
      let tys = List.map
          (fun v ->
             match Env.String.Map.find v map with
             | CkRef ty_expr -> ty_expr
             | CkArrow _ -> failwith "Internal error" )
          xs
      in
      Utils.new_ck state (`CkTuple tys) true

  and init_substitutions =
    let subs_of_atom_expr subs (e: atom_expr) =
      IntMap.add e.expr_ck.ck_id e.expr_ck subs
    in
    let subs_of_ck_out: _ -> Structclocks.ck_e -> _ =
      fun subs ->
        function
        | ({ck_desc=`CkRefine _;ck_id;_} as ck_r) ->
          IntMap.add ck_id ck_r subs
        | {ck_desc=`CkTuple ckl;_} ->
          List.fold_left
            (fun subs ck_r -> IntMap.add ck_r.Structclocks.ck_id ck_r subs)
            subs ckl
        | {ck_desc=`CkArrow _;_} -> failwith "Internal error"
    in
    let subs_of_arrow subs =
      function
      | {Structclocks.ck_desc=`CkArrow (ck_args,ck_out);_} ->
        let subs =
          List.to_seq ck_args
          |> Seq.map snd
          |> Seq.fold_left (fun subs ck -> IntMap.add ck.Structclocks.ck_id ck subs) subs
        in
        subs_of_ck_out subs ck_out
    in
    let subs_of_expr subs (e: expr) =
      match e with
      | ({expr_desc=(`ExprConst _|`ExprIdent _);_} as e) ->
        subs_of_atom_expr subs e
      | {expr_desc=(`ExprApply _|`ExprPckUp _|`ExprFby _|`ExprWhen _|`ExprMerge _|`ExprITE _) as expr_desc;expr_ck;_} ->
        let subs = IntMap.add expr_ck.ck_id expr_ck subs in
        let ck_args =
          match expr_desc with
          | `ExprApply (_,args) -> (args :> atom_expr List.t)
          | (`ExprPckUp {rate_expr;_}|`ExprFby (_,{rate_expr;_})) ->
            [(rate_expr :> atom_expr)]
          | `ExprWhen {when_expr;cond_expr;_} ->
            ([when_expr; cond_expr] :> atom_expr List.t)
          | `ExprMerge {merge_var;merge_exprs;_} ->
            (merge_var::(List.map snd merge_exprs) :> atom_expr List.t)
          | `ExprITE {if_expr;then_expr;else_expr;_} ->
            ([if_expr; then_expr; else_expr] :> atom_expr List.t)
        in
        let ck_fn =
          match expr_desc with
          | `ExprApply ({expr_ck;_},_) -> expr_ck
          | `ExprPckUp {rate_ck;_} | `ExprFby (_,{rate_ck;_}) -> rate_ck
          | `ExprWhen {when_ck;_} -> when_ck
          | `ExprMerge {merge_ck;_} -> merge_ck
          | `ExprITE {ite_ck;_} -> ite_ck
        in
        let subs = subs_of_arrow subs ck_fn in
        List.fold_left subs_of_atom_expr subs ck_args
    in
    let subs_of_eq subs {eq_rhs;_} =
      subs_of_expr subs eq_rhs
    in
    let subs_of_vdecl subs vdecl =
      IntMap.add vdecl.vdecl_ck.ck_id vdecl.vdecl_ck subs
    in
    fun eqs inputs outputs locals ->
      let subs = List.fold_left subs_of_eq IntMap.empty eqs in
      let subs = List.fold_left subs_of_vdecl subs inputs in
      let subs = List.fold_left subs_of_vdecl subs outputs in
      List.fold_left subs_of_vdecl subs locals

end

type t = stmt list

and stmt =
  | Assert of expr
  | Minimize of expr
  | DeclareFun of {fname: string; domain: sort list; range: sort}
  | DefineFun of {fname: string; args: (string * sort) list; range: sort; body: expr}
  | DeclareDatatype of string * constructor list
  | CheckSat
  | GetModel
  | GetValue of string list
  | SetOption of smtoption
  | SetLogic of smtlogic
  | Push
  | Pop
  | Reset

and expr =
  | Bool of bool
  | Int of int
  | Id of string
  | Appl of string * expr list
  | ForAll of (string * sort) list * expr

and sort =
  | IntSort
  | BoolSort
  | Sort of string

and constructor = { consname: string; members: member list}

and member = {accessor: string; sort: sort}

and smtoption =
  | ProduceModels of bool
  | ProduceUnsatCores of bool

and smtlogic =
  | ALL
  | LIA
  | QF_UFLIA
  | NIA
  | QF_UFNIA

and sat =
  | SAT
  | UNSAT

let ck_sort = Sort "Ck"

let ite i t e =
  Appl("ite", [i; t; e])

let lnot e =
  Appl("not", [e])

let pand es =
  match es with
  | [] -> Bool true
  | [e] -> e
  | _ ->
    Appl("and", es)

let por es =
  match es with
  | [] -> Bool true
  | [e] -> e
  | _ ->
    Appl("or", es)

let nand es =
  lnot@@pand es

let nor es =
  lnot@@por es

let impl a b =
  Appl("=>", [a;b])

let eq es =
  match es with
  | [] | _::[] -> Bool true
  | _ -> Appl("=", es)

let neq es =
  lnot@@eq es

let ge a b =
  Appl(">=", [a; b])

let plulib : t =
  [
    SetOption (ProduceModels true);
    SetOption (ProduceUnsatCores true);
    SetLogic QF_UFLIA;

    DefineFun {fname="isdiv"; args=["n", IntSort; "k", IntSort]; range=BoolSort;
               body=ite
                   (pand [ge (Id "k") (Int 1); ge (Id "n") (Int 1)])
                   (Appl ("=", [Appl ("mod", [Id "n"; Id "k"]); Int 0]))
                   (Bool false)
              };


    DefineFun {fname="ckdiv"; args=["n", IntSort; "k", IntSort]; range=IntSort;
               body=ite
                   (Appl ("isdiv", [Id "n"; Id "k"]))
                   (Appl ("div", [Id "n"; Id "k"]))
                   (Appl ("-", [Int 0; Int 1]))
              };

    DefineFun {fname="bool-to-int"; args=["b", BoolSort]; range=IntSort;
               body=ite (Id "b") (Int 1) (Int 0)};
  ]

let selftest : t =
  [

  ]


module PP =
struct
  let rec to_string smt =
    Format.asprintf "%a@." pp smt

  and pp fmt smt =
    Print.list
      "@[<v>" "@;@;" "@]"
      pp_stmt fmt smt

  and pp_stmt fmt stmt =
    match stmt with
    | Assert e ->
      Format.fprintf fmt
        "@[<hov 2>(assert@ %a)@]"
        pp_expr e
    | Minimize e ->
      Format.fprintf fmt
        "@[<hov 2>(minimize@ %a)@]"
        pp_expr e
    | DeclareFun {fname;domain;range} ->
      Format.fprintf fmt
        "@[<hov 2>(declare-fun@ %s@ (%a)@ %a)@]"
        fname
        (Print.list
           "@[<hov 2>" "@ " "@]"
           pp_sort)
        domain
        pp_sort range
    | DefineFun {fname; args; range; body} ->
      Format.fprintf fmt
        "@[<hov 2>(define-fun@ %s@ %a@ %a@ %a)@]"
        fname pp_args args
        pp_sort range
        pp_expr body
    | DeclareDatatype (tname,cons) ->
      Format.fprintf fmt
        "@[<hov 2>(declare-datatype@ %s@ %a)@]"
        tname
        (Print.list
           "@[<v 1>(" "@ " ")@]"
           pp_cons)
        cons
    | CheckSat -> Format.fprintf fmt "(check-sat)"
    | GetModel -> Format.fprintf fmt "(get-model)"
    | GetValue vars ->
      Print.list
        "@[<hov 1>(get-value@ (" "@ " "))@]"
        Format.pp_print_string
        fmt vars
    | SetOption opt ->
      Format.fprintf fmt
        "@[<hov 2>(set-option@ %a)@]"
        pp_opt opt
    | SetLogic logic ->
      Format.fprintf fmt
        "@[<hov 2>(set-logic@ %a)@]"
        pp_logic logic
    | Push ->
      Format.fprintf fmt "(push)"
    | Pop ->
      Format.fprintf fmt "(pop)"
    | Reset ->
      Format.fprintf fmt "(reset)"

  and pp_expr fmt e =
    match e with
    | Bool b -> pp_bool fmt b
    | Int i -> Format.fprintf fmt "%i" i
    | Id id -> Format.fprintf fmt "%s" id
    | Appl (f, args) ->
      Format.fprintf fmt "@[<hv 2>(%s@ %a)@]"
        f
        (Print.list
           "" "@ " ""
           pp_expr)
        args
    | ForAll (bounds, expr) ->
      Format.fprintf fmt
        "@[<hov 2>(forall@ %a@ %a)@]"
        pp_bounds bounds
        pp_expr expr

  and pp_sort fmt sort =
    match sort with
    | IntSort -> Format.fprintf fmt "Int"
    | BoolSort -> Format.fprintf fmt "Bool"
    | Sort sort -> Format.fprintf fmt "%s" sort

  and pp_bounds fmt bounds =
    Print.list
      "@[<hov>(" "@ " ")@]"
      pp_arg fmt bounds

  and pp_args fmt args =
    Print.list
      "@[<hov>(" "@ " ")@]"
      pp_arg fmt args

  and pp_arg fmt (var,sort) =
    Format.fprintf fmt
      "@[<hov 2>(%s@ %a)@]"
      var pp_sort sort

  and pp_bool fmt b =
    match b with
    | true -> Format.fprintf fmt "true"
    | false -> Format.fprintf fmt "false"

  and pp_opt fmt opt =
    match opt with
    | ProduceModels b -> Format.fprintf fmt
                           "@[<hov 2>:produce-models@ %a@]"
                           pp_bool b
    | ProduceUnsatCores b -> Format.fprintf fmt
                               "@[<hov 2>:produce-unsat-cores@ %a@]"
                               pp_bool b

  and pp_cons fmt {consname;members} =
    Format.fprintf fmt "@[<h>(%s@ %a)@]"
      consname
      (Print.list
         "@[<v>" "@ " "@]"
         pp_member)
      members

  and pp_member fmt {accessor;sort} =
    Format.fprintf fmt "@[<h>(%s@ %a)@]"
      accessor
      pp_sort sort

  and pp_logic fmt logic =
    match logic with
    | ALL -> Format.fprintf fmt "ALL"
    | LIA -> Format.fprintf fmt "LIA"
    | QF_UFLIA -> Format.fprintf fmt "QF_UFLIA"
    | NIA -> Format.fprintf fmt "NIA"
    | QF_UFNIA -> Format.fprintf fmt "QF_UFNIA"

end

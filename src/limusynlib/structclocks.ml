open Common
open Commonclocks

type 'desc ck_expr_base =
  {
    ck_desc: 'desc;
    ck_id: Int.t;
    ck_scoped: Bool.t;
  }

and ck_e = ck_e_desc ck_expr_base

and ck_r = ck_r_desc ck_expr_base

and ck_c = ck_c_desc ck_expr_base

and ck_b = ck_b_desc ck_expr_base

and ck_uvar = ck_uni_desc ck_expr_base
and ck_var = ck_var_desc ck_expr_base
and ck_v = ck_v_desc ck_expr_base
and ck_arrow_expr = ck_arrow_desc ck_expr_base

and ck_e_desc =
  [
    | `CkArrow of fun_desc
    | `CkTuple of  ck_r list
    | `CkRefine of ck_ref
  ]

and ck_r_desc =
  [
    | `CkRefine of ck_ref
  ]

and ck_c_desc =
  [
    | `CkVar
    | `CkUnivar
    | `Pck
    | `On of on_desc
    | `Zeta of ck_v
  ]

and ck_b_desc =
  [
    | `CkVar
    | `CkUnivar
    | `Pck
    | `On of on_desc
  ]

and ck_uni_desc = [ `CkUnivar ]
and ck_var_desc = [ `CkVar ]
and ck_v_desc = [ `CkVar | `CkUnivar ]
and ck_arrow_desc = [ `CkArrow of fun_desc ]
and ck_out_desc = [ `CkRefine of ck_ref | `CkTuple of ck_r list ]

and refinement = RefHole

and fun_desc = (string * ck_r) list * ck_e

and on_desc =
  {
    ck_on: ck_b;
    case: String.t;
    cond: String.t;
    view: ck_r;
  }

and ck_ref = ck_c * refinement

type ck = ck_desc ck_expr_base

and ck_desc = [ ck_e_desc | ck_r_desc | ck_c_desc | ck_b_desc ]

type state =
  {
    next_ck_id: Int.t;
  }

module PP =
struct
  let rec decl ff d =
    match d with
    | DeclAny -> Format.fprintf ff "_"
    | Decl Pck {period;offset} -> Format.fprintf ff "(%i,%i)" period offset
    | Decl On {decl=d';decl_case;decl_cond} ->
      Format.fprintf ff "@[<hov 2>%a@ on@ %s:%s@]"
        decl d' decl_case decl_cond

  let rec ck_b ff (c: ck_b) =
    match c.ck_desc with
    | `Pck -> Format.fprintf ff "pck"
    | (`CkUnivar | `CkVar) as desc ->
      let underscore = if desc = `CkVar then "_" else "" in
      let apostrophe = if c.ck_scoped then "''" else "'" in
      Format.fprintf ff "%s%sck%i"
        apostrophe underscore c.ck_id
    | `On {ck_on;case;cond;view} ->
      Format.fprintf ff "@[<hov 2>%a@ on@ %s(%s,@ %a)@]"
        ck_b ck_on
        case
        cond
        ck_r view

  and ck_c ff (c: ck_c) =
    match c with
    | ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as c) -> ck_b ff c
    | ({ck_desc=`Zeta c';_}) ->
      Format.fprintf ff "z(%a, \\x.*%i)"
        ck_b (c' :> ck_b) c.ck_id

  and ck_r ff (c: ck_r) =
    match c.ck_desc with
    | `CkRefine (ck_ref,RefHole) ->
      Format.fprintf ff "@[{@ %a@ |@ *%i@ }@]"
        ck_c ck_ref c.ck_id

  let rec ck_arrow ff (c: ck_arrow_expr) =
    match c.ck_desc with
    | `CkArrow (ck_args,ck_out) ->
      Format.fprintf ff "@[%a@ ->@ %a@]"
        (Print.list "" "@ ->@ " ""
           (fun ff (id,t) -> Format.fprintf ff "%s:%a" id ck_r t))
        ck_args ck (ck_out :> ck)

  and ck_e ff (c: ck_e) =
    match c with
    | ({ck_desc=`CkArrow _;_} as c) ->
      ck_arrow ff c
    | ({ck_desc=`CkRefine _;_} as c) ->
      ck_r ff c
    | {ck_desc=`CkTuple ckl;_} -> Print.list "@[(" "@ *@ " ")@]" ck_r ff ckl


  and ck ff (c: ck) =
    match c with
    | ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as c) -> ck_b ff c
    | ({ck_desc=`Zeta _;_} as c) -> ck_c ff c
    | ({ck_desc=`CkRefine _;_} as c) ->
      ck_r ff c
    | ({ck_desc=`CkTuple _;_} as c) -> ck_e ff c
    | ({ck_desc=`CkArrow _;_} as c) ->
      ck_arrow ff c

end

let new_ck_id {next_ck_id} =
  next_ck_id,{next_ck_id=next_ck_id+1}

let new_ck : state -> ([> ] as 'a) -> bool -> state * 'a ck_expr_base =
  fun state (ck_desc : [> ]) ck_scoped : (state * ([> ] ck_expr_base)) ->
  let ck_id,state = new_ck_id state in
  state,
  {
    ck_desc;
    ck_scoped;
    ck_id;
  }

module Predef =
struct

  let pck,init_state =
    let state = {next_ck_id=0} in
    let state,pck = new_ck state `Pck false in
    let pck = (pck :> ck_b) in
    pck,state
end


let new_ckvar : state -> bool -> state * ck_var =
  fun state -> new_ck state `CkVar

let new_refvar : state -> bool -> state * ck_r =
  fun state scoped ->
  let state,ckvar = new_ckvar state scoped in
  let ckvar = (ckvar :> ck_c) in
  new_ck state (`CkRefine (ckvar,RefHole)) scoped

let new_refined_of : state -> ck_c -> bool -> state * ck_r =
  fun state ck_c ->
  new_ck state (`CkRefine (ck_c,RefHole))

let new_refpck : state -> bool -> state * ck_r =
  fun state scoped ->
  let pck = Predef.pck in
  let pck = (pck :> ck_c) in
  new_ck state (`CkRefine (pck,RefHole)) scoped

let new_on_of : state -> ck_b -> String.t -> String.t -> ck_r -> bool -> state * ck_b =
  fun state ck_on case cond view ->
  new_ck state (`On {ck_on;case;cond;view})

type gen_vars =
  {
    gen_ck_v: ck_v IntMap.t;
    gen_ck_b: ck_b IntMap.t;
    gen_ck_c: ck_c IntMap.t;
    gen_ck_r: ck_r IntMap.t;
    gen_ck_e: ck_e IntMap.t;
  }

let no_gen_vars =
  {
    gen_ck_v = IntMap.empty;
    gen_ck_b = IntMap.empty;
    gen_ck_c = IntMap.empty;
    gen_ck_r = IntMap.empty;
    gen_ck_e = IntMap.empty;
  }

let gen_ck_v_aux : state -> gen_vars -> ck_v -> (_,ck_v) result =
  fun state gen_vars ck ->
  let old_id = ck.ck_id in
  match IntMap.find_opt old_id gen_vars.gen_ck_v, ck.ck_desc with
  | Some ck,_ -> Ok (Some(ck,state,gen_vars))
  | None,`CkUnivar -> Ok None
  | None,`CkVar ->
    if ck.ck_scoped then
      Error (ck :> ck_v)
    else
      let state,tuni = new_ck state `CkUnivar false in
      let gen_ck_v = IntMap.add ck.ck_id tuni gen_vars.gen_ck_v in
      let gen_vars = {gen_vars with gen_ck_v} in
      Ok(Some(tuni,state,gen_vars))

let rec gen_ck_b_aux : state -> gen_vars -> ck_b -> (_,ck_v) result =
  fun state gen_vars ck ->
  let old_id = ck.ck_id in
  match IntMap.find_opt old_id gen_vars.gen_ck_b, ck with
  | Some ck,_ -> Ok (Some (ck,state,gen_vars))
  | None,{ck_desc=`Pck;_} -> Ok None
  | None, ({ck_desc=(`CkVar|`CkUnivar);_} as ck) ->
    begin
      match gen_ck_v_aux state gen_vars ck with
      | Ok None -> Ok None
      | Ok Some (ck,state,gen_vars) ->
        let ck = (ck :> ck_b) in
        let gen_ck_b = IntMap.add old_id ck gen_vars.gen_ck_b in
        let gen_vars = {gen_vars with gen_ck_b} in
        Ok(Some(ck,state,gen_vars))
      | Error x -> Error x
    end
  | None,{ck_desc=`On ({ck_on;view;_} as on_desc);_} ->
    let ck_on,changed,state,gen_vars =
      match gen_ck_b_aux state gen_vars ck_on with
      | Ok None -> Ok ck_on,false,state,gen_vars
      | Ok Some (ck_on,state,gen_vars) -> Ok ck_on,true,state,gen_vars
      | Error x -> Error x,false,state,gen_vars
    in
    let view,changed,state,gen_vars =
      match gen_ck_r_aux state gen_vars view with
      | Ok None -> Ok view,changed,state,gen_vars
      | Ok Some (view,state,gen_vars) -> Ok view,true,state,gen_vars
      | Error x -> Error x,false,state,gen_vars
    in
    begin
      match ck_on,view,changed with
      | Ok _,Ok _,false -> Ok None
      | Ok ck_on,Ok view,true ->
        let state,ck = new_ck state (`On {on_desc with ck_on;view}) false in
        let gen_ck_b = IntMap.add old_id ck gen_vars.gen_ck_b in
        let gen_vars = {gen_vars with gen_ck_b} in
        Ok (Some (ck,state,gen_vars))
      | Error x,_,_ -> Error x
      | _,Error x,_ -> Error x
    end


and gen_ck_b state ck = gen_ck_b_aux state no_gen_vars ck

and gen_ck_c_aux state (gen_vars: gen_vars) (ck: ck_c) =
  let old_id = ck.ck_id in
  match IntMap.find_opt old_id gen_vars.gen_ck_c, ck with
  | Some ck,_ -> Ok (Some (ck,state,gen_vars))
  | None,({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as ck) ->
    begin
      match gen_ck_b_aux state gen_vars ck with
      | Ok None -> Ok None
      | Ok Some (ck_b,state,gen_vars) ->
        let ck = (ck_b :> ck_c) in
        let gen_ck_c = IntMap.add old_id ck gen_vars.gen_ck_c in
        let gen_vars = {gen_vars with gen_ck_c} in
        Ok (Some (ck,state,gen_vars))
      | Error x -> Error x
    end
  | None,{ck_desc=`Zeta ck_zvar;_} ->
    begin
      match gen_ck_v_aux state gen_vars ck_zvar with
      | Ok None -> Ok None
      | Ok Some (({ck_desc=`CkVar|`CkUnivar;_} as ck_zvar),state,gen_vars) ->
        let state,ck = new_ck state (`Zeta ck_zvar) ck_zvar.ck_scoped in
        let ck = (ck :> ck_c) in
        let gen_ck_c = IntMap.add old_id ck gen_vars.gen_ck_c in
        let gen_vars = {gen_vars with gen_ck_c} in
        Ok(Some(ck,state,gen_vars))
      | Error x -> Error x
    end

and gen_ck_c state ck = gen_ck_c_aux state no_gen_vars ck

and gen_ck_r_aux state (gen_vars: gen_vars) ck =
  let old_id = ck.ck_id in
  match IntMap.find_opt old_id gen_vars.gen_ck_r, ck.ck_desc with
  | Some ck,_ -> Ok (Some(ck,state,gen_vars))
  | None,`CkRefine (ck_ref,RefHole) ->
    begin
      match gen_ck_c_aux state gen_vars ck_ref with
      | Ok None -> Ok None
      | Ok Some (ck_ref,state,gen_vars) ->
        let ck_ref = (ck_ref :> ck_c) in
        let state,ck = new_ck state (`CkRefine (ck_ref,RefHole)) ck.ck_scoped in
        let gen_ck_r = IntMap.add old_id ck gen_vars.gen_ck_r in
        let gen_vars = {gen_vars with gen_ck_r} in
        Ok (Some (ck,state,gen_vars))
      | Error x -> Error x
    end

let gen_ck_r state ck = gen_ck_r_aux state no_gen_vars ck

let rec gen_ck_arrow_aux state (gen_vars: gen_vars) (ck: ck_arrow_expr) :
  ((ck_arrow_expr * state * gen_vars) option, ck) result =
  let old_id = ck.ck_id in
  match IntMap.find_opt old_id gen_vars.gen_ck_e, ck with
  | Some ({ck_desc=`CkArrow _;_} as ck),_ ->
    Ok (Some(ck,state,gen_vars))
  | Some _,_ -> Error (ck :> ck)
  | None, {ck_desc=`CkArrow (ck_args,ck_out);_} ->
    let ck_args,changed,state,gen_vars =
      List.fold_left
        (fun (ck_args,changed,state,gen_vars) (var,ck) ->
           match ck_args with
           | Ok ck_args ->
             begin
               match gen_ck_r_aux state gen_vars ck with
               | Ok (Some(ck,state,gen_vars)) ->
                 Ok ((var,ck)::ck_args),true,state,gen_vars
               | Ok None ->
                 Ok ((var,ck)::ck_args),changed,state,gen_vars
               | Error x -> Error x,changed,state,gen_vars
             end
           | Error x -> Error x,changed,state,gen_vars)
        (Ok [],false,state,gen_vars) ck_args
    in
    begin
      match ck_args with
      | Ok ck_args ->
        let ck_args = List.rev ck_args in
        begin
          match gen_ck_e_aux state gen_vars ck_out,changed,ck_out,state,gen_vars with
          | Ok Some (ck_out,state,gen_vars),_,_,_,_
          | Ok None,true,ck_out,state,gen_vars
            ->
            let state,ck = new_ck state (`CkArrow (ck_args,ck_out)) false in
            let gen_ck_e = IntMap.add old_id ck gen_vars.gen_ck_e in
            let gen_vars = {gen_vars with gen_ck_e} in
            Ok (Some(ck,state,gen_vars))
          | Ok None,false,_,_,_ -> Ok None
          | Error x,_,_,_,_ -> Error (x :> ck)
        end
      | Error x -> Error (x :> ck)
    end

and gen_ck_arrow state ck = gen_ck_arrow_aux state no_gen_vars ck

and gen_ck_e_aux state (gen_vars: gen_vars) (ck: ck_e) =
  let old_id = ck.ck_id in
  match IntMap.find_opt old_id gen_vars.gen_ck_e, ck with
  | _,({ck_desc=`CkRefine _;_} as ck) ->
    ((gen_ck_r_aux state gen_vars ck) :> ((ck_e * state * gen_vars) option, ck) result)
  | _,({ck_desc=`CkArrow _;_} as ck) ->
    ((gen_ck_arrow_aux state gen_vars ck) :> ((ck_e * state * gen_vars) option, ck) result)
  | Some ({ck_desc=`CkTuple _;_} as ck),{ck_desc=`CkTuple _;_} ->
    Ok (Some(ck,state,gen_vars))
  | Some {ck_desc=`CkArrow _|`CkRefine _;_}, {ck_desc=`CkTuple _;_} ->
    Error (ck :> ck)
  | None,{ck_desc=`CkTuple ckl;_} ->
    let ckl,changed,state,gen_vars =
      List.fold_left
        (fun (ckl,changed,state,gen_vars) ck ->
           match ckl with
           | Ok ckl ->
             begin
               match gen_ck_r_aux state gen_vars ck with
               | Ok Some(ck,state,gen_vars) ->
                 Ok (ck::ckl),true,state,gen_vars
               | Ok None ->
                 Ok (ck::ckl),changed,state,gen_vars
               | Error x ->
                 Error x,changed,state,gen_vars
             end
           | Error x -> Error x,changed,state,gen_vars)
        (Ok [],false,state,gen_vars) ckl
    in
    begin
      match ckl,changed with
      | Ok ckl,true ->
        let state,ck = new_ck state (`CkTuple (List.rev ckl)) false in
        let gen_ck_e = IntMap.add old_id ck gen_vars.gen_ck_e in
        let gen_vars = {gen_vars with gen_ck_e} in
        Ok (Some (ck,state,gen_vars))
      | Ok _,false -> Ok None
      | Error x,_ -> Error (x :> ck)
    end

let gen_ck_e state ck = gen_ck_e_aux state no_gen_vars ck

type inst_vars = ck_var IntMap.t

let inst_ck_v : state -> inst_vars -> ck_v -> (state*ck_var*_) option =
  fun state inst_vars ck ->
  match ck with
  | {ck_desc=`CkVar;_} -> None
  | ({ck_desc=`CkUnivar;_} as ck) ->
    begin
      match IntMap.find_opt ck.ck_id inst_vars with
      | Some ck' -> Some(state,ck',inst_vars)
      | None ->
        let state,ck' = new_ckvar state true in
        let inst_vars = IntMap.add ck.ck_id ck' inst_vars in
        Some (state,ck',inst_vars)
    end

let rec inst_ck_b : state -> inst_vars -> ck_b -> (state*ck_b*_) option =
  fun state (inst_vars: inst_vars) (ck: ck_b) ->
  match ck with
  | ({ck_desc=`CkVar|`CkUnivar;_} as ck) ->
    (inst_ck_v state inst_vars ck :> (state*ck_b*_) option)
  | {ck_desc=`Pck;_} -> None
  | {ck_desc=`On ({ck_on;view;_} as on_desc);_} ->
    let ck_on,changed,state,inst_vars =
      match inst_ck_b state inst_vars ck_on with
      | Some (state,ck_on,inst_vars) -> ck_on,true,state,inst_vars
      | None -> ck,false,state,inst_vars
    in
    let view,changed,state,inst_vars =
      match inst_ck_r state inst_vars view with
      | Some (state,view,inst_vars) -> view,true,state,inst_vars
      | None -> view,changed,state,inst_vars
    in
    if changed then
      let state,ck = new_ck state (`On {on_desc with ck_on;view}) true in
      Some (state,ck,inst_vars)
    else
      None

and inst_ck_c : state -> inst_vars -> ck_c -> (state*ck_c*_) option =
  fun state inst_vars (ck: ck_c) ->
  match ck with
  | ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as ck) ->
    begin
      match inst_ck_b state inst_vars ck with
      | Some (state,ck,inst_vars) ->
        let ck = (ck :> ck_c) in
        Some (state,ck,inst_vars)
      | None -> None
    end
  | {ck_desc=`Zeta ck_zvar;_} ->
    begin
      match inst_ck_v state inst_vars ck_zvar with
      | Some (state,ck_zvar,inst_vars) ->
        let ck_zvar = (ck_zvar :> ck_v) in
        let state,ck = new_ck state (`Zeta ck_zvar) ck_zvar.ck_scoped in
        Some (state,ck,inst_vars)
      | None ->
        (* Maybe always produce a new type when instanciating ??? *)
        None
    end

and inst_ck_r : state -> inst_vars -> ck_r -> (state*ck_r*_) option =
  fun state inst_vars ck ->
  match ck with
  | {ck_desc=`CkRefine (ck_ref,RefHole);_} ->
    begin
      match inst_ck_c state inst_vars ck_ref with
      | Some (state,ck_ref,inst_vars) ->
        let ck_ref = (ck_ref :> ck_c) in
        let state,ck = new_ck state (`CkRefine (ck_ref,RefHole)) ck_ref.ck_scoped in
        Some (state,ck,inst_vars)
      | None -> None
    end

let rec inst_ck_e : state -> inst_vars -> ck_e -> (state*ck_e*_) option =
  fun state inst_vars (ck: ck_e) ->
  match ck with
  | ({ck_desc=`CkRefine _;_} as ck) ->
    (inst_ck_r state inst_vars ck :> (state*ck_e*_) option)
  | {ck_desc=`CkTuple ckl;_} ->
    let ckl,changed,scoped,state,inst_vars =
      List.fold_left
        (fun (ckl,changed,scoped,state,inst_vars) ck ->
           match inst_ck_r state inst_vars ck with
           | Some (state,ck,inst_vars) ->
             ck::ckl,true,scoped||ck.ck_scoped,state,inst_vars
           | None ->
             ck::ckl,changed,scoped||ck.ck_scoped,state,inst_vars)
        ([],false,false,state,inst_vars) ckl
    in
    if changed then
      let ckl = List.rev ckl in
      let state,ck = new_ck state (`CkTuple ckl) scoped in
      Some (state,ck,inst_vars)
    else
      None
  | {ck_desc=`CkArrow (ck_args,ck_out);_} ->
    let ck_args,changed,scoped,state,inst_vars =
      List.fold_left
        (fun (ck_args,changed,scoped,state,int_vars) (var,ck) ->
           match inst_ck_r state int_vars ck with
           | Some (state,ck,inst_vars) ->
             (var,ck)::ck_args,true,scoped||ck.ck_scoped,state,inst_vars
           | None ->
             (var,ck)::ck_args,changed,scoped||ck.ck_scoped,state,inst_vars)
        ([],false,false,state,inst_vars) ck_args
    in
    let ck_out,changed,scoped,state,inst_vars =
      match inst_ck_e state inst_vars ck_out with
      | Some (state,ck_out,inst_vars) ->
        ck_out,true,scoped||ck_out.ck_scoped,state,inst_vars
      | None ->
        ck_out,changed,scoped||ck_out.ck_scoped,state,inst_vars
    in
    if changed then
      let state,ck = new_ck state (`CkArrow (ck_args,ck_out)) scoped in
      Some (state,ck,inst_vars)
    else
      None

module Substitute =
struct
  type subs =
  {
    subs_ck_v: ck_v IntMap.t;
    subs_ck_b: ck_b IntMap.t;
    subs_ck_c: ck_c IntMap.t;
    subs_ck_r: ck_r IntMap.t;
    subs_ck_arrow : ck_arrow_expr IntMap.t;
    subs_ck_e: ck_e IntMap.t;
  }

  type subs_state = subs*state

  let no_subs : subs =
    {
      subs_ck_v = IntMap.empty;
      subs_ck_b = IntMap.empty;
      subs_ck_c = IntMap.empty;
      subs_ck_r = IntMap.empty;
      subs_ck_arrow = IntMap.empty;
      subs_ck_e = IntMap.empty;
    }

  let v_in_ck_v_aux : subs_state -> ck_v -> ck_v -> ck_v -> (subs_state*ck_v) option =
    fun ((subs,state) as subs_state) from_ck to_ck ck_v ->
    let result = IntMap.find_opt ck_v.ck_id subs.subs_ck_v in
    match result,from_ck,to_ck,ck_v with
    | Some ck_v,_,_,_ -> Some(subs_state,ck_v)
    | None,from_ck,to_ck,ck_v when from_ck=ck_v ->
      let subs_ck_v = IntMap.add ck_v.ck_id to_ck subs.subs_ck_v in
      let subs = {subs with subs_ck_v} in
      Some((subs,state),to_ck)
    | _ -> None

  let rec b_in_ck_b_aux : subs_state -> ck_b -> ck_b -> ck_b -> (subs_state*ck_b) option =
    fun ((subs,_) as subs_state) from_ck to_ck ck_b ->
    let result = IntMap.find_opt ck_b.ck_id subs.subs_ck_b in
    match result,from_ck,to_ck,ck_b with
    | Some(ck_b),_,_,_ -> Some(subs_state,ck_b)
    | None,
      ({ck_desc=`CkVar|`CkUnivar;_} as from_ck),
      ({ck_desc=`CkVar|`CkUnivar;_} as to_ck),
      ({ck_desc=`CkVar|`CkUnivar;_} as ck_v)
      ->
      begin
        match v_in_ck_v_aux subs_state from_ck to_ck ck_v with
        | Some((subs,state),ck_subs) ->
          let ck_subs = (ck_subs :> ck_b) in
          let subs_ck_b = IntMap.add ck_v.ck_id ck_subs subs.subs_ck_b in
          let subs = {subs with subs_ck_b} in
          Some((subs,state),ck_subs)
        | None -> None
      end
    | None,from_ck,to_ck,ck_b when from_ck=ck_b ->
      Some(subs_state,to_ck)
    | None,from_ck,to_ck,({ck_desc=`On ({ck_on;_} as on_desc);ck_scoped;ck_id}) ->
      (* It's not an equality, but we could have to push the substitutions deeper *)
      begin
        match b_in_ck_b_aux subs_state from_ck to_ck ck_on with
        | Some ((subs,state),ck_on) ->
          let state,ck_b = new_ck state (`On {on_desc with ck_on}) ck_scoped in
          let subs_ck_b = IntMap.add ck_id ck_b subs.subs_ck_b in
          let subs = {subs with subs_ck_b} in
          Some((subs,state),ck_b)
        | None -> None
      end
    | None,{ck_desc=`CkVar|`CkUnivar|`On _;_},{ck_desc=`CkVar|`CkUnivar|`Pck|`On _;_},{ck_desc=`Pck;_}
    | None,{ck_desc=`Pck|`CkVar|`On _;_},{ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_},{ck_desc=`CkUnivar;_}
    | None,{ck_desc=`Pck|`CkUnivar|`On _;_},{ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_}, {ck_desc=`CkVar;_}
    (* Nothing to substitute here *)
    | None,{ck_desc=`Pck;_},{ck_desc=`Pck;_},_
    (* There is only one Pck *)
    | None,{ck_desc=`CkVar;_},_,{ck_desc=`CkVar;_}
    | None,{ck_desc=`CkUnivar;_},_,{ck_desc=`CkUnivar;_}
    | None,{ck_desc=`Pck;_},_,{ck_desc=`Pck;_}
      (* If we land here, the equality check failed, ie from_ck=/=ck_b*)
      ->
      None

  let rec c_in_ck_c_aux : subs_state -> ck_c -> ck_c -> ck_c -> (subs_state*ck_c) option =
    fun ((subs,state) as subs_state) from_ck to_ck ck_c ->
    let result = IntMap.find_opt ck_c.ck_id subs.subs_ck_c in
    match result,from_ck,to_ck,ck_c with
    | Some ck_c,_,_,_ -> Some(subs_state,ck_c)
    | None,from_ck,to_ck,ck_c when from_ck=ck_c ->
      Some (subs_state,to_ck)
    | None,
      ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as from_ck),
      ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as to_ck),
      ({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as ck_b)
      ->
      begin
        match  b_in_ck_b_aux subs_state from_ck to_ck ck_b with
        | Some((subs,state),ck_subs) ->
          let ck_subs = (ck_subs :> ck_c) in
          let subs_ck_c = IntMap.add ck_c.ck_id ck_subs subs.subs_ck_c in
          let subs = {subs with subs_ck_c} in
          Some((subs,state),ck_subs)
        | None -> None
      end
    | None,
      ({ck_desc=`CkVar|`Pck|`CkUnivar;_} as from_ck),
      ({ck_desc=`CkVar|`Pck|`CkUnivar;_} as to_ck),
      ({ck_desc=`Zeta ck_zvar;ck_scoped;ck_id}) ->
      begin
        let ck_b = (ck_zvar :> ck_b) in
        match b_in_ck_b_aux subs_state from_ck to_ck ck_b with
        | Some ((subs,state),({ck_desc=`CkVar|`CkUnivar;_} as ck_zvar)) ->
          let ck_desc = `Zeta ck_zvar in
          let state,ck_c = new_ck state ck_desc ck_scoped in
          let subs_ck_c = IntMap.add ck_id ck_c subs.subs_ck_c in
          let subs = {subs with subs_ck_c} in
          Some ((subs,state),ck_c)
        | Some (subs_state,({ck_desc=`Pck;_} as ck_c)) ->
          Some (subs_state,ck_c)
        | Some (_,{ck_desc=`On _;_}) -> failwith "Internal error"
        | None -> None
      end
    | None,
      ({ck_desc=`CkVar|`CkUnivar;_} as from_ck),
      ({ck_desc=`On {ck_on;case;cond;_};_}),
      ({ck_desc=`Zeta ck_zvar;ck_scoped;ck_id} as _ck_c) ->
      (* Substitute a CkVar by a On (potentially) while inside a Zeta *)
      if from_ck = ck_zvar then
        let (state,tmp_var) : _ * ck_v = new_ck state `CkVar ck_on.ck_scoped in
        let (state,tmp_zeta) : _ * ck_c = new_ck state (`Zeta tmp_var) ck_on.ck_scoped in
        let subs_state = (subs,state) in
        match c_in_ck_c_aux subs_state (tmp_var :> ck_c) (ck_on :> ck_c) tmp_zeta with
        | Some ((subs,state),({ck_desc=`Pck|`On _;_} as ck_on)) ->
          let state,view = new_refpck state false in
          let on_desc = {ck_on;case;cond;view} in
          let state,ck_c = new_ck state (`On on_desc) ck_scoped in
          let subs_ck_c = IntMap.add ck_id ck_c subs.subs_ck_c in
          let subs = {subs with subs_ck_c} in
          Some ((subs,state),ck_c)
        | Some (_,{ck_desc=`Zeta _;_}) -> failwith "unimplemented"
        | Some (_,{ck_desc=`CkVar|`CkUnivar;_}) | None ->
          failwith "Internal error"
      else
        None
    | None,
      ({ck_desc=`CkVar|`CkUnivar;_} as from_ck),
      {ck_desc=`Zeta new_zvar;_}, {ck_desc=`Zeta ck_zvar;ck_scoped;ck_id}
      ->
      begin
        if from_ck = ck_zvar then
          let state,ck_c = new_ck state (`Zeta new_zvar) ck_scoped in
          let subs_ck_c = IntMap.add ck_id ck_c subs.subs_ck_c in
          let subs = {subs with subs_ck_c} in
          Some ((subs,state),ck_c)
        else
          None
      end
    | None,
      ({ck_desc=`Pck|`CkVar|`CkUnivar;_} as to_ck),
      ({ck_desc=`Zeta _;_} as from_ck),
      ({ck_desc=`On ({ck_on;_} as on_desc);ck_scoped;ck_id})
      ->
      begin
        match c_in_ck_c_aux subs_state to_ck from_ck (ck_on :> ck_c) with
        | Some ((subs,state),({ck_desc=`Pck|`CkVar|`CkUnivar|`On _;_} as ck_on)) ->
          let state,ck_c = new_ck state (`On {on_desc with ck_on}) ck_scoped in
          let subs_ck_c = IntMap.add ck_id ck_c subs.subs_ck_c in
          let subs = {subs with subs_ck_c} in
          Some ((subs,state),ck_c)
        | Some ((subs,state),{ck_desc=`Zeta ck_zvar;_}) ->
          (* We performed the substitution below and GASP! it's a Zeta
           * Construct a fresh On such that we have a fresh view *)
          let state,view = new_refpck state false in
          let ck_on = (ck_zvar :> ck_b) in
          let state,ck_c = new_ck state (`On {on_desc with ck_on; view}) ck_scoped in
          let subs_ck_c = IntMap.add ck_id ck_c subs.subs_ck_c in
          let subs = {subs with subs_ck_c} in
          Some ((subs,state),ck_c)
        | None -> None
      end
    | None,{ck_desc=`On _;_},{ck_desc=`Zeta _;_},{ck_desc=`On _;_}
      ->
      (* Not sure when we would encounter that *)
      failwith @@ Format.asprintf "error c_in_ck_c_aux: [%a] [%a] [%a]"
        PP.ck_c from_ck
        PP.ck_c to_ck
        PP.ck_c ck_c
    | None,{ck_desc=`Pck;_},_,{ck_desc=`Pck;_}
    | None,{ck_desc=`CkVar;_},_,{ck_desc=`CkVar;_}
    | None,{ck_desc=`CkUnivar;_},_,{ck_desc=`CkUnivar;_}
    | None,{ck_desc=`Zeta _;_},_,{ck_desc=`Zeta _;_}
    (* "Counter-examples" to appeas OCaml match-checker *)
    | None,{ck_desc=`CkVar|`CkUnivar|`On _|`Zeta _;_},_,{ck_desc=`Pck;_}
    | None,{ck_desc=`Pck|`CkUnivar|`On _|`Zeta _;_},_,{ck_desc=`CkVar;_}
    | None,{ck_desc=`Pck|`CkVar|`On _|`Zeta _;_},_,{ck_desc=`CkUnivar;_}
    | None,{ck_desc=`Zeta _;_},_,{ck_desc=`On _;_}
    | None,{ck_desc=`Pck|`On _;_},_,{ck_desc=`Zeta _;_}
      (* Impossible matches *)
      ->
      None

  let c_in_ck_r_aux : subs_state -> ck_c -> ck_c -> ck_r -> (subs_state*ck_r) option =
    fun ((subs,_) as subs_state) from_ck to_ck ck_r ->
    let result = IntMap.find_opt ck_r.ck_id subs.subs_ck_r in
    let {ck_desc=`CkRefine (ck_ref,RefHole);ck_scoped;ck_id} = ck_r in
    match result,c_in_ck_c_aux subs_state from_ck to_ck ck_ref with
    | Some ck_r,_ -> Some(subs_state,ck_r)
    | None,Some ((subs,state),ck_ref) ->
      let state,ck_r = new_ck state (`CkRefine (ck_ref,RefHole)) ck_scoped in
      let subs_ck_r = IntMap.add ck_id ck_r subs.subs_ck_r in
      let subs = {subs with subs_ck_r} in
      Some((subs,state),ck_r)
    | None,None -> None

  let rec r_in_ck_r_aux : subs_state -> ck_r -> ck_r -> ck_r -> (subs_state*ck_r) option =
    fun ((subs,state) as subs_state) from_ck to_ck ck_r ->
    if from_ck = ck_r then
      let subs_ck_r = IntMap.add ck_r.ck_id to_ck subs.subs_ck_r in
      let subs = {subs with subs_ck_r} in
      Some ((subs,state),to_ck)
    else
      let {ck_desc=`CkRefine (ck_ref,RefHole);ck_scoped;ck_id} = ck_r in
      match r_in_ck_c_aux subs_state from_ck to_ck ck_ref with
      | Some ((subs,state),ck_ref) ->
        let state,ck_r = new_ck state (`CkRefine (ck_ref,RefHole)) ck_scoped in
        let subs_ck_r = IntMap.add ck_id ck_r subs.subs_ck_r in
        let subs = {subs with subs_ck_r} in
        Some ((subs,state),ck_r)
      | None -> None


  and r_in_ck_c_aux : subs_state -> ck_r -> ck_r -> ck_c -> (subs_state*ck_c) option =
    fun subs_state to_ck from_ck ck_c ->
    match ck_c with
    | {ck_desc=`Pck|`CkVar|`CkUnivar|`Zeta _;_} -> None
    | ({ck_desc=`On ({view={ck_id=view_id;_} as view;_} as on_desc);ck_scoped;ck_id=on_id} as _ck_c) ->
      begin
        match r_in_ck_r_aux subs_state to_ck from_ck view with
        | Some ((subs,state),view) ->
          let state,ck_c = new_ck state (`On {on_desc with view}) ck_scoped in
          let subs_ck_r = IntMap.add view_id view subs.subs_ck_r in
          let subs_ck_c = IntMap.add on_id ck_c subs.subs_ck_c in
          let subs = {subs with subs_ck_r;subs_ck_c} in
          Some ((subs,state),ck_c)
        | None -> None
      end


  let rec c_in_ck_e_aux : subs_state -> ck_c -> ck_c -> ck_e -> (subs_state*ck_e) option =
    fun ((subs,_) as subs_state) from_ck to_ck ck_e ->
    let result = IntMap.find_opt ck_e.ck_id subs.subs_ck_e in
    match result,ck_e with
    | Some ck_e,_ -> Some(subs_state,ck_e)
    | None,({ck_desc=`CkRefine _;_} as ck_r) ->
      begin
        match c_in_ck_r_aux subs_state from_ck to_ck ck_r with
        | Some((subs,state),ck_subs) ->
          let ck_subs = (ck_subs :> ck_e) in
          let subs_ck_e = IntMap.add ck_r.ck_id ck_subs subs.subs_ck_e in
          let subs = {subs with subs_ck_e} in
          Some((subs,state),ck_subs)
        | None -> None
      end
    | None,{ck_desc=`CkTuple ckl;ck_scoped;ck_id} ->
      let (changed,(subs,state)),ckl =
        List.fold_left_map
          (fun (changed,subs_state) ck ->
             match c_in_ck_r_aux subs_state from_ck to_ck ck with
             | Some (subs_state,ck) -> (true,subs_state),ck
             | None -> (changed,subs_state),ck)
          (false,subs_state) ckl
      in
      if changed then
        let state,ck_e = new_ck state (`CkTuple ckl) ck_scoped in
        let subs_ck_e = IntMap.add ck_id ck_e subs.subs_ck_e in
        let subs = {subs with subs_ck_e} in
        Some((subs,state),ck_e)
      else
        None
    | None,{ck_desc=`CkArrow (ck_args,ck_out);ck_scoped;ck_id} ->
      let (changed,(subs,state)),ck_args =
        List.fold_left_map
          (fun (changed,subs_state) (var,ck) ->
             match c_in_ck_r_aux subs_state from_ck to_ck ck with
             | Some (subs_state,ck) -> (true,subs_state),(var,ck)
             | None -> (changed,subs_state),(var,ck))
          (false,subs_state) ck_args
      in
      let changed,(subs,state),ck_out =
        match c_in_ck_e_aux (subs,state) from_ck to_ck ck_out with
        | Some (subs_state,ck_out) -> true,subs_state,ck_out
        | None -> changed,subs_state,ck_out
      in
      if changed then
        let state,ck_e = new_ck state (`CkArrow (ck_args,ck_out)) ck_scoped in
        let subs_ck_e = IntMap.add ck_id ck_e subs.subs_ck_e in
        let subs = {subs with subs_ck_e} in
        Some ((subs,state),ck_e)
      else
        None

  let rec r_in_ck_arrow_aux : subs_state -> ck_r -> ck_r -> ck_arrow_expr -> (subs_state*ck_arrow_expr) option =
    fun ((subs,_) as subs_state) from_ck to_ck ck_arrow ->
    let result = IntMap.find_opt ck_arrow.ck_id subs.subs_ck_arrow in
    match result,ck_arrow with
    | Some ck_arrow,_ -> Some (subs_state,ck_arrow)
    | None,{ck_desc=`CkArrow (ck_args,ck_out);ck_scoped;ck_id} ->
      let (changed,(subs,state)),ck_args =
        List.fold_left_map
          (fun (changed,subs_state) (var,ck) ->
             match r_in_ck_r_aux subs_state from_ck to_ck ck with
             | Some (subs_state,ck) -> (true,subs_state),(var,ck)
             | None -> (changed,subs_state),(var,ck))
          (false,subs_state) ck_args
      in
      let changed,(subs,state),ck_out =
        match r_in_ck_e_aux (subs,state) from_ck to_ck ck_out with
        | Some (subs_state,ck_out) -> true,subs_state,ck_out
        | None -> changed,subs_state,ck_out
      in
      if changed then
        let state,ck_e = new_ck state (`CkArrow (ck_args,ck_out)) ck_scoped in
        let subs_ck_e = IntMap.add ck_id ck_e subs.subs_ck_e in
        let subs = {subs with subs_ck_e} in
        Some ((subs,state),ck_e)
      else
        None

  and r_in_ck_e_aux : subs_state -> ck_r -> ck_r -> ck_e -> (subs_state*ck_e) option =
    fun ((subs,_) as subs_state) from_ck to_ck ck_e ->
    let result = IntMap.find_opt ck_e.ck_id subs.subs_ck_e in
    match result,ck_e with
    | Some ck_e,_ -> Some (subs_state,ck_e)
    | None, ({ck_desc=`CkRefine _;_} as ck_r) ->
      begin
        match r_in_ck_r_aux subs_state from_ck to_ck ck_r with
        | Some((subs,state),ck_subs) ->
          let ck_subs = (ck_subs :> ck_e) in
          let subs_ck_e = IntMap.add ck_r.ck_id ck_subs subs.subs_ck_e in
          let subs = {subs with subs_ck_e} in
          Some((subs,state),ck_subs)
        | None -> None
      end
    | None,{ck_desc=`CkTuple ckl;ck_scoped;ck_id} ->
      let (changed,(subs,state)),ckl =
        List.fold_left_map
          (fun (changed,subs_state) ck ->
             match r_in_ck_r_aux subs_state from_ck to_ck ck with
             | Some (subs_state,ck) -> (true,subs_state),ck
             | None -> (changed,subs_state),ck)
          (false,subs_state) ckl
      in
      if changed then
        let state,ck_e = new_ck state (`CkTuple ckl) ck_scoped in
        let subs_ck_e = IntMap.add ck_id ck_e subs.subs_ck_e in
        let subs = {subs with subs_ck_e} in
        Some((subs,state),ck_e)
      else
        None
    | None,{ck_desc=`CkArrow (ck_args,ck_out);ck_scoped;ck_id} ->
      let (changed,(subs,state)),ck_args =
        List.fold_left_map
          (fun (changed,subs_state) (var,ck) ->
             match r_in_ck_r_aux subs_state from_ck to_ck ck with
             | Some (subs_state,ck) -> (true,subs_state),(var,ck)
             | None -> (changed,subs_state),(var,ck))
          (false,subs_state) ck_args
      in
      let changed,(subs,state),ck_out =
        match r_in_ck_e_aux (subs,state) from_ck to_ck ck_out with
        | Some (subs_state,ck_out) -> true,subs_state,ck_out
        | None -> changed,subs_state,ck_out
      in
      if changed then
        let state,ck_e = new_ck state (`CkArrow (ck_args,ck_out)) ck_scoped in
        let subs_ck_e = IntMap.add ck_id ck_e subs.subs_ck_e in
        let subs = {subs with subs_ck_e} in
        Some ((subs,state),ck_e)
      else
        None
end

module type SMap =
sig
  type ock_v
  type ock_b
  type ock_c
  type ock_r
  type ock_e

  type ack_v
  type ack_b
  type ack_c
  type ack_r
  type ack_e

  type maps =
    {
      mck_v: ock_v IntMap.t;
      mck_b: ock_b IntMap.t;
      mck_c: ock_c IntMap.t;
      mck_r: ock_r IntMap.t;
      mck_e: ock_e IntMap.t;
    }

  type map_state = state * maps

  val fck_v: map_state -> ck_v -> ack_v -> (map_state*ock_v) option
  val fck_b: map_state -> ck_b -> ack_b -> (map_state*ock_b) option
  val fck_c: map_state -> ck_c -> ack_c -> (map_state*ock_c) option
  val fck_r: map_state -> ck_r -> ack_r -> (map_state*ock_r) option
  val fck_e: map_state -> ck_e -> ack_e -> (map_state*ock_e) option
end

module Map (Mapper: SMap) =
struct
  type ock_v = Mapper.ock_v
  type ock_b = Mapper.ock_b
  type ock_c = Mapper.ock_c
  type ock_r = Mapper.ock_r
  type ock_e = Mapper.ock_e

  type ack_v = Mapper.ack_v
  type ack_b = Mapper.ack_b
  type ack_c = Mapper.ack_c
  type ack_r = Mapper.ack_r
  type ack_e = Mapper.ack_e

  type maps = Mapper.maps
  type map_state = Mapper.map_state

  let no_maps: maps =
    {
      mck_v = IntMap.empty;
      mck_b = IntMap.empty;
      mck_c = IntMap.empty;
      mck_r = IntMap.empty;
      mck_e = IntMap.empty;
    }

  let v_in_ck_v_aux : map_state -> ck_v -> ack_v -> (map_state*ock_v) option =
    fun ((_,maps) as map_state) ck_v arg ->
    let result = IntMap.find_opt ck_v.ck_id maps.mck_v in
    match result,Mapper.fck_v map_state ck_v arg with
    | Some ock_v,_ -> Some(map_state,ock_v)
    | None, Some((state,maps),ock_v) ->
      let mck_v = IntMap.add ck_v.ck_id ock_v maps.mck_v  in
      let maps = {maps with mck_v} in
      Some ((state,maps),ock_v)
    | None,None -> None

  let c_in_ck_c_aux  : map_state -> ck_c -> ack_c -> (map_state*ock_c) option =
    fun ((_,maps) as map_state) ck_c arg ->
    let result = IntMap.find_opt ck_c.ck_id maps.mck_c in
    match result,Mapper.fck_c map_state ck_c arg with
    | Some ock_c,_ -> Some (map_state,ock_c)
    | None, _ -> failwith ""
end

let ck_r_expr_of_decl : state -> decl -> bool -> state * ck_r =
  let rec aux : state -> decl -> bool -> (String.t*String.t) list -> state * ck_r =
    fun state decl scoped acc ->
      match decl with
      | DeclAny ->
        new_refvar state scoped
      | Decl Pck {period;offset} ->
        let ty = Predef.pck in
        let state,ck_ref =
          List.fold_left
            (fun (state,ck_on) (case,cond) ->
               let state,view = new_refpck state scoped in
               new_ck state (`On {ck_on;case;cond;view}) scoped)
            (state,ty) acc
        in
        let ck_ref = (ck_ref :> ck_c) in
        let state,ty = new_ck state (`CkRefine (ck_ref,RefHole)) scoped in
        if period < 1 || offset < 0 then
          failwith "Internal error";
        state,ty
      | Decl On {decl;decl_case;decl_cond} ->
        aux state decl scoped ((decl_case,decl_cond)::acc)
  in
  fun state decl scoped ->
    aux state decl scoped []

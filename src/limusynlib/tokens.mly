%token<Int.t> INT
%token<Char.t> CHAR
%token<String.t> IDENT

%token TINT
%token TCHAR
%token TFLOAT

%token PCKUP
%token FBY
%token WHEN MERGE

%token COL COMMA SCOL
%token EQ
%token PIPE
%token LPAR RPAR
%token RBRACK LBRACK

%token ON VAR END RATE

%token NODE RETURNS
%token LET TEL
%token DUE

%token AUTOMATON THEN ARROW
%token UNLESS UNTIL

%token EOF

%%

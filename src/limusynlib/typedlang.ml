open Common

type prog = decl list

and decl =
  {
    decl_desc: decl_desc;
    decl_loc: Location.t;
  }

and decl_desc =
  | Node of node_desc
  | Enum of String.t * StringSet.t * Types.ty_enum

and node_desc =
  {
    node_name: String.t;
    node_inputs: vdecl list;
    node_outputs: vdecl list;
    node_locals: vdecl list;
    node_eqs: eq list;
    node_ty: Types.ty_expr;
  }

and eq =
  {
    eq_lhs: String.t list;
    eq_rhs: expr;
    eq_loc: Location.t;
  }

and 'desc expr_base =
  {
    expr_desc: 'desc;
    expr_loc: Location.t;
    expr_ty: Types.ty_expr;
  }

and expr = expr_desc expr_base

and atom_expr = atom_expr_desc expr_base

and ident_expr = ident_expr_desc expr_base

and const_expr = const_expr_desc expr_base

and expr_desc =
  [
    | `ExprConst of const
    | `ExprIdent of String.t
    | `ExprApply of ident_expr * ident_expr List.t
    | `ExprPckUp of rate_op_desc
    | `ExprFby of const_expr*rate_op_desc
    | `ExprWhen of when_desc
    | `ExprMerge of merge_desc
    | `ExprITE of ite_desc
  ]

and atom_expr_desc =
  [
    | `ExprConst of const
    | `ExprIdent of String.t
  ]

and ident_expr_desc =
  [
    | `ExprIdent of String.t
  ]

and const_expr_desc =
  [
    | `ExprConst of const
  ]

and const =
  [
    | `ConstInt of Int.t
    | `ConstChar of Char.t
    | `ConstFloat of Float.t
    | `ConstEnum of enum_value
  ]

and enum_value = {variant: String.t; enum: String.t}

and vdecl =
  {
    vdecl_name: String.t;
    vdecl_ty_decl: Types.decl;
    vdecl_ck_decl: Commonclocks.decl;
    vdecl_dl: Int.t Option.t;
    vdecl_ty: Types.ty_expr;
  }

and rate_op_desc =
  {
    rate_expr: ident_expr;
    rate_factor: Int.t;
    rate_ty: Types.ty_fn
  }

and when_desc =
  {
    when_expr: ident_expr;
    case: String.t;
    cond_expr: ident_expr;
    when_ty: Types.ty_fn
  }

and merge_desc =
  {
    merge_var: ident_expr;
    merge_exprs: (String.t*ident_expr) list;
    merge_ty: Types.ty_fn
  }

and ite_desc =
  {
    if_expr: ident_expr;
    then_expr: ident_expr;
    else_expr: ident_expr;
    ite_ty: Types.ty_fn;
  }

module PP =
struct

  let rec prog =
    fun ff p ->
      Print.list "@[<v>" "@ @ " "@ @]" decl ff p

  and decl =
    fun ff (d:decl) ->
      match d.decl_desc with
      | Node ndesc ->
        node ff ndesc
      | Enum (ename,evariants,etype) ->
        enum ff ename evariants etype

  and node =
    fun ff ({node_name;node_inputs;node_outputs;node_locals;node_eqs;node_ty}: node_desc) ->
      Format.fprintf ff "@[<v>";
      Format.fprintf ff "@[<hov 2>node@ %s@ :@ %a " node_name Types.PP.ty node_ty;
      Print.list "@[<hov 2>(" ";@ " ")@]" vdecl ff node_inputs;
      Format.fprintf ff "@ returns@ ";
      Print.list "@[<hov 2>(" ";@ " ")@]" vdecl ff node_outputs;
      Format.fprintf ff "@]@ ";
      Print.list "var @[<hov>" "@ " "@]@ " (Print.post "%a;" vdecl) ff node_locals;
      Print.list "@[<v 2>let@ " "@ " "@]@ tel" eq ff node_eqs;
      Format.fprintf ff "@]"

  and enum =
    fun ff ename evariants etype ->
      Format.fprintf ff "@[<v 2>@[<h>type@ %s@ (%a)@ =@]%a@]"
        ename
        Types.PP.ty (etype :> Types.ty_expr)
        (Print.seq "@ |" "@ |" "" Format.pp_print_string)
        (StringSet.to_seq evariants)

  and eq =
    fun ff {eq_lhs;eq_rhs;_} ->
      Format.fprintf ff "@[<hov 2>%a@ =@ %a;@]"
        (Print.list "@[<hov>" ",@," "@]" Format.pp_print_string) eq_lhs
        expr eq_rhs

  and expr =
    let aux =
      fun ff (e:expr) ->
        match e.expr_desc with
        | `ExprConst `ConstInt i -> Format.fprintf ff "%i" i
        | `ExprConst `ConstChar c -> Format.fprintf ff "%c" c
        | `ExprConst `ConstFloat f -> Format.fprintf ff "%f" f
        | `ExprConst `ConstEnum {variant;_} -> Format.fprintf ff "%s" variant
        | `ExprIdent id -> Format.fprintf ff "%s" id
        | `ExprApply (f,args) ->
          Format.fprintf ff "%a(%a)(%a)"
            expr (f :> expr)
            Types.PP.ty (f.expr_ty :> Types.ty_expr)
            (Print.list
               "@[<hov 2>" ",@ " "@]"
               expr)
            (args :> expr List.t)
        | `ExprPckUp {rate_expr;rate_factor;rate_ty} ->
          Format.fprintf ff "%a *^(%a) %i"
            expr (rate_expr :> expr)
            Types.PP.ty (rate_ty :> Types.ty_expr)
            rate_factor
        | `ExprFby (cst,{rate_expr;rate_ty;rate_factor=_}) ->
          Format.fprintf ff "%a fby(%a) %a"
            expr (cst :> expr)
            Types.PP.ty (rate_ty :> Types.ty_expr)
            expr (rate_expr :> expr)
        | `ExprWhen {when_expr;case;cond_expr;when_ty} ->
          Format.fprintf ff "%a when(%a) %s->%a"
            expr (when_expr :> expr)
            Types.PP.ty (when_ty :> Types.ty_expr)
            case
            expr (cond_expr :> expr)
        | `ExprMerge {merge_var;merge_exprs;merge_ty} ->
          Format.fprintf ff "merge(%a)(%a, %a)"
            Types.PP.ty (merge_ty :> Types.ty_expr)
            expr (merge_var :> expr)
            (Print.list
               "@[<hov 2>" "@ " "@]"
               (fun ff (case,e) ->
                  Format.fprintf ff "%s->%a"
                    case expr (e :> expr)))
            merge_exprs
        | `ExprITE {if_expr;then_expr;else_expr;ite_ty} ->
          Format.fprintf ff "if(%a) %a then %a else %a"
            Types.PP.ty (ite_ty :> Types.ty_expr)
            expr (if_expr :> expr)
            expr (then_expr :> expr)
            expr (else_expr :> expr)
    in
    fun ff e ->
      Format.fprintf ff "@[<hov>(%a:@ %a)@]"
        aux e Types.PP.ty e.expr_ty


  and vdecl =
    fun ff v ->
      Format.fprintf ff "@[<hov>%s: %a %a %a@]"
        v.vdecl_name
        Types.PP.ty v.vdecl_ty
        Structclocks.PP.decl v.vdecl_ck_decl
        dl v.vdecl_dl

  and dl =
    fun ff d ->
      Print.option
        (fun ff i -> Format.fprintf ff "due %i" i)
        ""
        ff d
end

module Make =
struct
  type enve =
    | TypedVar of Types.ty_expr
    | Enum of StringSet.t * Types.ty_enum
    | EnumVariant of String.t * Types.ty_enum

  type env = enve Env.String.t

  type state =
    {
      typer_state: Types.state;
      main_node: String.t;
    }

  type ty_constraint =
    | Eq of Types.ty_expr * Types.ty_expr

  let init_env =
    Env.String.empty

  let new_type,new_tyfn,new_tyenum,new_named_type,generalize,
      ty_expr_of_decl,
      new_tyvar,
      new_pck_up_ty,new_fby_ty,new_when_ty,new_merge_ty,new_ite_ty,
      arrow_eq,
      init_typer_state
    =
    let typer_state = Types.Predef.init_state in
    let with_ty_state_impure_3: type a b c. (Types.state -> a -> b -> c -> Types.state) -> state -> a -> b -> c -> state =
      fun fn state x y z ->
        let typer_state = state.typer_state in
        let typer_state = fn typer_state x y z in
        {state with typer_state}
    in
    let with_ty_state_3: type a b c d. (Types.state -> a -> b -> c -> (d * Types.state)) -> state -> a -> b -> c -> (state*d) =
      fun fn state x y z ->
        let typer_state = state.typer_state in
        let out,typer_state = fn typer_state x y z in
        {state with typer_state},out
    in
    let with_ty_state_2: type a b c. (Types.state -> a -> b -> (c * Types.state)) -> state -> a -> b -> (state*c) =
      fun fn state x y ->
        let typer_state = state.typer_state in
        let out,typer_state = fn typer_state x y in
        {state with typer_state},out
    in
    let with_ty_state_1: type a b. (Types.state -> a -> (b * Types.state)) -> state -> a -> (state*b) =
      fun fn state x ->
        let typer_state = state.typer_state in
        let out,typer_state = fn typer_state x in
        {state with typer_state},out
    in
    let with_ty_state_0: type a. (Types.state -> (a * Types.state)) -> state -> (state*a) =
      fun fn state ->
        let typer_state = state.typer_state in
        let out,typer_state = fn typer_state in
        {state with typer_state},out
    in
    let with_ty_state_res_1: type a b c. (Types.state -> a -> (b * Types.state,c) result) -> state -> a -> (state*b,c) result =
      fun fn state x ->
        let typer_state = state.typer_state in
        match fn typer_state x with
        | Ok (out,typer_state) ->
          Ok ({state with typer_state},out)
        | Error err -> Error err
    in

    let pck_up: Types.state -> (Types.ty_fn * Types.state) =
      fun typer_state ->
        let tyvar,typer_state = Types.new_tyvar typer_state true in
        let ty_args = ["@pck_up_e",tyvar] in
        let ty_out = tyvar in
        Types.new_tyfn typer_state ty_args ty_out true
    in

    let fby_ty: Types.state -> (Types.ty_fn * Types.state) =
      fun typer_state ->
        let tyvar,typer_state = Types.new_tyvar typer_state true in
        let ty_args = ["@fyb_cst",tyvar; "@fby_e",tyvar] in
        let ty_out = tyvar in
        Types.new_tyfn typer_state ty_args ty_out true
    in

    let when_ty : Types.state -> Types.ty_enum -> (Types.ty_fn * Types.state) =
      fun typer_state enum_type ->
        let tyvar_e,typer_state = Types.new_tyvar typer_state true in
        let enum_type = (enum_type :> Types.ty_expr) in
        let ty_args = ["@when_e",tyvar_e;"when_c",enum_type] in
        let ty_out = tyvar_e in
        Types.new_tyfn typer_state ty_args ty_out true
    in

    let merge_ty =
      fun typer_state cases ->
        let tyvar_var,typer_state = Types.new_tyvar typer_state true in
        let tyvar_exprs,typer_state = Types.new_tyvar typer_state true in
        let ty_args =
          ("@merge_c",tyvar_var)::
          (List.map
             (fun case -> ("@merge_"^case,tyvar_exprs))
             cases)
        in
        let ty_out = tyvar_exprs in
        Types.new_tyfn typer_state ty_args ty_out true
    in

    let ite_ty =
      fun typer_state ->
        let tyvar,typer_state = Types.new_tyvar typer_state true in
        let _,ty_bool = StringMap.find "bool" typer_state.named_types in
        let ty_args = ["@ite_if", ty_bool;
                       "@ite_then", tyvar;
                       "@ite_else", tyvar]
        in
        let ty_out = tyvar in
        Types.new_tyfn typer_state ty_args ty_out true
    in

    let arrow_eq state ty_args ty_out =
      let ty_args = List.map (fun ty -> ("",ty)) ty_args in
      Types.new_type state (`TyArrow {Types.ty_args;ty_out}) true
    in
    with_ty_state_2 Types.new_type,
    with_ty_state_3 Types.new_tyfn,
    with_ty_state_3 Types.new_tyenum,
    with_ty_state_impure_3 Types.new_named_type,
    with_ty_state_res_1 Types.generalize,
    with_ty_state_2 Types.ty_expr_of_decl,
    with_ty_state_1 (fun state -> Types.new_type state `TyVar),
    with_ty_state_0 pck_up,
    with_ty_state_0 fby_ty,
    with_ty_state_1 when_ty,
    with_ty_state_1 merge_ty,
    with_ty_state_0 ite_ty,
    with_ty_state_2 arrow_eq,
    typer_state

  let rec of_anf : String.t -> Anflang.prog -> prog =
    fun main_node anf_prog ->
    let state = init_state main_node in
    Env.String.st_map decl_of_anf init_env state anf_prog

  and decl_of_anf : env -> state -> Anflang.decl -> _ =
    fun env state (decl: Anflang.decl) ->
    let decl_loc = decl.decl_loc in
    match decl.decl_desc with
    | Node ndesc ->
      let state,ndesc = node_of_anf env state decl_loc ndesc in
      let kvs = [ndesc.node_name, TypedVar ndesc.node_ty] in
      kvs,state,{decl_desc=Node ndesc; decl_loc}
    | EnumDecl (ename,evariants) ->
      let state,etype = new_tyenum state ename evariants false in
      let state = new_named_type state ename Types.Enum (etype :> Types.ty_expr) in
      let kvs =
        StringSet.to_seq evariants
        |> Seq.map
          (fun variant ->
             (variant,(EnumVariant (ename,etype))))
        |> List.of_seq
      in
      let kvs = (ename, Enum (evariants,etype))::kvs in
      kvs,state,{decl_desc=Enum (ename,evariants,etype); decl_loc}

  and node_of_anf : env -> state -> Location.t -> Anflang.node_desc -> state * node_desc =
    fun env state _loc ndesc ->
    (* let _is_main = ndesc.node_name = state.main_node in *)
    let state,node_inputs = vdecls_of_anf state false ndesc.node_inputs in
    let state,node_outputs = vdecls_of_anf state true ndesc.node_outputs in
    let state,node_locals = vdecls_of_anf state true ndesc.node_locals in
    let vdecl_seq =
      List.to_seq node_inputs |>
      Seq.append (List.to_seq node_outputs) |>
      Seq.append (List.to_seq node_locals)
    in
    let internal_env =
      Seq.fold_left
        (fun acc (v:vdecl) -> Env.String.add v.vdecl_name (TypedVar v.vdecl_ty) acc)
        env vdecl_seq
    in
    let state,node_eqs = eqs_of_anf internal_env state ndesc.node_eqs in
    let _,node_ty = node_type_of_vars state node_inputs node_outputs in
    let ndesc' =
      {
        node_name = ndesc.node_name;
        node_inputs;
        node_locals;
        node_outputs;
        node_eqs;
        node_ty = node_ty;
      }
    in
    Print.vdebug Print.DebugTyped "%a@." PP.node ndesc';
    let state,constraints = eqs_constraints internal_env state node_eqs in
    let state,substitutions = solve_constraints state constraints IntMap.empty in
    let state,node_inputs = vdecls_ty_subs state substitutions node_inputs in
    let state,node_outputs = vdecls_ty_subs state substitutions node_outputs in
    let state,node_locals = vdecls_ty_subs state substitutions node_locals in
    let state,node_eqs = eqs_ty_subs state substitutions node_eqs in
    let state,node_ty = node_type_of_vars state node_inputs node_outputs in
    match generalize state node_ty with
    | Ok (state,node_ty) ->
      state,
      {
        node_name = ndesc.node_name;
        node_inputs;
        node_locals;
        node_outputs;
        node_eqs;
        node_ty;
      }
    | Error _ ->
      failwith (Format.sprintf "Could not generalize node %s" ndesc.node_name)

  and node_type_of_vars state inputs outputs =
    let ty_args =
      List.map
        (fun (v:vdecl) ->
           (v.vdecl_name,v.vdecl_ty))
        inputs
    in
    let state,ty_out = type_of_vdecls state outputs in
    new_type state (`TyArrow {Types.ty_args;ty_out}) false

  and vdecls_of_anf state scoped vdecls =
    let state,vdecls =
      List.fold_left_map
        (vdecl_of_anf scoped)
        state vdecls
    in
    state,vdecls

  and vdecl_of_anf scoped state v : state*vdecl =
    let {vdecl_name;vdecl_ty_decl;vdecl_ck_decl;vdecl_dl} : Anflang.vdecl = v in
    let state,vdecl_ty = ty_expr_of_decl state vdecl_ty_decl scoped in
    state,
    {
      vdecl_name;
      vdecl_ty_decl;
      vdecl_ck_decl;
      vdecl_dl;
      vdecl_ty;
    }

  and eqs_of_anf : env -> state -> Anflang.eq list -> state * eq list =
    fun env state eqs ->
    let state,eqs =
      List.fold_left_map
        (eq_of_anf env)
        state eqs
    in
    state,List.rev eqs

  and eq_of_anf : env -> state -> Anflang.eq -> state * eq =
    fun env state ({eq_lhs;eq_rhs;eq_loc}: Anflang.eq) ->
    let state,eq_rhs = expr_of_anf env state eq_rhs in
    let eq: eq = {eq_lhs;eq_rhs;eq_loc} in
    state,eq

  and expr_of_anf: env -> state -> Anflang.expr -> state * expr =
    fun env state expr ->
    let expr_loc = expr.expr_loc in
    match expr with
    | ({expr_desc=`ExprIdent _;_} as expr) ->
      let state,expr = ident_expr_of_anf env state expr in
      state,(expr :> expr)
    | {expr_desc=`ExprConst const;_} ->
      let const = const_of_anf const in
      let expr_ty = type_of_const env const in
      state,{expr_desc=`ExprConst const;expr_loc;expr_ty}
    | {expr_desc=`ExprApply (f,args);_} ->
      let state,f = ident_expr_of_anf env state f in
      let state,args = List.fold_left_map (ident_expr_of_anf env) state args in
      let state,expr_ty = new_tyvar state true  in
      state,{expr_desc=`ExprApply (f,args); expr_loc; expr_ty}
    | {expr_desc=(`ExprPckUp {rate_expr;rate_factor});_} ->
      let state,rate_expr = ident_expr_of_anf env state rate_expr in
      let state,rate_ty = new_pck_up_ty state in
      let state,expr_ty = new_tyvar state true in
      state,{expr_desc=`ExprPckUp {rate_expr;rate_factor;rate_ty};expr_loc;expr_ty}
    | {expr_desc=(`ExprFby (cst,{rate_expr;rate_factor}));_} ->
      let state,rate_expr = ident_expr_of_anf env state rate_expr in
      let cst = const_expr_of_anf env cst in
      let state,rate_ty = new_fby_ty state in
      let state,expr_ty = new_tyvar state true in
      state,{expr_desc=`ExprFby (cst,{rate_expr;rate_factor;rate_ty});expr_loc;expr_ty}
    | {expr_desc=(`ExprWhen {when_expr;case;cond_expr});_} ->
      let state,when_expr = ident_expr_of_anf env state when_expr in
      let state,cond_expr = ident_expr_of_anf env state cond_expr in
      let enum_ty = type_of_case env case in
      let state,when_ty = new_when_ty state enum_ty in
      let state,expr_ty = new_tyvar state true in
      state,{expr_desc=`ExprWhen {when_expr;case;cond_expr;when_ty};expr_loc;expr_ty}
    | {expr_desc=`ExprMerge {merge_var;merge_exprs};_} ->
      let state,merge_var = ident_expr_of_anf env state merge_var in
      let state,merge_exprs =
        List.fold_left_map
          (fun state (case,e) ->
             let state,e = ident_expr_of_anf env state e in
             state,(case,e))
          state merge_exprs
      in
      let cases = List.map fst merge_exprs in
      let state,merge_ty = new_merge_ty state cases in
      let state,expr_ty = new_tyvar state true in
      state,{expr_desc=`ExprMerge {merge_var;merge_exprs;merge_ty};expr_loc;expr_ty}
    | {expr_desc=`ExprITE {if_expr;then_expr;else_expr};_} ->
      let state,if_expr = ident_expr_of_anf env state if_expr in
      let state,then_expr = ident_expr_of_anf env state then_expr in
      let state,else_expr = ident_expr_of_anf env state else_expr in
      let state,ite_ty = new_ite_ty state in
      let state,expr_ty = new_tyvar state true in
      state,{expr_desc=`ExprITE {if_expr;then_expr;else_expr;ite_ty};expr_ty;expr_loc}

  and ident_expr_of_anf : env -> state -> Anflang.ident_expr -> state*ident_expr =
    fun env state expr ->
    let expr_loc = expr.expr_loc in
    match expr with
    | {expr_desc=((`ExprIdent v) as expr_desc);_} ->
      begin
        match Env.String.curr env |> Env.String.Map.find v with
        | TypedVar expr_ty ->
          state,{expr_desc;expr_loc;expr_ty}
        | Enum _ ->
          failwith "Identifier %s is a type name, not a value"
        | EnumVariant (_,expr_ty) ->
          let expr_ty = (expr_ty :> Types.ty_expr) in
          state,{expr_desc;expr_loc;expr_ty}
      end

  and const_expr_of_anf: env -> Anflang.const_expr -> const_expr =
    fun env expr ->
    let expr_loc = expr.expr_loc in
    match expr with
    | {expr_desc=`ExprConst const;_} ->
      let const = const_of_anf const in
      let expr_ty = type_of_const env const in
      {expr_desc=`ExprConst const;expr_loc;expr_ty}

  and const_of_anf: Anflang.const -> const =
    function
    | (`ConstInt _|`ConstChar _|`ConstFloat _) as a -> a
    | `ConstEnum {variant;enum} -> `ConstEnum {variant;enum}

  and type_of_const: env -> const -> Types.ty_expr =
    fun env const ->
    match const with
    | `ConstChar _ -> Types.Predef.ty_char
    | `ConstFloat _ -> Types.Predef.ty_float
    | `ConstInt _ -> Types.Predef.ty_int
    | `ConstEnum {enum;variant} ->
      begin
        match Env.String.curr env |> Env.String.Map.find_opt enum with
        | Some Enum (_,ty) ->
          (ty :> Types.ty_expr)
        | Some (EnumVariant _ |TypedVar _) | None ->
          failwith @@ Format.sprintf
            "Not a constant value %s"
            variant
      end

  and type_of_case: env -> String.t -> Types.ty_enum =
    fun env enum_variant ->
    match Env.String.curr env |> Env.String.Map.find_opt enum_variant with
    | Some EnumVariant (_,ty_enum) -> ty_enum
    | Some _ | None -> failwith ""

  and eqs_constraints env state eqs =
    let state,constraints =
      List.fold_left_map
        (eq_constraints env)
        state eqs
    in
    let constraints = List.concat constraints in
    state,constraints

  and eq_constraints env state ({eq_lhs;eq_rhs;_}:eq) =
    let state,constraints = expr_constraints env state eq_rhs in
    let state,lhs_ty = type_of_idents env state eq_lhs in
    state,(Eq (lhs_ty,eq_rhs.expr_ty))::constraints

  and expr_constraints env state expr =
    match expr with
    | {expr_desc=`ExprConst _|`ExprIdent _;_} ->
      state,[]
    | ({expr_desc=`ExprPckUp _; expr_ty;_} as expr)
    | ({expr_desc=`ExprWhen _; expr_ty;_} as expr)
    | ({expr_desc=`ExprMerge _; expr_ty;_} as expr)
    | ({expr_desc=`ExprFby _; expr_ty;_} as expr)
    | ({expr_desc=`ExprITE _; expr_ty;_} as expr)
    | ({expr_desc=`ExprApply _; expr_ty;_} as expr)
      ->
      let args =
        match expr.expr_desc with
        | `ExprPckUp {rate_expr;_} -> [(rate_expr :> atom_expr)]
        | `ExprFby (cst,{rate_expr;_}) ->
          [ (cst :> atom_expr); (rate_expr :> atom_expr) ]
        | `ExprWhen {when_expr;cond_expr;_} -> ([when_expr;cond_expr] :> atom_expr List.t)
        | `ExprMerge {merge_var;merge_exprs;_} ->
          (merge_var::(List.map snd merge_exprs) :> atom_expr List.t)
        | `ExprITE {if_expr;then_expr;else_expr;_} ->
          ([if_expr;then_expr;else_expr] :> atom_expr List.t)
        | `ExprApply (f,args) ->
          (f::args :> atom_expr List.t)
      in
      let fn_ty =
        match expr.expr_desc with
        | `ExprPckUp {rate_ty;_} | `ExprFby (_,{rate_ty;_})
          -> (rate_ty :> Types.ty_expr)
        | `ExprWhen {when_ty;_} -> (when_ty :> Types.ty_expr)
        | `ExprMerge {merge_ty;_} -> (merge_ty :> Types.ty_expr)
        | `ExprITE {ite_ty;_} -> (ite_ty :> Types.ty_expr)
        | `ExprApply ({expr_ty;_},_) -> expr_ty
      in
      let state,constraints =
        List.fold_left_map
          (fun state arg ->
             expr_constraints env state (arg :> expr))
          state args
      in
      let constraints = List.flatten constraints in
      let args_tys = List.map (fun e -> e.expr_ty) args in
      let state,eq_type = arrow_eq state args_tys expr_ty in
      state,(Eq (eq_type,fn_ty))::constraints

  and solve_constraints state constraints (substitutions: Types.ty_base IntMap.t) =
    Print.vdebug DebugTyped "%a@.%a@."
      (Print.int_map "" "@." "@."
         (fun ff (id,ty) ->
            Format.fprintf ff "%i -> %a" id Types.PP.ty (ty :> Types.ty_expr)))
      substitutions
      (Print.list "" "@." "@."
         (fun ff -> function
            | Eq (ty1,ty2) ->
              Format.fprintf ff "Eq@[<hov>(%a,@ %a)@]" Types.PP.ty ty1 Types.PP.ty ty2))
      constraints
    ;
    match constraints with
    | [] ->
      state,substitutions
    | (Eq (id1,id2))::constraints when id1=id2 ->
      (* types are identical, do nothing *)
      solve_constraints state constraints substitutions
    | (Eq (({ty_desc=`TyVar;_} as t1),({ty_desc=`TyVar;_} as t2)))::constraints
      ->
      let older,newer = if t1.ty_id < t2.ty_id then t1,t2 else t2,t1 in
      let state,substitutions = substitute_substitutions state newer older substitutions in
      let state,constraints = substitute_constraints state newer older constraints in
      solve_constraints state constraints substitutions
    | (Eq (({ty_desc=`TyArrow {ty_args=ta1;ty_out=to1};_} as arrow1),
           ({ty_desc=`TyArrow {ty_args=ta2;ty_out=to2};_} as arrow2)))
      ::constraints ->
      let new_constraints =
        match List.map2 (fun (_,t1) (_,t2) -> Eq (t1,t2)) ta1 ta2 with
        | x -> x
        | exception Invalid_argument _ ->
          failwith @@ Format.asprintf "arrow err %a %a" Types.PP.ty arrow1 Types.PP.ty arrow2
      in
      let new_constraints = Eq (to1,to2)::new_constraints in
      let constraints = List.rev_append new_constraints constraints in
      solve_constraints state constraints substitutions
    | (Eq(({ty_desc=`TyVar;_} as ty_var),
          ({ty_desc=`TyInt|`TyChar|`TyFloat|`TyEnum _;_} as ty_base)))::constraints
      ->
      let state,substitutions = substitute_substitutions state ty_var ty_base substitutions in
      let state,constraints = substitute_constraints state ty_var ty_base constraints in
      solve_constraints state constraints substitutions
    | (Eq(({ty_desc=`TyInt|`TyChar|`TyFloat|`TyEnum _;_} as ty_base),
          ({ty_desc=`TyVar;_} as ty_var)))::constraints
      ->
      let state,substitutions = substitute_substitutions state ty_var ty_base substitutions in
      let state,constraints = substitute_constraints state ty_var ty_base constraints in
      solve_constraints state constraints substitutions
    | Eq (t1,t2)::_ -> failwith (Format.asprintf "type clash %a %a" Types.PP.ty t1 Types.PP.ty t2)

  and vdecls_ty_subs state substitutions vdecls =
    List.fold_left_map
      (fun state v ->
         vdecl_ty_subs state substitutions v)
      state vdecls

  and vdecl_ty_subs state substitutions (v:vdecl) =
    match IntMap.find_opt v.vdecl_ty.ty_id substitutions with
    | Some vdecl_ty ->
      let vdecl_ty = (vdecl_ty :> Types.ty_expr) in
      state,{v with vdecl_ty}
    | None -> state,v

  and eqs_ty_subs state substitutions eqs =
    List.fold_left_map
      (fun state eq ->
         eq_ty_subs state substitutions eq)
      state eqs

  and eq_ty_subs state substitutions eq =
    let state,eq_rhs = expr_ty_subs state substitutions eq.eq_rhs in
    state,{eq with eq_rhs}

  and expr_ty_subs state substitutions expr =
    match expr with
    | {expr_desc=`ExprConst _;_} -> state,expr
    | ({expr_desc=`ExprIdent _;_} as expr) ->
      let state,expr = ident_expr_ty_subs state substitutions expr in
      state,(expr :> expr)
    | ({expr_desc=(`ExprApply _|`ExprPckUp _|`ExprFby _|`ExprWhen _|`ExprMerge _|`ExprITE _);_} as expr) ->
      let state,expr_desc =
        match expr.expr_desc with
        | `ExprApply (f,args) ->
          let state,f = ident_expr_ty_subs state substitutions f in
          let state,args =
            List.fold_left_map
              (fun state arg ->
                 ident_expr_ty_subs state substitutions arg)
              state args
          in
          state,`ExprApply (f,args)
        | `ExprPckUp ({rate_expr;rate_ty;_} as rate_op) ->
          let state,rate_expr = ident_expr_ty_subs state substitutions rate_expr in
          let state,rate_ty = ty_fn_ty_subs state substitutions rate_ty in
          state,`ExprPckUp {rate_op with rate_expr; rate_ty}
        | `ExprFby (cst,({rate_expr;rate_ty;_} as rate_op)) ->
          let state,rate_expr = ident_expr_ty_subs state substitutions rate_expr in
          let state,rate_ty = ty_fn_ty_subs state substitutions rate_ty in
          (* constants should already be fully typed *)
          state,`ExprFby (cst,{rate_op with rate_expr; rate_ty})
        | `ExprWhen ({when_expr;cond_expr;when_ty;_} as when_desc) ->
          let state,when_expr = ident_expr_ty_subs state substitutions when_expr in
          let state,cond_expr = ident_expr_ty_subs state substitutions cond_expr in
          let state,when_ty = ty_fn_ty_subs state substitutions when_ty in
          state,`ExprWhen {when_desc with when_expr;cond_expr;when_ty}
        | `ExprMerge {merge_var;merge_exprs;merge_ty} ->
          let state,merge_var = ident_expr_ty_subs state substitutions merge_var in
          let state,merge_exprs =
            List.fold_left_map
              (fun state (case,expr) ->
                 let state,expr = ident_expr_ty_subs state substitutions expr in
                 state,(case,expr))
              state merge_exprs
          in
          let state,merge_ty = ty_fn_ty_subs state substitutions merge_ty in
          state,`ExprMerge {merge_var;merge_exprs;merge_ty}
        | `ExprITE {if_expr;then_expr;else_expr;ite_ty} ->
          let state,if_expr = ident_expr_ty_subs state substitutions if_expr in
          let state,then_expr = ident_expr_ty_subs state substitutions then_expr in
          let state,else_expr = ident_expr_ty_subs state substitutions else_expr in
          let state,ite_ty = ty_fn_ty_subs state substitutions ite_ty in
          state,`ExprITE {if_expr;then_expr;else_expr;ite_ty}
      in
      let state,expr_ty = ty_ty_subs state substitutions expr.expr_ty in
      state,{expr with expr_ty; expr_desc}

  and ident_expr_ty_subs: _ -> _ -> ident_expr -> _ =
    fun state substitutions expr ->
    match IntMap.find_opt expr.expr_ty.ty_id substitutions with
    | Some expr_ty ->
      let expr_ty = (expr_ty :> Types.ty_expr) in
      state,{expr with expr_ty}
    | None -> state,expr

  and ty_ty_subs_aux : state -> Types.ty_base IntMap.t -> Types.ty_expr -> (state * Types.ty_expr) option =
    fun state substitutions ty ->
    match IntMap.find_opt ty.ty_id substitutions,ty with
    | _,({ty_desc=`TyVar;_} as ty_var) ->
      (ty_var_ty_subs_aux state substitutions ty_var :> (state * Types.ty_expr) option)
    | Some ty,_ -> Some(state,(ty :> Types.ty_expr))
    | None,{ty_desc=`TyUnivar|`TyInt|`TyChar|`TyFloat|`TyEnum _;_} ->
      None
    | None,{ty_desc=`TyArray (_t',_n);_} -> failwith "m"
    | None,{ty_desc=`TyTuple tyl;ty_scoped;_} ->
      let tyl,state,changed =
        List.fold_left
          (fun (tyl,state,changed) ty ->
             match ty_ty_subs_aux state substitutions ty with
             | Some (state,ty) -> (ty :> Types.ty_expr)::tyl,state,true
             | None -> ty::tyl,state,changed)
          ([],state,false) tyl
      in
      if changed then
        let tyl = List.rev tyl in
        let ty,state = new_type state (`TyTuple tyl) ty_scoped in
        Some(ty,state)
      else
        None
    | None,{ty_desc=`TyArrow {ty_args;ty_out};ty_scoped;_} ->
      let ty_args',state,args_changed =
        List.fold_left
          (fun (ty_args,state,changed) (arg,ty) ->
             match ty_ty_subs_aux state substitutions ty with
             | Some(state,ty) ->
               (arg,(ty :> Types.ty_expr))::ty_args,state,true
             | None ->
               (arg,ty)::ty_args,state,changed)
          ([],state,false) ty_args
      in
      let ty_args = if args_changed then List.rev ty_args' else ty_args in
      let ty_out' = ty_ty_subs_aux state substitutions ty_out in
      begin
        match (ty_out' :> (state * Types.ty_expr) option),ty_out,args_changed,state with
        | Some(state,ty_out),_,_,_ | None,ty_out,true,state ->
          let ty,state = new_type state (`TyArrow {ty_args;ty_out}) ty_scoped in
          Some (ty,state)
        | None,_,false,_ -> None
      end

  and ty_ty_subs : state -> Types.ty_base IntMap.t -> Types.ty_expr -> (state * Types.ty_expr) =
    fun state substitutions (ty: Types.ty_expr) ->
    match ty_ty_subs_aux state substitutions ty with
    | Some(state,ty) -> state,ty
    | None -> state,ty

  and ty_fn_ty_subs state substitutions ty =
    match ty_fn_ty_subs_aux state substitutions ty with
    | Some (state,ty) -> state,ty
    | None -> state,ty

  and ty_fn_ty_subs_aux state substitutions (ty: Types.ty_fn) =
    let {Types.ty_desc=`TyArrow {Types.ty_args;ty_out};ty_scoped;_} = ty in
    let ty_args',state,args_changed =
      List.fold_left
        (fun (ty_args,state,changed) (arg,ty) ->
           match ty_ty_subs_aux state substitutions ty with
           | Some(state,ty) ->
             (arg,(ty :> Types.ty_expr))::ty_args,state,true
           | None ->
             (arg,ty)::ty_args,state,changed)
        ([],state,false) ty_args
    in
    let ty_args = if args_changed then List.rev ty_args' else ty_args in
    let ty_out' = ty_ty_subs_aux state substitutions ty_out in
    begin
      match (ty_out' :> (state*Types.ty_expr) option),ty_out,args_changed,state with
      | Some(state,ty_out),_,_,_ | None,ty_out,true,state ->
        let ty,state = new_tyfn state ty_args ty_out ty_scoped in
        Some (ty,state)
      | None,_,false,_ -> None
    end

  and ty_var_ty_subs_aux state (substitutions: Types.ty_base IntMap.t) (ty: Types.ty_base) =
    match IntMap.find_opt ty.ty_id substitutions,ty with
    | Some ty,_ -> Some(state,ty)
    | None,_ -> None

  and ty_var_ty_subs state substitutions ty =
    match ty_var_ty_subs_aux state substitutions ty with
    | Some(state,ty) -> state,ty
    | None -> state,ty

  and substitute_constraints state from_ty to_ty constraints =
    let substitutions = IntMap.singleton from_ty.Types.ty_id to_ty in
    List.fold_left_map
      (fun state -> function
         | Eq (t1,t2) ->
           let state,t1 = ty_ty_subs state substitutions t1 in
           let state,t2 = ty_ty_subs state substitutions t2 in
           state,Eq (t1,t2))
      state constraints

  and substitute_substitutions state (from_ty : Types.ty_base) (to_ty : Types.ty_base) (substitutions : Types.ty_base IntMap.t) =
    let single_subs = IntMap.singleton from_ty.ty_id to_ty in
    let state,substitutions =
      IntMap.fold
        (fun ty_id old_to_ty (state,map) ->
           let state,new_to_ty = ty_var_ty_subs state single_subs old_to_ty in
           state,IntMap.add ty_id new_to_ty map)
        substitutions (state,IntMap.empty)
    in
    let substitutions =
      IntMap.update from_ty.ty_id
        (function
          | Some old_to_ty ->
            failwith (Format.asprintf "Internal error %a %a %a" Types.PP.ty (from_ty :> Types.ty_expr) Types.PP.ty (old_to_ty :> Types.ty_expr) Types.PP.ty (to_ty :> Types.ty_expr))
          | None -> Some to_ty)
        substitutions
    in
    state,substitutions

  and type_of_idents env state idents =
    match idents with
    | [] -> failwith "Internal error"
    | [x] ->
      begin
        match Env.String.curr env |> Env.String.Map.find x with
        | TypedVar ty -> state,ty
        | Enum _ | EnumVariant _ ->
          failwith @@ Format.asprintf "Identifier %s is not a variable" x
      end
    | xs ->
      let map = Env.String.curr env in
      let tys = List.map
          (fun v ->
             match Env.String.Map.find v map with
             | TypedVar ty_expr -> ty_expr
             | Enum _ | EnumVariant _ ->
               failwith @@ Format.asprintf "Identifier %s is not a variable" v)
          xs
      in
      new_type state (`TyTuple tys) true

  and type_of_vdecls (state: state) (vdecls: vdecl list) =
    match vdecls with
    | [] -> failwith "Internal error"
    | [v] ->
      state,v.vdecl_ty
    | vdecls ->
      let tys = List.map (fun (v:vdecl) -> v.vdecl_ty) vdecls in
      new_type state (`TyTuple tys) false

  and init_state main_node =
    {
      typer_state = init_typer_state;
      main_node;
    }

end

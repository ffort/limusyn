open Common

type decl =
  | DeclAny
  | Decl of decl_desc

and decl_desc =
  | Int
  | Char
  | Float
  | Array of decl * Int.t
  | Usertype of String.t

and 'ty_desc ty_expr_base =
  {
    ty_desc: 'ty_desc;
    ty_scoped: bool;
    ty_id: int;
  }

and ty_expr = ty_desc ty_expr_base

and ty_base = ty_base_desc ty_expr_base

and ty_enum = ty_enum_desc ty_expr_base

and ty_fn = ty_fn_desc ty_expr_base

and ty_desc =
  [
    | `TyVar
    | `TyUnivar
    | `TyArrow of fun_desc
    | `TyTuple of ty_expr list
    | `TyInt
    | `TyChar
    | `TyFloat
    | `TyArray of ty_expr * Int.t
    | `TyEnum of String.t * StringSet.t
  ]

and ty_base_desc =
  [
    | `TyVar
    | `TyInt
    | `TyChar
    | `TyFloat
    | `TyEnum of String.t * StringSet.t
  ]

and ty_enum_desc =
  [
    |` TyEnum of String.t * StringSet.t
  ]

and ty_fn_desc =
  [
    | `TyArrow of fun_desc
  ]

and fun_desc = {ty_args: (string * ty_expr) list; ty_out: ty_expr}

and named_type_kind = Alias | Enum

and state =
  {
    next_ty_id: Int.t;
    named_types: (named_type_kind*ty_expr) Common.StringMap.t;
  }

module PP =
struct
  let rec decl ff d =
    match d with
    | DeclAny -> Format.fprintf ff "_"
    | Decl Int -> Format.fprintf ff "int"
    | Decl Char -> Format.fprintf ff "char"
    | Decl Float -> Format.fprintf ff "float"
    | Decl Array (d',i) -> Format.fprintf ff "%a[%i]" decl d' i
    | Decl Usertype t -> Format.fprintf ff "%s" t

  let rec ty ff (t: ty_expr) =
    match t.ty_desc with
    | `TyInt -> Format.fprintf ff "int"
    | `TyFloat -> Format.fprintf ff "float"
    | `TyChar -> Format.fprintf ff "char"
    | `TyArray (t,n) ->
      Format.fprintf ff "%a[%i]" ty t n
    | `TyTuple tyl -> Print.list "@[(" "@ *@ " ")@]" ty ff tyl
    | `TyArrow {ty_args;ty_out} ->
      Format.fprintf ff "@[%a@ ->@ %a@]"
        (Print.list "" "@ ->@ " ""
           (fun ff (id,t) -> Format.fprintf ff "%s:%a" id ty t))
        ty_args ty ty_out
    | `TyEnum (e,_) -> Format.fprintf ff "%s" e
    | (`TyUnivar | `TyVar) as desc ->
      let pre = desc = `TyVar in
      Format.fprintf ff "'%st%i"
        (if pre then "_" else "") t.ty_id

end

let new_type_id ({next_ty_id;_} as state) =
  next_ty_id,{state with next_ty_id=next_ty_id+1}

let new_type : state -> 'ty_desc -> bool -> 'ty_desc ty_expr_base * state =
  fun state ty_desc ty_scoped ->
  let ty_id,state = new_type_id state in
  {
    ty_desc;
    ty_scoped;
    ty_id;
  },
  state

let new_tyvar : state -> bool -> ty_expr * state =
  fun state -> new_type state `TyVar

let new_tyfn : state -> (string * ty_expr) list -> ty_expr -> bool -> ty_fn * state =
  fun state ty_args ty_out ->
  new_type state (`TyArrow {ty_args;ty_out})

let new_tyenum : state -> String.t -> StringSet.t -> bool -> ty_enum * state =
  fun state enum variants ->
  new_type state (`TyEnum (enum,variants))

let new_named_type: state -> String.t -> named_type_kind -> ty_expr -> state =
  fun state name named_type_kind ty ->
  let named_types = StringMap.add name (named_type_kind,ty) state.named_types in
  {state with named_types}

module Predef =
struct

  let ty_int,ty_char,ty_float,init_state =
    let state = {next_ty_id=0;named_types=Common.StringMap.empty} in
    let ty_int,state = new_type state (`TyInt) false in
    let ty_char,state = new_type state (`TyChar) false in
    let ty_float,state = new_type state (`TyFloat) false in
    ty_int,ty_char,ty_float,state

end

let rec ty_expr_of_decl : state -> decl -> bool -> ty_expr * state =
  fun state decl scoped ->
  match decl with
  | DeclAny -> new_type state `TyVar scoped
  | Decl Int -> Predef.ty_int,state
  | Decl Char -> Predef.ty_char,state
  | Decl Float -> Predef.ty_float,state
  | Decl Array (d,n) ->
    let ty,state = ty_expr_of_decl state d scoped in
    new_type state (`TyArray (ty,n)) scoped
  | Decl Usertype ty ->
    begin
      match StringMap.find_opt ty state.named_types with
      | Some (_,ty) -> ty,state
      | None -> failwith (Format.sprintf "Incorrect type name: %s" ty)
    end

let instanciate : state -> ty_expr -> ty_expr * state =
  let rec aux: state -> ty_expr IntMap.t -> ty_expr -> (ty_expr * state * ty_expr IntMap.t) option =
    fun state inst_vars ty ->
      match ty.ty_desc with
      | `TyUnivar ->
        begin
          match IntMap.find_opt ty.ty_id inst_vars with
          | Some t' -> Some(t',state,inst_vars)
          | None ->
            let t',state = new_tyvar state true in
            let inst_vars = IntMap.add ty.ty_id t' inst_vars in
            Some(t',state,inst_vars)
        end
      | `TyVar
      | `TyInt |`TyChar | `TyFloat | `TyEnum _
        -> None
      | `TyArray (t',n) ->
        begin
          match aux state inst_vars t' with
          | Some(t',state,inst_vars) ->
            let ty,state = new_type state (`TyArray (t',n)) true in
            Some(ty,state,inst_vars)
          | None -> None
        end
      | `TyTuple tyl ->
        let tyl,state,inst_vars =
          List.fold_left
            (fun (tyl,state,inst_vars) ty ->
               match aux state inst_vars ty, tyl with
               | Some (ty,state,inst_vars), (`Changed tyl|`Unchanged tyl) ->
                 `Changed (ty::tyl),state,inst_vars
               | None, `Changed tyl ->
                 `Changed (ty::tyl),state,inst_vars
               | None, `Unchanged tyl ->
                 `Unchanged (ty::tyl),state,inst_vars)
            (`Unchanged [],state,inst_vars) tyl
        in
        begin
          match tyl with
          | `Unchanged _ -> None
          | `Changed tyl ->
            let tyl = List.rev tyl in
            let ty,state = new_type state (`TyTuple tyl) true in
            Some (ty,state,inst_vars)
        end
      | `TyArrow {ty_args;ty_out} ->
        let ty_args,state,inst_vars =
          List.fold_left
            (fun (ty_args,state,inst_vars) (arg,ty) ->
               match aux state inst_vars ty, ty_args with
               | Some (ty,state,inst_vars), (`Changed ty_args|`Unchanged ty_args) ->
                 `Changed ((arg,ty)::ty_args),state,inst_vars
               | None, `Changed ty_args ->
                 `Changed ((arg,ty)::ty_args),state,inst_vars
               | None, `Unchanged ty_args ->
                 `Unchanged ((arg,ty)::ty_args),state,inst_vars)
            (`Unchanged [],state,inst_vars) ty_args
        in
        let ty_out,state,inst_vars =
          match aux state inst_vars ty_out with
          | Some(ty_out,state,inst_vars) ->
            `Changed ty_out, state, inst_vars
          | None -> `Unchanged ty_out, state, inst_vars
        in
        begin
          match ty_args,ty_out with
          | `Changed ty_args,`Changed ty_out
          | `Changed ty_args,`Unchanged ty_out
          | `Unchanged ty_args,`Changed ty_out
            ->
            let ty,state = new_type state (`TyArrow {ty_args;ty_out}) true in
            Some (ty,state,inst_vars)
          | `Unchanged _,`Unchanged _ ->
            None
        end
  in
  fun state ty ->
    match aux state IntMap.empty ty with
    | Some (ty,state,_) -> ty,state
    | None -> ty,state

let generalize: state -> ty_expr -> (ty_expr * state,ty_expr) result =
  let rec aux: state -> ty_expr IntMap.t -> ty_expr -> ((ty_expr * state * ty_expr IntMap.t) option,ty_expr) result =
    fun state gen_vars ty ->
      match ty.ty_desc with
      | `TyVar ->
        if ty.ty_scoped then
          Error ty
        else
          begin
            match IntMap.find_opt ty.ty_id gen_vars with
            | Some tuni -> Ok (Some(tuni,state,gen_vars))
            | None ->
              let tuni,state = new_type state `TyUnivar false in
              let gen_vars = IntMap.add ty.ty_id tuni gen_vars in
              Ok(Some(tuni,state,gen_vars))
          end
      | `TyUnivar
      | `TyInt | `TyChar | `TyFloat | `TyEnum _ ->
        Ok None
      | `TyArray (t',n) ->
        begin
          match aux state gen_vars t' with
          | Ok Some (t',state,gen_vars) ->
            let ty,state = new_type state (`TyArray (t',n)) false in
            Ok (Some(ty,state,gen_vars))
          | Ok None -> Ok None
          | Error x -> Error x
        end
      | `TyTuple tyl ->
        let tyl,changed,state,gen_vars =
          List.fold_left
            (fun (tyl,changed,state,gen_vars) ty ->
               match tyl with
               | Ok tyl ->
                 begin
                   match aux state gen_vars ty with
                   | Ok Some(ty,state,gen_vars) ->
                     Ok (ty::tyl),true,state,gen_vars
                   | Ok None ->
                     Ok (ty::tyl),changed,state,gen_vars
                   | Error x ->
                     Error x,changed,state,gen_vars
                 end
               | Error x -> Error x,changed,state,gen_vars)
            (Ok [],false,state,gen_vars) tyl
        in
        begin
          match tyl,changed with
          | Ok tyl,true ->
            let ty,state = new_type state (`TyTuple (List.rev tyl)) false in
            Ok (Some (ty,state,gen_vars))
          | Ok _,false -> Ok None
          | Error x,_ -> Error x
        end
      | `TyArrow {ty_args;ty_out} ->
        let ty_args,changed,state,gen_vars =
          List.fold_left
            (fun (ty_args,changed,state,gen_vars) (var,ty) ->
               match ty_args with
               | Ok ty_args ->
                 begin
                   match aux state gen_vars ty with
                   | Ok Some(ty,state,gen_vars) ->
                     Ok ((var,ty)::ty_args),true,state,gen_vars
                   | Ok None ->
                     Ok ((var,ty)::ty_args),changed,state,gen_vars
                   | Error x ->
                     Error x,changed,state,gen_vars
                 end
               | Error x -> Error x,changed,state,gen_vars)
            (Ok [],false,state,gen_vars) ty_args
        in
        let ty_args = Result.map List.rev ty_args in
        let ty_out' = aux state gen_vars ty_out in
        begin
          match ty_out',changed,ty_args with
          | Ok Some (ty_out,state,gen_vars),_,Ok ty_args ->
            let ty,state = new_type state (`TyArrow {ty_args;ty_out}) false in
            Ok (Some (ty,state,gen_vars))
          | Ok None,true,Ok ty_args ->
            let ty,state = new_type state (`TyArrow {ty_args;ty_out}) false in
            Ok (Some (ty,state,gen_vars))
          | Ok None,false,Ok _ ->
            Ok None
          | Error x,_,_ -> Error x
          | _,_,Error x -> Error x
        end
  in
  fun state ty ->
    match aux state IntMap.empty ty with
    | Ok Some(ty,state,_) -> Ok (ty,state)
    | Ok None -> Ok (ty,state)
    | Error x -> Error x

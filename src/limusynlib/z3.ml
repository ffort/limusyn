type t =
  {
    ic: in_channel;
    oc: out_channel;
    fmt: Format.formatter;
  }

let close z3 =
  Format.fprintf z3.fmt "@.";
  Unix.close_process (z3.ic,z3.oc)
  |> ignore

let send z3 smt =
  Print.vdebug Print.DebugZ3 "%a@."
    Smtlib.PP.pp smt;
  Format.fprintf z3.fmt
    "%a@."
    Smtlib.PP.pp smt

let push z3 =
  send z3 [Push]

let pop z3 =
  send z3 [Pop]

let check_sat z3 =
  send z3 [CheckSat];
  let line = input_line z3.ic in
  match line with
  | "sat" -> Smtlib.SAT
  | "unsat" | "unknown" -> Smtlib.UNSAT
  | s ->
    failwith@@Format.sprintf
      "Unrecognized (check-sat) result: %s."
      s

(* let check_sat_of z3 smt =
 *   push z3;
 *   send z3 smt;
 *   let sat = check_sat z3 in
 *   pop z3;
 *   sat *)

(* let check_unsat_of z3 smt =
 *   push z3;
 *   send z3 smt;
 *   let sat = check_sat z3 in
 *   pop z3;
 *   not sat
 *
 * let check_unsats_in z3 env f tests =
 *   push z3;
 *   send z3 env;
 *   let valids =
 *     List.filter
 *       (fun test ->
 *          not@@check_sat_of z3 (f test))
 *       tests
 *   in
 *   pop z3;
 *   valids *)

let get_int z3 v =
  send z3 [GetValue [v]];
  let str = input_line z3.ic in
  Scanf.sscanf str "((%_s %i))"
    Fun.id

let get_ints_of z3 vs smt =
  push z3;
  send z3 smt;
  let res =
    match check_sat z3 with
    | SAT -> Some (List.map (get_int z3) vs)
    | _ -> None
  in
  pop z3;
  res

let get_model z3 =
  let rec aux z3 parens acc =
    let str = input_line z3.ic in
    let o_par =
      String.to_seq str
      |> Seq.filter ((=) '(')
      |> List.of_seq |> List.length
    in
    let c_par =
      String.to_seq str
      |> Seq.filter ((=) ')')
      |> List.of_seq |> List.length
    in
    let parens = parens + o_par - c_par in
    let acc = acc ^ str in
    if parens > 0 then
      aux z3 parens acc
    else
      acc
  in
  send z3 [GetModel];
  aux z3 0 ""

let reset z3 =
  send z3 [Reset]

let make () =
  let ic,oc = Unix.open_process "z3 -in" in
  let fmt = Format.formatter_of_out_channel oc in
  let z3 = {ic;oc; fmt} in
  send z3 Smtlib.plulib;
  send z3 Smtlib.selftest;
  z3

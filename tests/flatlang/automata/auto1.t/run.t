Simple upsampling node with automaton
  $ limusync -node main -process print_flat ./main.plu
  type bool =
    | false
    | true
  
  type state0 =
    | State0A
    | State0B
  
  node main (i: _ _ ; j: _ _ ; c: _ _ ) returns (o: _ _ )
  var auto0B_ns: _ _ ; auto0A_ns: _ _ ; auto0B_s: _ _ ; auto0A_s: _ _ ;
      auto0B_o: _ _ ; auto0A_o: _ _ ; auto0_s: _ _ ; auto0_ns: _ _ ;
      auto0_pns: _ _ ;
  let
    auto0A_ns = State0A;
    auto0B_ns = State0B;
    auto0A_s = if c when State0A->auto0_ns then State0B else State0A;
    auto0B_s = if c when State0B->auto0_ns then State0A else State0B;
    auto0B_o = j when State0B->auto0_s *^ 2;
    auto0A_o = i when State0A->auto0_s *^ 2;
    o = merge(auto0_s, State0A->auto0A_o State0B->auto0B_o);
    auto0_pns = State0A fby auto0_ns;
    auto0_ns = merge(auto0_s, State0A->auto0A_ns State0B->auto0B_ns);
    auto0_s = merge(auto0_pns, State0A->auto0A_s State0B->auto0B_s);
  tel
  

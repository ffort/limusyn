Simple identity node with fixed rate
  $ limusync -node main -process print_rclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:int -> int rate
    i:{ pck | p = 10 && o = 0 } -> { pck | p = 10 && o = 0 }
    (i: int rate { pck | p = 10 && o = 0 } )
  returns
  (o: int rate { pck | p = 10 && o = 0 } )
  let
    o = i: int rate { pck | p = 10 && o = 0 };
  tel

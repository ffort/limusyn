Simple swap node with fixed rate
  $ limusync -node main -process print_rclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:int -> j:int -> (int * int) rate
    i:{ pck | p = 10 && o = 0 } -> j:{ pck | p = 20 && o = 0 } ->
    ({ pck | p = 20 && o = 0 } * { pck | p = 10 && o = 0 })
    (i: int rate { pck | p = 10 && o = 0 } ;
      j: int rate { pck | p = 20 && o = 0 } )
  returns
  (o: int rate { pck | p = 20 && o = 0 } ;
    p: int rate { pck | p = 10 && o = 0 } )
  let
    p = i: int rate { pck | p = 10 && o = 0 };
    o = j: int rate { pck | p = 20 && o = 0 };
  tel

Simple upsampling node with fixed rate
  $ limusync -node main -process print_rclocked ./main.plu
  type bool =
  | false
  | true

  node main : i:int -> int rate i:{ pck | p = 10 && o = 0 } -> { pck | true }
    (i: int rate { pck | p = 10 && o = 0 } )
  returns
  (o: int rate { pck | p = 5 && o = 0 } )
  let
    o =
      (i: int rate { pck | p = 10 && o = 0 })
        *^2(@pck_up_e:int -> int rate
          @pck_up_e:{ "_ck3 | p div 2 } ->
          { z("_ck3, \x.*p = p(zetax) && o = o(zetax)) |
          p = (p(@pck_up_e) / 2) && o = o(@pck_up_e) }): int rate
      { pck | p = 5 && o = 0 };
  tel

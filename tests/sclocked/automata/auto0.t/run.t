Simple pseudo-when/merge node with automaton
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  type state0 =
  | State0A
  | State0B
  
  node main : i:int -> j:int -> c:bool -> int rate
    i:{ pck | *6 } -> j:{ pck | *7 } -> c:{ pck | *8 } -> { pck | *202 }
    (i: int rate { pck | *6 } ; j: int rate { pck | *7 } ;
      c: bool rate { pck | *8 } )
  returns
  (o: int rate { pck | *202 } )
  var v1: bool rate { pck on State0B(auto0_ns, { pck | *64 }) | *171 } ;
      v0: bool rate { pck on State0A(auto0_ns, { pck | *40 }) | *164 } ;
      auto0B_ns: state0 rate { pck on State0B(auto0_s, { pck | *126 }) | *215 } ;
      auto0A_ns: state0 rate { pck on State0A(auto0_s, { pck | *126 }) | *214 } ;
      auto0B_s: state0 rate
      { pck on State0B(auto0_pns, { pck | *137 }) | *224 } ;
      auto0A_s: state0 rate
      { pck on State0A(auto0_pns, { pck | *137 }) | *223 } ;
      auto0B_o: int rate { pck on State0B(auto0_s, { pck | *88 }) | *182 } ;
      auto0A_o: int rate { pck on State0A(auto0_s, { pck | *88 }) | *199 } ;
      auto0_s: state0 rate { pck | *216 } ;
      auto0_ns: state0 rate { pck | *207 } ;
      auto0_pns: state0 rate { pck | *205 } ;
  let
    auto0A_ns =
      State0A: state0 rate { pck on State0A(auto0_s, { pck | *126 }) | *214 };
    auto0B_ns =
      State0B: state0 rate { pck on State0B(auto0_s, { pck | *126 }) | *215 };
    v0 =
      (c: bool rate { pck | *8 })
        when(@when_e:bool -> when_c:state0 -> bool rate
          @when_e:{ ''_ck37 | *38 } -> @when_c:{ ''_ck37 | *39 } ->
          { ''_ck37 on State0A(auto0_ns, { pck | *40 }) | *42 })
        State0A->auto0_ns: state0 rate { pck | *207 }: bool rate
      { pck on State0A(auto0_ns, { pck | *40 }) | *164 };
    auto0A_s =
      if(@ite_if:bool -> @ite_then:state0 -> @ite_else:state0 -> state0 rate
        @ite_if:{ ''_ck50 | *51 } -> @ite_then:{ ''_ck52 | *53 } ->
        @ite_else:{ ''_ck54 | *55 } -> { ''_ck56 | *57 })
        v0: bool rate { pck on State0A(auto0_ns, { pck | *40 }) | *164 } then
        State0B: state0 rate { ''_ck46 | *47 } else
        State0A: state0 rate { ''_ck48 | *49 }: state0 rate
      { pck on State0A(auto0_pns, { pck | *137 }) | *223 };
    v1 =
      (c: bool rate { pck | *8 })
        when(@when_e:bool -> when_c:state0 -> bool rate
          @when_e:{ ''_ck61 | *62 } -> @when_c:{ ''_ck61 | *63 } ->
          { ''_ck61 on State0B(auto0_ns, { pck | *64 }) | *66 })
        State0B->auto0_ns: state0 rate { pck | *207 }: bool rate
      { pck on State0B(auto0_ns, { pck | *64 }) | *171 };
    auto0B_s =
      if(@ite_if:bool -> @ite_then:state0 -> @ite_else:state0 -> state0 rate
        @ite_if:{ ''_ck74 | *75 } -> @ite_then:{ ''_ck76 | *77 } ->
        @ite_else:{ ''_ck78 | *79 } -> { ''_ck80 | *81 })
        v1: bool rate { pck on State0B(auto0_ns, { pck | *64 }) | *171 } then
        State0A: state0 rate { ''_ck70 | *71 } else
        State0B: state0 rate { ''_ck72 | *73 }: state0 rate
      { pck on State0B(auto0_pns, { pck | *137 }) | *224 };
    auto0B_o =
      (j: int rate { pck | *7 })
        when(@when_e:int -> when_c:state0 -> int rate
          @when_e:{ ''_ck85 | *86 } -> @when_c:{ ''_ck85 | *87 } ->
          { ''_ck85 on State0B(auto0_s, { pck | *88 }) | *90 })
        State0B->auto0_s: state0 rate { pck | *216 }: int rate
      { pck on State0B(auto0_s, { pck | *88 }) | *182 };
    auto0A_o =
      (i: int rate { pck | *6 })
        when(@when_e:int -> when_c:state0 -> int rate
          @when_e:{ ''_ck94 | *95 } -> @when_c:{ ''_ck94 | *96 } ->
          { ''_ck94 on State0A(auto0_s, { pck | *97 }) | *99 })
        State0A->auto0_s: state0 rate { pck | *216 }: int rate
      { pck on State0A(auto0_s, { pck | *88 }) | *201 };
    o =
      merge(@merge_c:state0 -> @merge_State0A:int -> @merge_State0B:int -> int
        rate
        @merge_c:{ ''_ck103 | *104 } ->
        @merge_State0A:{ ''_ck103 on State0A(auto0_s, { pck | *105 }) | *107 }
        ->
        @merge_State0B:{ ''_ck103 on State0B(auto0_s, { pck | *105 }) | *109 }
        -> { ''_ck103 | *110 })(auto0_s: state0 rate { pck | *216 },
        State0A->auto0A_o: int rate
                 { pck on State0A(auto0_s, { pck | *88 }) | *199 },
        State0B->auto0B_o: int rate
                 { pck on State0B(auto0_s, { pck | *88 }) | *182 }): int rate
      { pck | *202 };
    auto0_pns =
      State0A: state0 rate { ''_ck120 | *121 }
        fby(@fyb_cst:state0 -> @fby_e:state0 -> state0 rate
          @fby_cst:{ ''_ck114 | *115 } -> @fby_e:{ ''_ck114 | *116 } ->
          { z(''_ck114, \x.*117) | *118 }) auto0_ns: state0 rate { pck | *207 }
      : state0 rate { pck | *205 };
    auto0_ns =
      merge(@merge_c:state0 -> @merge_State0A:state0 -> @merge_State0B:state0
            -> state0 rate
        @merge_c:{ ''_ck124 | *125 } ->
        @merge_State0A:{ ''_ck124 on State0A(auto0_s, { pck | *126 }) | *128 }
        ->
        @merge_State0B:{ ''_ck124 on State0B(auto0_s, { pck | *126 }) | *130 }
        -> { ''_ck124 | *131 })(auto0_s: state0 rate { pck | *216 },
        State0A->auto0A_ns: state0 rate
                 { pck on State0A(auto0_s, { pck | *126 }) | *214 },
        State0B->auto0B_ns: state0 rate
                 { pck on State0B(auto0_s, { pck | *126 }) | *215 }): state0
      rate { pck | *207 };
    auto0_s =
      merge(@merge_c:state0 -> @merge_State0A:state0 -> @merge_State0B:state0
            -> state0 rate
        @merge_c:{ ''_ck135 | *136 } ->
        @merge_State0A:{ ''_ck135 on State0A(auto0_pns, { pck | *137 }) | *139
                       }
        ->
        @merge_State0B:{ ''_ck135 on State0B(auto0_pns, { pck | *137 }) | *141
                       }
        -> { ''_ck135 | *142 })(auto0_pns: state0 rate { pck | *205 },
        State0A->auto0A_s: state0 rate
                 { pck on State0A(auto0_pns, { pck | *137 }) | *223 },
        State0B->auto0B_s: state0 rate
                 { pck on State0B(auto0_pns, { pck | *137 }) | *224 }): state0
      rate { pck | *216 };
  tel

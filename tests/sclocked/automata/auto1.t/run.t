Simple switch node with automaton
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  type state0 =
  | State0A
  | State0B
  
  node main : i:int -> j:int -> c:bool -> (int * int) rate
    i:{ pck | *6 } -> j:{ pck | *7 } -> c:{ pck | *8 } ->
    ({ pck | *269 } * { pck | *256 })
    (i: int rate { pck | *6 } ; j: int rate { pck | *7 } ;
      c: bool rate { pck | *8 } )
  returns
  (o: int rate { pck | *269 } ; p: int rate { pck | *256 } )
  var v1: bool rate { pck on State0B(auto0_ns, { pck | *70 }) | *210 } ;
      v0: bool rate { pck on State0A(auto0_ns, { pck | *46 }) | *203 } ;
      auto0B_ns: state0 rate { pck on State0B(auto0_s, { pck | *161 }) | *282 } ;
      auto0A_ns: state0 rate { pck on State0A(auto0_s, { pck | *161 }) | *281 } ;
      auto0B_s: state0 rate
      { pck on State0B(auto0_pns, { pck | *172 }) | *291 } ;
      auto0A_s: state0 rate
      { pck on State0A(auto0_pns, { pck | *172 }) | *290 } ;
      auto0B_p: int rate { pck on State0B(auto0_s, { pck | *103 }) | *230 } ;
      auto0B_o: int rate { pck on State0B(auto0_s, { pck | *94 }) | *224 } ;
      auto0A_p: int rate { pck on State0A(auto0_s, { pck | *103 }) | *253 } ;
      auto0A_o: int rate { pck on State0A(auto0_s, { pck | *94 }) | *266 } ;
      auto0_s: state0 rate { pck | *283 } ;
      auto0_ns: state0 rate { pck | *274 } ;
      auto0_pns: state0 rate { pck | *272 } ;
  let
    auto0A_ns =
      State0A: state0 rate { pck on State0A(auto0_s, { pck | *161 }) | *281 };
    auto0B_ns =
      State0B: state0 rate { pck on State0B(auto0_s, { pck | *161 }) | *282 };
    v0 =
      (c: bool rate { pck | *8 })
        when(@when_e:bool -> when_c:state0 -> bool rate
          @when_e:{ ''_ck43 | *44 } -> @when_c:{ ''_ck43 | *45 } ->
          { ''_ck43 on State0A(auto0_ns, { pck | *46 }) | *48 })
        State0A->auto0_ns: state0 rate { pck | *274 }: bool rate
      { pck on State0A(auto0_ns, { pck | *46 }) | *203 };
    auto0A_s =
      if(@ite_if:bool -> @ite_then:state0 -> @ite_else:state0 -> state0 rate
        @ite_if:{ ''_ck56 | *57 } -> @ite_then:{ ''_ck58 | *59 } ->
        @ite_else:{ ''_ck60 | *61 } -> { ''_ck62 | *63 })
        v0: bool rate { pck on State0A(auto0_ns, { pck | *46 }) | *203 } then
        State0B: state0 rate { ''_ck52 | *53 } else
        State0A: state0 rate { ''_ck54 | *55 }: state0 rate
      { pck on State0A(auto0_pns, { pck | *172 }) | *290 };
    v1 =
      (c: bool rate { pck | *8 })
        when(@when_e:bool -> when_c:state0 -> bool rate
          @when_e:{ ''_ck67 | *68 } -> @when_c:{ ''_ck67 | *69 } ->
          { ''_ck67 on State0B(auto0_ns, { pck | *70 }) | *72 })
        State0B->auto0_ns: state0 rate { pck | *274 }: bool rate
      { pck on State0B(auto0_ns, { pck | *70 }) | *210 };
    auto0B_s =
      if(@ite_if:bool -> @ite_then:state0 -> @ite_else:state0 -> state0 rate
        @ite_if:{ ''_ck80 | *81 } -> @ite_then:{ ''_ck82 | *83 } ->
        @ite_else:{ ''_ck84 | *85 } -> { ''_ck86 | *87 })
        v1: bool rate { pck on State0B(auto0_ns, { pck | *70 }) | *210 } then
        State0A: state0 rate { ''_ck76 | *77 } else
        State0B: state0 rate { ''_ck78 | *79 }: state0 rate
      { pck on State0B(auto0_pns, { pck | *172 }) | *291 };
    auto0B_o =
      (j: int rate { pck | *7 })
        when(@when_e:int -> when_c:state0 -> int rate
          @when_e:{ ''_ck91 | *92 } -> @when_c:{ ''_ck91 | *93 } ->
          { ''_ck91 on State0B(auto0_s, { pck | *94 }) | *96 })
        State0B->auto0_s: state0 rate { pck | *283 }: int rate
      { pck on State0B(auto0_s, { pck | *94 }) | *224 };
    auto0B_p =
      (i: int rate { pck | *6 })
        when(@when_e:int -> when_c:state0 -> int rate
          @when_e:{ ''_ck100 | *101 } -> @when_c:{ ''_ck100 | *102 } ->
          { ''_ck100 on State0B(auto0_s, { pck | *103 }) | *105 })
        State0B->auto0_s: state0 rate { pck | *283 }: int rate
      { pck on State0B(auto0_s, { pck | *103 }) | *230 };
    auto0A_o =
      (i: int rate { pck | *6 })
        when(@when_e:int -> when_c:state0 -> int rate
          @when_e:{ ''_ck109 | *110 } -> @when_c:{ ''_ck109 | *111 } ->
          { ''_ck109 on State0A(auto0_s, { pck | *112 }) | *114 })
        State0A->auto0_s: state0 rate { pck | *283 }: int rate
      { pck on State0A(auto0_s, { pck | *94 }) | *268 };
    auto0A_p =
      (j: int rate { pck | *7 })
        when(@when_e:int -> when_c:state0 -> int rate
          @when_e:{ ''_ck118 | *119 } -> @when_c:{ ''_ck118 | *120 } ->
          { ''_ck118 on State0A(auto0_s, { pck | *121 }) | *123 })
        State0A->auto0_s: state0 rate { pck | *283 }: int rate
      { pck on State0A(auto0_s, { pck | *103 }) | *255 };
    p =
      merge(@merge_c:state0 -> @merge_State0A:int -> @merge_State0B:int -> int
        rate
        @merge_c:{ ''_ck127 | *128 } ->
        @merge_State0A:{ ''_ck127 on State0A(auto0_s, { pck | *129 }) | *131 }
        ->
        @merge_State0B:{ ''_ck127 on State0B(auto0_s, { pck | *129 }) | *133 }
        -> { ''_ck127 | *134 })(auto0_s: state0 rate { pck | *283 },
        State0A->auto0A_p: int rate
                 { pck on State0A(auto0_s, { pck | *103 }) | *253 },
        State0B->auto0B_p: int rate
                 { pck on State0B(auto0_s, { pck | *103 }) | *230 }): int rate
      { pck | *256 };
    o =
      merge(@merge_c:state0 -> @merge_State0A:int -> @merge_State0B:int -> int
        rate
        @merge_c:{ ''_ck138 | *139 } ->
        @merge_State0A:{ ''_ck138 on State0A(auto0_s, { pck | *140 }) | *142 }
        ->
        @merge_State0B:{ ''_ck138 on State0B(auto0_s, { pck | *140 }) | *144 }
        -> { ''_ck138 | *145 })(auto0_s: state0 rate { pck | *283 },
        State0A->auto0A_o: int rate
                 { pck on State0A(auto0_s, { pck | *94 }) | *266 },
        State0B->auto0B_o: int rate
                 { pck on State0B(auto0_s, { pck | *94 }) | *224 }): int rate
      { pck | *269 };
    auto0_pns =
      State0A: state0 rate { ''_ck155 | *156 }
        fby(@fyb_cst:state0 -> @fby_e:state0 -> state0 rate
          @fby_cst:{ ''_ck149 | *150 } -> @fby_e:{ ''_ck149 | *151 } ->
          { z(''_ck149, \x.*152) | *153 }) auto0_ns: state0 rate { pck | *274 }
      : state0 rate { pck | *272 };
    auto0_ns =
      merge(@merge_c:state0 -> @merge_State0A:state0 -> @merge_State0B:state0
            -> state0 rate
        @merge_c:{ ''_ck159 | *160 } ->
        @merge_State0A:{ ''_ck159 on State0A(auto0_s, { pck | *161 }) | *163 }
        ->
        @merge_State0B:{ ''_ck159 on State0B(auto0_s, { pck | *161 }) | *165 }
        -> { ''_ck159 | *166 })(auto0_s: state0 rate { pck | *283 },
        State0A->auto0A_ns: state0 rate
                 { pck on State0A(auto0_s, { pck | *161 }) | *281 },
        State0B->auto0B_ns: state0 rate
                 { pck on State0B(auto0_s, { pck | *161 }) | *282 }): state0
      rate { pck | *274 };
    auto0_s =
      merge(@merge_c:state0 -> @merge_State0A:state0 -> @merge_State0B:state0
            -> state0 rate
        @merge_c:{ ''_ck170 | *171 } ->
        @merge_State0A:{ ''_ck170 on State0A(auto0_pns, { pck | *172 }) | *174
                       }
        ->
        @merge_State0B:{ ''_ck170 on State0B(auto0_pns, { pck | *172 }) | *176
                       }
        -> { ''_ck170 | *177 })(auto0_pns: state0 rate { pck | *272 },
        State0A->auto0A_s: state0 rate
                 { pck on State0A(auto0_pns, { pck | *172 }) | *290 },
        State0B->auto0B_s: state0 rate
                 { pck on State0B(auto0_pns, { pck | *172 }) | *291 }): state0
      rate { pck | *283 };
  tel

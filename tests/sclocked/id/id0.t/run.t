Simple identity node without annotations
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:'t7 -> 't7 rate i:{ 'ck12 | *13 } -> { 'ck12 | *13 }
    (i: '_t4 rate { '_ck6 | *7 } )
  returns
  (o: '_t4 rate { '_ck6 | *7 } )
  let
    o = i: '_t4 rate { '_ck6 | *7 };
  tel

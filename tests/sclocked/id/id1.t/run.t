Simple identity node with type annotations
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:int -> int rate i:{ 'ck12 | *13 } -> { 'ck12 | *13 }
    (i: int rate { '_ck6 | *7 } )
  returns
  (o: int rate { '_ck6 | *7 } )
  let
    o = i: int rate { '_ck6 | *7 };
  tel

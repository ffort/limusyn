Simple identity node with rate annotations
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:'t7 -> 't7 rate i:{ pck | *6 } -> { pck | *6 }
    (i: '_t4 rate { pck | *6 } )
  returns
  (o: '_t4 rate { pck | *6 } )
  let
    o = i: '_t4 rate { pck | *6 };
  tel

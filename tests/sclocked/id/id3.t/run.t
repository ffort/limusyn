Simple identity node with two inputs and no annotations
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:'t10 -> j:'t11 -> ('t10 * 't11) rate
    i:{ 'ck18 | *19 } -> j:{ 'ck20 | *21 } ->
    ({ 'ck18 | *19 } * { 'ck20 | *21 })
    (i: '_t4 rate { '_ck6 | *7 } ; j: '_t5 rate { '_ck8 | *9 } )
  returns
  (o: '_t4 rate { '_ck6 | *7 } ; p: '_t5 rate { '_ck8 | *9 } )
  let
    p = j: '_t5 rate { '_ck8 | *9 };
    o = i: '_t4 rate { '_ck6 | *7 };
  tel

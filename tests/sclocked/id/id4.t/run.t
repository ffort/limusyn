Simple identity node without annotations
Input/Output relation is "swapped"
(first input goes to second output, second input goes to first output)
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:'t10 -> j:'t11 -> ('t11 * 't10) rate
    i:{ 'ck18 | *19 } -> j:{ 'ck20 | *21 } ->
    ({ 'ck20 | *21 } * { 'ck18 | *19 })
    (i: '_t4 rate { '_ck6 | *7 } ; j: '_t5 rate { '_ck8 | *9 } )
  returns
  (o: '_t5 rate { '_ck8 | *9 } ; p: '_t4 rate { '_ck6 | *7 } )
  let
    p = i: '_t4 rate { '_ck6 | *7 };
    o = j: '_t5 rate { '_ck8 | *9 };
  tel

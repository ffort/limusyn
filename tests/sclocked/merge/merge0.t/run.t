Simple merge node
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:'t31 -> j:'t31 -> c:bool -> 't31 rate
    i:{ 'ck97 | *98 } -> j:{ 'ck97 | *99 } -> c:{ 'ck97 | *100 } ->
    { 'ck97 | *101 }
    (i: '_t4 rate { '_ck6 | *7 } ; j: '_t4 rate { '_ck6 | *82 } ;
      c: bool rate { '_ck6 | *83 } )
  returns
  (o: '_t4 rate { '_ck6 | *84 } )
  var i_t: '_t4 rate { '_ck6 on true(c, { pck | *20 }) | *93 } ;
      j_f: '_t4 rate { '_ck6 on false(c, { pck | *20 }) | *88 } ;
  let
    o =
      merge(@merge_c:bool -> @merge_true:'_t4 -> @merge_false:'_t4 -> '_t4 rate
        @merge_c:{ ''_ck18 | *19 } ->
        @merge_true:{ ''_ck18 on true(c, { pck | *20 }) | *22 } ->
        @merge_false:{ ''_ck18 on false(c, { pck | *20 }) | *24 } ->
        { ''_ck18 | *25 })(c: bool rate { '_ck6 | *83 },
        true->i_t: '_t4 rate { '_ck6 on true(c, { pck | *20 }) | *93 },
        false->j_f: '_t4 rate { '_ck6 on false(c, { pck | *20 }) | *88 }): '_t4
      rate { '_ck6 | *84 };
    j_f =
      (j: '_t4 rate { '_ck6 | *82 })
        when(@when_e:'_t4 -> when_c:bool -> '_t4 rate
          @when_e:{ ''_ck29 | *30 } -> @when_c:{ ''_ck29 | *31 } ->
          { ''_ck29 on false(c, { pck | *32 }) | *34 })
        false->c: bool rate { '_ck6 | *83 }: '_t4 rate
      { '_ck6 on false(c, { pck | *20 }) | *90 };
    i_t =
      (i: '_t4 rate { '_ck6 | *7 })
        when(@when_e:'_t4 -> when_c:bool -> '_t4 rate
          @when_e:{ ''_ck38 | *39 } -> @when_c:{ ''_ck38 | *40 } ->
          { ''_ck38 on true(c, { pck | *41 }) | *43 })
        true->c: bool rate { '_ck6 | *83 }: '_t4 rate
      { '_ck6 on true(c, { pck | *20 }) | *95 };
  tel

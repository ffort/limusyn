Simple upsampling node; upsamples input by factor 2
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:int -> int rate i:{ pck | *6 } -> { pck | *21 }
    (i: int rate { pck | *6 } )
  returns
  (o: int rate { pck | *21 } )
  let
    o =
      (i: int rate { pck | *6 })
        *^2(@pck_up_e:int -> int rate @pck_up_e:{ pck | *19 } -> { pck | *20 })
      : int rate { pck | *21 };
  tel

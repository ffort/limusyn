Upsampling node with one output and two outputs
first output is repeatition of the input
second output is the input upsampled by factor 2
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:int -> (int * int) rate
    i:{ pck | *6 } -> ({ pck | *6 } * { pck | *24 })
    (i: int rate { pck | *6 } )
  returns
  (o: int rate { pck | *6 } ; p: int rate { pck | *24 } )
  let
    p =
      (i: int rate { pck | *6 })
        *^2(@pck_up_e:int -> int rate @pck_up_e:{ pck | *22 } -> { pck | *23 })
      : int rate { pck | *24 };
    o = i: int rate { pck | *6 };
  tel

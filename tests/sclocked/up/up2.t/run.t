Double upsampling node
Input is first upsampled by factor 2, then factor 3
  $ limusync -node main -process print_sclocked ./main.plu
  type bool =
  | false
  | true
  
  node main : i:int -> int rate i:{ pck | *6 } -> { pck | *36 }
    (i: int rate { pck | *6 } )
  returns
  (o: int rate { pck | *36 } )
  var v0: int rate { pck | *31 } ;
  let
    v0 =
      (i: int rate { pck | *6 })
        *^2(@pck_up_e:int -> int rate @pck_up_e:{ pck | *29 } -> { pck | *30 })
      : int rate { pck | *31 };
    o =
      (v0: int rate { pck | *31 })
        *^3(@pck_up_e:int -> int rate @pck_up_e:{ pck | *34 } -> { pck | *35 })
      : int rate { pck | *36 };
  tel
